.pushseg
.segment "ZEROPAGE"

; Zero Page Addresses
; Reserved by X16 Kernel
; This gets reserved via the ZP bits in cx16.cfg
;RESERVED: .res $22

zp_SPEED: .byte 0
zp_BPM: .byte 0
zp_VBLANK_SKIP_COUNT: .byte 0   ; Count of current VBLANK skip
zp_NOTE_NUMERIC: .byte 0

; Bit 7 = Note Play
; Bit 6 = Release
; Bit 5 = Off
; Bit 4 = Null
; Bit 3 = Legato
zp_NOTE_FLAGS: .byte 0

; Pattern Row Data
zp_ROW_NOTE: .byte 0
zp_ROW_INSTRUMENT: .byte 0
zp_ROW_VOLUME: .byte 0
zp_ROW_EFFECT_PARAMETER: .byte 0
zp_ROW_EFFECT_VALUE: .byte 0
zp_MACRO_EFFECT_PARAMETER: .byte 0
zp_MACRO_EFFECT_VALUE: .byte 0
zp_MACRO_ADDRESS: .word 00

zp_MAX_PATTERN: .byte 0
zp_PATTERN_NUMBER: .byte 0
zp_ROW_NUMBER: .byte 0
zp_ORDER_NUMBER: .byte 0
zp_INSTRUMENT: .byte 0
zp_CHANNEL_COUNT: .byte 0

; Track the state of the tracker engine
zp_STATE: .byte 0

; The offset for the jump tables of the current module we are in
zp_CURRENT_MODULE: .byte $00

zp_ROW_POINTER: .word $0000           ; 16-bit address of the row in the pattern
zp_PAGE_POINTER: .word $0000
zp_SCROLL_ROW_POINTER: .word $0000
zp_SCROLL_PAGE_POINTER: .word $0000
; Absolute row position on scroll
zp_SCROLL_ROW_NUMBER: .byte 0
; Relative screen position on scroll.
; Can be different if starting the song at a place other than row 00 pattern 00
zp_SCROLL_SCREEN_NUMBER: .byte 0
zp_SCROLL_PATTERN_NUMBER: .byte 0
zp_SCROLL_ORDER_NUMBER: .byte 0
zp_SCROLL_ROW_EFFECT_PARAM: .byte 0
zp_SCROLL_ROW_EFFECT_VALUE: .byte 0

; PCM ZP Entires
zp_PCM_SAMPLE_NUMBER:       .byte $00
zp_PCM_SAMPLE_PAGE:         .byte $00
zp_PCM_SAMPLE_ADDRESS:      .word $0000
zp_PCM_FLAGS:               .byte $00
zp_PCM_SAMPLE_RATE:         .byte $00
zp_PCM_VOLUME:              .byte $00
zp_PCM_NUM_CHONKS:          .byte $00
zp_PCM_CHONK_COUNT:         .byte $00




; Temp vars for math operations
zp_MATH0: .word $0000
zp_MATH1: .word $0000
zp_MATH2: .word $0000
zp_MATH3: .word $0000
zp_MATH4: .word $0000
zp_MATH5: .word $0000

; Temp vars for address manipulation
zp_ADDR0: .word $0000
zp_ADDR1: .word $0000
zp_ADDR2: .word $0000




; MIDI IO card base address
zp_MIDI_BASE: 	.word $0000
zp_WAVETABLE_BASE: 	.word $0000
; Since MIDI clock runs at 24 PPQN, we need to count up 24 times
; per row update and we use this to track it
zp_MIDI_CLOCK_COUNTER: .byte 0
; Song start/stop state
; 00 = Stopped
; 01 = Running
; FF = Disabled
zp_MIDI_STATE: .byte 0
zp_MIDI_CHANNEL: .byte 0

.segment "EXTZP": zeropage

; Our own temporary variables
; Function call arguments
zp_ARG0: .word $0000
zp_ARG1: .word $0000
zp_ARG2: .word $0000
zp_ARG3: .word $0000
zp_ARG4: .word $0000
zp_ARG5: .word $0000
zp_ARG6: .word $0000
zp_ARG7: .word $0000
zp_ARG8: .word $0000


; Color of text for drawing things
zp_TEXT_COLOR: .byte 0

; Keyboard input from user
zp_KEY_PRESSED: .byte 0

; FM ZP Entries
zp_FM_ADDRESS:              .byte $00
zp_FM_DATA:                 .byte $00



zp_ISR_TEMP: .byte $00

; Special value for returning from a jump table
; (e.g. the effects table)
zp_ADDR_RETURN: .word $0000

;; ZSM
; Whether or not to write ZSM
zp_ZSM_ENABLE: .byte 0
; Counter that tracks tick delays (to be written before activity)
zp_ZSM_DELAY_COUNTER: .byte 0
; This indicates something changed for the current tick
; meaning we should write a $80 since we wrote stuff
; or else increment zp_ZSM_DELAY_COUNTER.
; This is part of the ISR.
zp_ZSM_TICK_ACTIVE: .byte 0
; Size of ZSM file (used mostly to determine the value of the PCM
; header if PCM data is present
zp_ZSM_SIZE: .faraddr $000000

; Temporary variables
zp_TMP0: .byte $00
zp_TMP1: .byte $00
zp_TMP2: .byte $00
zp_TMP3: .byte $00
zp_TMP4: .byte $00
zp_TMP5: .byte $00
zp_TMP6: .byte $00
zp_TMP7: .byte $00
zp_TMP8: .byte $00
zp_TMP9: .byte $00
zp_TMPA: .byte $00


; Bank Numbers
;; Bank labels
; zp_BANK_MACROS: .byte $00 ; Macros Page
zp_BANK_TABLES: .byte $00 ; Tables Page
zp_BANK_FINS:   .byte $00 ; FM Instruments Page
zp_BANK_VINS:   .byte $00 ; VERA Instruments Page
zp_BANK_ENVS:   .byte $00 ; Envelopes
zp_BANK_MISC:   .byte $00 ; Misc


.popseg