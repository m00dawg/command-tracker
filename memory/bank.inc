; Defined bank segments in BRAM

.pushseg

; Bank labels
; Now generated on the fly based on available RAM
;BANK_TABLES = $FB ; FM Instruments Page
;BANK_FINS   = $FC ; FM Instruments Page
;BANK_VINS   = $FD ; VERA Instruments Page
;BANK_ENVS   = $FE ; Envelopes
;BANK_MISC   = $FF ; Misc

.segment "BANKMISC"
    ; An array for the order list
    order_list: .res $100
    ; An array to store the sorted order list - this is temporary data for when
    ; saving a song
    order_list_sorted: .res $100

    song_title: .res $20
    composer: .res $20
    sync_method: .res $01
    starting_speed: .res $01
    starting_bpm: .res $01
    global_transpose: .res $01

    ; Global FM Initial Settings:
    fm_global_timer_a_high: .res $01
    fm_global_timer_a_low: .res $01
    fm_global_timer_b: .res $01
    fm_global_irq_control: .res $01
    fm_global_ch7_noise_control: .res $01
    fm_global_lfo_frequency: .res $01
    fm_global_lfo_am_depth: .res $01
    fm_global_lfo_pm_depth: .res $01
    fm_global_lfo_waveform: .res $01

    ; PSG Channel Types (PSG, MIDI, WT)
    psg_channel_modes: .res $10

    ; Padding
    padding: .res $00A0
    
    

    ; At $02FD

    song_description: .res $0400

    ; At $0700
    ; Space to grow
    space_to_grow: .res $0100

    ; pcm sample names = 2048 (16 * 128)
    ; Starts at $A800 or 2k into the 8k
    sample_names:  .res ($80 * $10)  ; 128 * 16

    ; PCM metadata is accessed via x offsets
    ; e.g.
    ; LDA pcm_sample_state,x
    ; where X is sample number

    ; Starts at $B000   
    ; bit 7: active (whether to save to song file or not)
    ; bit 3-0: volume
    sample_states:   .res $80

    ; Starts at $B080
    ; 1k, 64 macros x 8 effects
    macros: .res $0400

    ; Remaining $0B86 (2.9k)

;.segment "BANKMCRO"
    ; Macros
    ; There are 64 macros so each high byte of the
    ; address points to 8 macros.
    ; So the "page" is the macro number divided by 8
    ; and the remainder is if it's one of 8 slots
    ; So if macro $03 is needed, it's address $A060

.segment "BANKTBLS"
    ; Tables
    ; See docs for how this works but in brief,
    ; Each table is 4 effects x 16 rows
    ; There are 64 tables so each high byte of the
    ; address points to an upper/lower table.
    ; Or said another way, the "page" is the table
    ; number divided by 2 and the remainder determines
    ; if it's in the first 128 bytes or second.
    ; So if table $03 is needed, it's address A000
    ; If table $04 is needed, it's address A180
    
.segment "BANKENVS"
    ; Envelopes
    ; See docs for how this works but in brief,
    ; A000 = Env 0, position 0
    ; A001 = Env 0, position 1
    ; ...
    ; A100 = Env 1, position 0
    ; etc.

.segment "BANKVINS"
    ; Using literals here because not sure it works with variables
    ; using structs doesn't seem to work either
    ; Number of instruments: $80 (128)

    ; Space used: 
    ; names = 2048 (16 * 128)
    ; instrument data = 2304 (18 * 128)
    ; Remaining: 3840

    vera_instrument_names:                      .res ($80 * $10)  ; 128 * 16
    vera_instruments_vol:                       .res $80
    vera_instruments_fine_pitch:                .res $80
    vera_instruments_wave:                      .res $80
    
    ; Currently unused
    vera_instruments_flags:                     .res $80

    ; Env Flags
    ; bit 7: Enable Envelope
    ; bit 6: Loop Enable
    ; bit 5: Loop Direction

    vera_instruments_vol_envelope_num:          .res $80
    vera_instruments_vol_envelope_length:       .res $80
    vera_instruments_vol_envelope_flags:        .res $80
    vera_instruments_vol_envelope_loop_start:   .res $80
    vera_instruments_vol_envelope_loop_end:     .res $80
    vera_instruments_wave_envelope_num:         .res $80
    vera_instruments_wave_envelope_length:      .res $80
    vera_instruments_wave_envelope_flags:       .res $80
    vera_instruments_wave_envelope_loop_start:  .res $80
    vera_instruments_wave_envelope_loop_end:    .res $80
    vera_instruments_pitch_envelope_num:        .res $80
    vera_instruments_pitch_envelope_length:     .res $80
    vera_instruments_pitch_envelope_flags:      .res $80
    vera_instruments_pitch_envelope_loop_start: .res $80
    vera_instruments_pitch_envelope_loop_end:   .res $80
    vera_instruments_pitch_speed:               .res $80

    ; Speed env values
    ; At the bottom because these were additions and would change
    ; existing save files. Organizing it will require a conversion script.
    vera_instruments_vol_envelope_speed:       .res $80
    vera_instruments_wave_envelope_speed:       .res $80
    vera_instruments_pitch_envelope_speed:       .res $80

.segment "BANKFINS"
    ; YM2151 Instruments

    ; Space used: 
    ; names = 2048 (16 * 128)
    ; instrument data = 4096 (32 * 128)
    ; Remaining: 2048

    fm_instrument_names:          .res ($80 * $10)    ; 128 * 16


    fm_instruments_global_volume:               .res $80

    ; Mask to indicate global volume should
    ; influence operator volume
    ; Bit 7: M1
    ; Bit 6: M2
    ; Bit 5: C1
    ; Bit 4: C2
    fm_instruments_global_to_op_volume:         .res $80

    ; Bit 7-6: Right/Left
    ; Bit 5-3: M1 Feedback
    ; Bit 2-0: Connection Algorithm
    fm_instruments_pan_feedback_algo:               .res $80
 
    ; Bit 6-4: Phase Modulation Sensitivity
    ; Bit 1-0: Amplitude Modulation Sensitivity
    fm_instruments_modulation_sensitivity:          .res $80

    ; Bit 6-4: DT1 (Positive not attenuation!)
    ; Bit 3-0: MUL (Frequency Multiplier)
    fm_instruments_detune_fine_frequency_mul_m1:    .res $80
    fm_instruments_detune_fine_frequency_mul_m2:    .res $80
    fm_instruments_detune_fine_frequency_mul_c1:    .res $80
    fm_instruments_detune_fine_frequency_mul_c2:    .res $80
 
    ; Using volume here though the actual YM2151 chip
    ; uses attenuation ($7F = silent, $00 = full volume)
    ; Using volume here to keep things consistent with 
    ; how volume is handled in the tracker for other things.
    ; When writing to the real registers, the value is
    ; subtracted from $7F. 
    ; Bit 6-0: Volume
    fm_instruments_volume_m1:                    .res $80
    fm_instruments_volume_m2:                    .res $80
    fm_instruments_volume_c1:                    .res $80
    fm_instruments_volume_c2:                    .res $80

    ; Bit 7-6: Key-Scaling
    ; Bit 4-0: Attack Rate (Positive! Not Attenuation)
    fm_instruments_ks_attack_m1:                .res $80
    fm_instruments_ks_attack_m2:                .res $80
    fm_instruments_ks_attack_c1:                .res $80
    fm_instruments_ks_attack_c2:                .res $80

    ; Bit 7: AM Enable
    ; Bit 4-0: Decay Rate 1 (Positive! Not Attenuation)
    fm_instruments_am_decay1_m1:                .res $80
    fm_instruments_am_decay1_m2:                .res $80
    fm_instruments_am_decay1_c1:                .res $80
    fm_instruments_am_decay1_c2:                .res $80

    ; Bit 7-6: Coarse Detune
    ; Bit 4-0: Decay Rate 2 (Positive! Not Attenuation)
    fm_instruments_detune_coarse_decay2_m1:     .res $80
    fm_instruments_detune_coarse_decay2_m2:     .res $80
    fm_instruments_detune_coarse_decay2_c1:     .res $80
    fm_instruments_detune_coarse_decay2_c2:     .res $80

    ; Bit 7-4: Decay 1 (Sustain) (Positive! Not Attenuation)
    ; Bit 3-0: Release (Positive! Not Attenuation)
    fm_instruments_decay1_release_m1:           .res $80
    fm_instruments_decay1_release_m2:           .res $80
    fm_instruments_decay1_release_c1:           .res $80
    fm_instruments_decay1_release_c2:           .res $80

.popseg