; Clears channel registers
; such as when stopping a song

; This can be made more efficient with just a loop
; from the start of channel register addresses
; to the end
.proc init_channel_registers
@start:
    ldx #$00
@loop:
    lda #$FF
    sta sound::vera::channel_instrument,x
    ;lda #$10
    ;sta sound::vera::channel_note,x
    ; Middle A
    lda #$04
    sta sound::vera::channel_note,x
    lda #$9D
    sta sound::vera::channel_semitone,x
    lda #NOTENULL
    sta sound::vera::channel_glide_note,x
    sta sound::vera::channel_glide_semitone,x
    sta sound::vera::channel_arp_base_note,x
    sta sound::vera::channel_note_delay_note,x
    ; Store zeroes
    stz sound::vera::channel_vol,x
    stz sound::vera::channel_attenuation,x
    stz sound::vera::channel_wave,x
    stz sound::vera::channel_flags,x
    stz sound::vera::channel_fine_pitch,x
    stz sound::vera::channel_linear_fine_pitch,x
    stz sound::vera::channel_pitch_speed,x
    stz sound::vera::channel_vol_envelope_num,x
    stz sound::vera::channel_vol_envelope_flags,x
    stz sound::vera::channel_vol_envelope_position,x
    stz sound::vera::channel_arp_value,x
    stz sound::vera::channel_pitch_sweep_value,x
    stz sound::vera::channel_volume_sweep_value,x
    stz sound::vera::channel_pwm_sweep_value,x
    stz sound::vera::channel_glide_speed,x
    stz sound::vera::channel_vibrato_depth,x
    stz sound::vera::channel_tremolo_depth,x

    stz sound::vera::channel_pulsolo_loop_length,x
    stz sound::vera::channel_pulsolo_loop_position,x
    stz sound::vera::channel_pulsolo_depth,x
    stz sound::vera::channel_pulsolo_direction,x

    
    inx
    cpx #sound::vera::NUM_CHANNELS
    bne @loop
@end:
    rts

.endproc
