; Set channel volume including (LR)
; x: channel-number
; sound::vera::channel_vol,x: desired volume
; zp_MATH0: temp var for bit maths
; VERA stride must be disabled
.proc set_channel_volume_register
    lda sound::vera::channel_address_lut,x
    clc
    adc #VERA_PSG_VOLUME_REGISTER
    sta VERA_addr_low
    lda sound::vera::channel_vol,x  
    sta VERA_data0
    rts
.endproc