; Update a VERA channel's instrument
; Basically apply the defaults from the 
; instrument to the channel

; zp_ARG0 = channel
; zp_ARG1 = instrument

.proc update_channel_instrument
    CHANNEL = zp_ARG0
    INSTRUMENT = zp_ARG1
@start:
    push_rambank
    rambank zp_BANK_VINS

    ldy CHANNEL
    lda INSTRUMENT
    sta sound::vera::channel_instrument,y
    
    ldx INSTRUMENT
    lda vera_instruments_vol,x
    sta sound::vera::channel_vol,y
    lda vera_instruments_wave,x
    sta sound::vera::channel_wave,y
    lda vera_instruments_fine_pitch,x
    sta sound::vera::channel_fine_pitch,y
    lda vera_instruments_pitch_speed,x
    sta sound::vera::channel_pitch_speed,y

    ; Volume Env
    lda vera_instruments_vol_envelope_num,x
    sta sound::vera::channel_vol_envelope_num,y
    lda vera_instruments_vol_envelope_speed,x
    sta sound::vera::channel_vol_envelope_speed,y
    lda vera_instruments_vol_envelope_flags,x
    sta sound::vera::channel_vol_envelope_flags,y
    lda vera_instruments_vol_envelope_length,x
    sta sound::vera::channel_vol_envelope_length,y
    lda vera_instruments_vol_envelope_loop_start,x
    sta sound::vera::channel_vol_envelope_loop_start,y
    lda vera_instruments_vol_envelope_loop_end,x
    sta sound::vera::channel_vol_envelope_loop_end,y
    ; Wave (PWM) Env
    lda vera_instruments_wave_envelope_num,x
    sta sound::vera::channel_wave_envelope_num,y
    lda vera_instruments_wave_envelope_speed,x
    sta sound::vera::channel_wave_envelope_speed,y
    lda vera_instruments_wave_envelope_flags,x
    sta sound::vera::channel_wave_envelope_flags,y
    lda vera_instruments_wave_envelope_length,x
    sta sound::vera::channel_wave_envelope_length,y
    lda vera_instruments_wave_envelope_loop_start,x
    sta sound::vera::channel_wave_envelope_loop_start,y
    lda vera_instruments_wave_envelope_loop_end,x
    sta sound::vera::channel_wave_envelope_loop_end,y
    ; Pitch Env
    lda vera_instruments_pitch_envelope_num,x
    sta sound::vera::channel_pitch_envelope_num,y
    lda vera_instruments_pitch_envelope_speed,x
    sta sound::vera::channel_pitch_envelope_speed,y
    lda vera_instruments_pitch_envelope_flags,x
    sta sound::vera::channel_pitch_envelope_flags,y
    lda vera_instruments_pitch_envelope_length,x
    sta sound::vera::channel_pitch_envelope_length,y
    lda vera_instruments_pitch_envelope_loop_start,x
    sta sound::vera::channel_pitch_envelope_loop_start,y
    lda vera_instruments_pitch_envelope_loop_end,x
    sta sound::vera::channel_pitch_envelope_loop_end,y


    ; Reset Envelopes and Effect Registers
    lda #$00
    sta sound::vera::channel_arp_value,y
    sta sound::vera::channel_arp_position,y
    sta sound::vera::channel_vol_envelope_position,y
    sta sound::vera::channel_wave_envelope_position,y
    sta sound::vera::channel_pitch_envelope_position,y

    ; Restore previous bank
    pop_rambank

    rts
.endproc