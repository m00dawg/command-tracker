; Volume Sweep
;
; Sweep Volume Up or Down

; a = channel
; y = volume sweet value
.proc volume_sweep
volume_slide_value = zp_MATH0
volume_value = zp_MATH1
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value
    sta volume_slide_value
    and #%00111111
    sta volume_value
    bne @sweep
; Value is 0 so we make sure we turn off sweep
@zero_reset:
    sta sound::vera::channel_volume_sweep_value,x
    jmp (return)

; For sweep, we set the channel registers 
; and let the engine do the rest.
@sweep:
    ; Get the direction value
    bbs 6, volume_slide_value, @sweep_up
@sweep_down:
    ; Bit 4 of channel flags is Volume sweep direction
    lda sound::vera::channel_flags,x
    and #%11101111
    bra @set_sweep
@sweep_up:
    lda sound::vera::channel_flags,x
    ora #%00010000
@set_sweep:    
    sta sound::vera::channel_flags,x
    lda volume_value
    sta sound::vera::channel_volume_sweep_value,x
    jmp (return)
.endproc