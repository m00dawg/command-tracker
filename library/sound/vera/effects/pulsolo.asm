; Pulsolo Set
;
; This is pretty basic since the synth engine
; will handle the heavy lifting.
; This just sets the channel registers based
; on the LFO luts

; a = channel
; y = loop-length / depth (4-bit/4-bit)

.proc pulsolo
return = zp_ADDR_RETURN
value = zp_MATH0

@start:
    tax ; Channel #
    ;sty value

    ; If speed is zero, reset
    cpy #$00
    bne @continue
    stz sound::vera::channel_pulsolo_depth,x
    stz sound::vera::channel_pulsolo_loop_length,x
    stz sound::vera::channel_pulsolo_loop_position,x
    stz sound::vera::channel_pulsolo_direction,x
    jmp (return)

@continue:
    ; Check if pulsolo was previously set 
    ; and bail if so
    ;lda sound::vera::channel_pulsolo_depth,x
    ;4abne @end
    ; Loop Length (Speed-ish?)
    tya
    lsr4
    sta sound::vera::channel_pulsolo_loop_length,x

    ; Jump halfway into ramp up
    ;lda sound::vera::channel_pulsolo_loop_length,x
    ; Divide by 2
    lsr
    sta sound::vera::channel_pulsolo_loop_position,x
    ;stz sound::vera::channel_pulsolo_loop_position,x

    ; Depth
    tya
    ;lda value
    and #%00001111  
    sta sound::vera::channel_pulsolo_depth,x

@end:
    jmp (return)
.endproc