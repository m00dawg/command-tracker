; Pitch Sweep Down
;
; This is intended to be called from 
; the pattern/row playback

; a = channel
; y = pitch sweep value

; Unlike pitch slide, sweep runs at synth speed
; and is controlled by the tick ISR. 
; More precise and useful for kicks and things

.proc pitch_sweep_down
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value
    sta sound::vera::channel_pitch_sweep_value,x
    lda sound::vera::channel_flags,x
    ; turn off bit 6 = sweep direction
    and #%11011111
    sta sound::vera::channel_flags,x
    jmp (return)
.endproc