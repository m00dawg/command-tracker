; Channel Attenuation
;
; Set channel attenuation

; a = channel
; y = attenuation

.proc channel_attenuation
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value
    sta sound::vera::channel_attenuation,x
    jmp (return)
.endproc