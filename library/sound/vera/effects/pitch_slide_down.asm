; Pitch Slide Down
;
; This is intended to be called from 
; the pattern/row playback

; a = channel
; y = pitch slide value

.proc pitch_slide_down
channel_pitch = zp_MATH0
channel_glide_pitch = zp_MATH1
pitch_slide_value = zp_MATH2
result = zp_MATH3
return = zp_ADDR_RETURN

@start:
    tax ; Channel #

    lda sound::vera::channel_semitone,x
    sta channel_pitch 
    lda sound::vera::channel_note,x
    sta channel_pitch + 1

    tya ; value
    sta pitch_slide_value
    sub16from8 channel_pitch, pitch_slide_value, result
    lda result 
    sta sound::vera::channel_semitone,x
    lda result + 1
    sta sound::vera::channel_note,x

; Do the same for glide so it can follow these changes as it glides
    lda sound::vera::channel_glide_semitone,x
    sta channel_glide_pitch 
    lda sound::vera::channel_glide_note,x
    sta channel_glide_pitch + 1

    tya ; value
    sta pitch_slide_value
    sub16from8 channel_glide_pitch, pitch_slide_value, result
    lda result 
    sta sound::vera::channel_glide_semitone,x
    lda result + 1
    sta sound::vera::channel_glide_note,x


    jmp (return)
.endproc
