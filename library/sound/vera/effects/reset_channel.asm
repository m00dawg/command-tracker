; Channel Reset
;
; Clear all the persistent registers
; (such as vibrato)
; a = channel

.proc reset_channel
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    lda #INSTNULL
    sta sound::vera::channel_instrument,x
    lda #NOTENULL
    sta sound::vera::channel_arp_base_note,x
    sta sound::vera::channel_note_delay_note,x

    stz sound::vera::channel_flags,x
    stz channel_instrument_flags,x
    stz sound::vera::channel_vol,x
    stz sound::vera::channel_attenuation,x
    stz sound::vera::channel_wave,x
    stz sound::vera::channel_fine_pitch,x
    stz sound::vera::channel_linear_fine_pitch,x
    stz sound::vera::channel_pitch_speed,x
    stz sound::vera::channel_arp_position,x
    stz sound::vera::channel_arp_value,x
    stz sound::vera::channel_arp_dirspeed,x
    stz sound::vera::channel_arp_speed_counter,x
    stz sound::vera::channel_pitch_sweep_value,x
    stz sound::vera::channel_volume_sweep_value,x
    stz sound::vera::channel_pwm_sweep_value,x
    stz sound::vera::channel_glide_speed,x
    stz sound::vera::channel_glide_note,x
    stz sound::vera::channel_glide_semitone,x
    stz sound::vera::channel_note_delay,x
    stz sound::vera::channel_vibrato_loop_length,x
    stz sound::vera::channel_vibrato_loop_position,x
    stz sound::vera::channel_vibrato_depth,x
    stz sound::vera::channel_tremolo_loop_length,x
    stz sound::vera::channel_tremolo_loop_position,x
    stz sound::vera::channel_tremolo_depth,x
    stz sound::vera::channel_pulsolo_loop_length,x
    stz sound::vera::channel_pulsolo_loop_position,x
    stz sound::vera::channel_pulsolo_depth,x
    jmp (return)
.endproc
