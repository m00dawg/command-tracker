; Enable/disable Legato

; bit 2 is legato, see sound::vera

; a = channel
; y = legato enable/disable
.proc legato
	tax
	lda sound::vera::channel_flags,x
	cpy #$00
  beq @disable_legato
@enable_legato:
  ora #%00000100
  sta sound::vera::channel_flags,x
  rts
@disable_legato:
  and #%11111011
  sta sound::vera::channel_flags,x
  rts
.endproc
