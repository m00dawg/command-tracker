; Finetune
;
; Sets the channel finetune value

; a = channel
; y = finetune

.proc finetune
return = zp_ADDR_RETURN
NOTE_SEMITONE = zp_MATH0

@start:
    tax ; Channel #
    tya ; value
    sta sound::vera::channel_fine_pitch,x
    jmp (return)
.endproc