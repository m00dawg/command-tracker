; Arpeggiator Speed
;
; This is pretty basic since the synth engine
; will handle the heavy lifting.
; This sets the direction and speed of the arp. 
; If high nibble is >0, direction is reversed
; Low nibble controlls speed, higher values
; mean slower arps, a bit like how normal song speed works 

; a = channel
; y = arp direction / speed

.proc arp_dirspeed
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value
    sta sound::vera::channel_arp_dirspeed,x
    stz sound::vera::channel_arp_speed_counter,x
    jmp (return)
.endproc