; PWM Set
;
; This is intended to be called from 
; the pattern/row playback

; a = channel
; y = PWM Value

; This is basic and coarse since it's
; hard steps each time if called
; from the pattern.
; May be better to have some way to set state and
; have the sound engine do something per tick
.proc pwm_set_value
pwm_value = zp_MATH0
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value
    sta pwm_value

    lda sound::vera::channel_wave,x
    and #%11000000
    ora pwm_value
    sta sound::vera::channel_wave,x
    
    jmp (return)
.endproc