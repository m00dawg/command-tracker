; Linear Finetune
;
; Sets the channel linear finetune value

; a = channel
; y = finetune

.proc linear_finetune
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value
    sta sound::vera::channel_linear_fine_pitch,x
    jmp (return)
.endproc