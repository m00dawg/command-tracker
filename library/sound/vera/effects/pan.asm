; Channel Pan
;
; Set channel pan (L/R/C)

; a = channel
; y = pan

.proc pan
return = zp_ADDR_RETURN

@start:
    tax ; Channel #

    cpy #PAN_LEFT
    beq @left
    cpy #PAN_RIGHT
    beq @right
@center:
    lda sound::vera::channel_vol,x
    ora #%11000000
    bra @store
@left:
    lda sound::vera::channel_vol,x
    and #%00111111
    ora #%01000000
    bra @store

@right:
    lda sound::vera::channel_vol,x
    and #%00111111
    ora #%10000000

@store:
    sta sound::vera::channel_vol,x
    jmp (return)
.endproc