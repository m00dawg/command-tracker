; Volume Slide
;
; Slide or Volume Up or Down

; a = channel
; y = volume slide value
.proc volume_slide
volume_slide_value = zp_MATH0
volume_value = zp_MATH1
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value
    sta volume_slide_value
    and #%00111111
    sta volume_value

; For slide, we set the pitch directly and don't
; mess with channel state since it's a one-shot
@slide:
    ; Check direction
    bbs 6, volume_slide_value, @slide_up
@slide_down:
    lda sound::vera::channel_vol,x
    and #%00111111
    sec
    sbc volume_value
    cmp #%01000000
    bge @slide_down_rollover
    bra @slide_end
@slide_down_rollover:
    lda #$00
    bra @slide_end
@slide_up:
    lda sound::vera::channel_vol,x
    and #%00111111
    adda volume_value
    cmp #%01000000
    blt @slide_end
@slide_up_rollover:
    lda #%00111111
@slide_end:
    and #%00111111
    sta volume_value

    lda sound::vera::channel_vol,x
    and #%11000000
    ora volume_value
    sta sound::vera::channel_vol,x
    jmp (return)

.endproc