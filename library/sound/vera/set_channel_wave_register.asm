; Set channel wave including (wave-mode)
; x: channel-number
; sound::vera::channel_wave,x: desired wave/pwm
; zp_MATH0: temp var for bit maths
; VERA stride must be disabled
.proc set_channel_wave_register
    lda sound::vera::channel_address_lut,x
    clc
    adc #VERA_PSG_WAVE_REGISTER
    sta VERA_addr_low
    lda sound::vera::channel_wave,x  
    sta VERA_data0
    rts
.endproc