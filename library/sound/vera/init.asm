; Set all registers to zero
; Basically loop from $1F9C0 to $1F9FF
; and store 0's

.proc init
  
  ; Setup for writing to vera fast
  ;lda #%00010001
  ;sta VERA_addr_high
  ;lda #VERA_PSG_HIGH_BYTE
  ;sta VERA_addr_med
  ;ldx #VERA_PSG_CHANNEL_BASE_LOW_BYTE
  ;stx VERA_addr_low
;@loop:
;  stz VERA_data0
;  inx
;  bne @loop
;@end:

  rombank #AUDIO_ROM_BANK
  jsr psg_init
  jsr init_channel_registers

  ; Pull channel types 
  push_rambank
  rambank zp_BANK_MISC
  ldx #$00
@channel_type_loop:
  lda psg_channel_modes,x
  sta channel_mode,x
  inx
  cpx #NUM_CHANNELS
  bne @channel_type_loop
  pop_rambank

  rts
.endproc