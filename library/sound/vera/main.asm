 ; Vera specific stuff
  .scope vera



    ; Constants
    NUM_CHANNELS = $10
    START_CHANNEL = $00

    MAX_SHARED_EFFECT = $15  ; Includes global effects, see data.inc
    MAX_UNIQUE_EFFECT = $61  ; Includes global effects, see data.inc

    REGISTER_EFFECTS_START = $40
    REGISTER_EFFECTS_STOP = $43
    ; After the direct register effects
    UNIQUE_EFFECTS_START = $44

    VOLUME_COLUMN_LEGATO = $E0

    HIGH_BYTE = $F9
    CHANNEL_BASE_LOW_BYTE = $C0
    LOW_FREQ_REGISTER = $00
    HIGH_FREQ_REGISTER = $01
    VOLUME_REGISTER = $02
    WAVE_REGISTER = $03

    BASE_NAMES_ADDRESS = $A000
    BASE_CHANNELS_ADDRESS = $A800
    BASE_CHANNELS_SIZE = $1000
    NUM_INSTRUMENTS = $80

    PAN_CENTER = $00
    PAN_LEFT = $01
    PAN_RIGHT = $02

    ; Constants for channel mode
    PSG = $00
    WAVETABLE = $01
    MIDI = $02

; Place holders to asses memory usage of potential new features
    ; Mutes
    channel_mute_mask:                 .word $0000

    ; Channel type (0 = PSG, 1 = WT, 2 = Ext MIDI)
    ; When WT or MIDI, see sound::midi memory defs
    channel_mode:                      .byte $00,$01,$02,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

    ; Tables
    channel_table:                     .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
    channel_table_speed:               .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
    channel_table_delay_counter:       .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
    ; If position > row size (16), it means table is stopped    
    channel_table_position:            .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF

    ; Channel registers
    channel_instrument:                .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    
    ; bit 7: active, rest are NOPs ATM
    ; bit 6: release
    ; bit 5: pitch sweep direction
    ; bit 4: volume sweep direction
    ; bit 3: pwm sweep direction
    ; bit 2: legato
    channel_flags:                     .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

    ; Yet to be implemented. (see bank.inc)
    ; Proposed:
    ; bit 7: Fine pitch mode
    ; bit 6: pitch sweep loop
    ; bit 5: volume sweep loop
    ; bit 4: pwm sweep loop
    channel_instrument_flags:          .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

    ; Note.Semitone style representation to align VERA to FM behavior
    channel_note:                      .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    ; Fractional Semitone Component
    ; (note.semitone)
    channel_semitone:                  .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

    ; Top bits are pan
    channel_vol:                       .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    
    channel_attenuation:               .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_wave:                      .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

    channel_fine_pitch:                .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    
    ; This is to add/subtract the final pitch in VERA. This is linear instead of log but is 
    ; a way to do certain effects not possible with the current granularity of fine pitch
    channel_linear_fine_pitch:        .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

    ; Channel pitch speed - number of left shifts of pitch value for
    ; pitch envelopes, pitch sweeps, and glide
    channel_pitch_speed:               .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

    ; Envelopes
    ; Envelope Flags:
    ; Bit 7 - Enabled
    ; Bit 6 - Loop Enabled
    ; Bit 5 - Loop Mode (FWD/Ping-Pong)
    ; Bit 4 - Current Loop Direction
    channel_vol_envelope_num:             .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_vol_envelope_flags:           .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_vol_envelope_length:          .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_vol_envelope_loop_start:      .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_vol_envelope_loop_end:        .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_vol_envelope_position:        .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_vol_envelope_speed:           .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_vol_envelope_speed_counter:   .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    
    channel_wave_envelope_num:            .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_wave_envelope_flags:          .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_wave_envelope_length:         .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_wave_envelope_loop_start:     .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_wave_envelope_loop_end:       .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_wave_envelope_position:       .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_wave_envelope_speed:          .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_wave_envelope_speed_counter:  .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

    channel_pitch_envelope_num:           .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_pitch_envelope_flags:         .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_pitch_envelope_length:        .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_pitch_envelope_loop_start:    .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_pitch_envelope_loop_end:      .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_pitch_envelope_position:      .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_pitch_envelope_speed:         .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_pitch_envelope_speed_counter: .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

    ; Effect Registers

    ; Arp
    channel_arp_position:              .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_arp_value:                 .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_arp_base_note:             .byte NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL
    ; Higher number = slower (how many pauses before going to the next arp)
    ; At present is first nibble is >0, arp in reverse
    channel_arp_dirspeed:              .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_arp_speed_counter:         .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

    ; Sweeps
    channel_pitch_sweep_value:         .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_volume_sweep_value:        .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_pwm_sweep_value:           .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    
    ; Glide
    channel_glide_speed:               .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_glide_semitone:            .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_glide_note:                .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

    ; Note delay
    channel_note_delay:               .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_note_delay_note:          .byte NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL,NOTENULL

    ; LFOs / -ato's (Vibrato, etc.)
    channel_vibrato_loop_length:       .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_vibrato_loop_position:     .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 
    channel_vibrato_direction:         .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_vibrato_depth:             .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_tremolo_loop_length:       .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_tremolo_loop_position:     .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 
    channel_tremolo_direction:         .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_tremolo_depth:             .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_pulsolo_loop_length:       .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_pulsolo_loop_position:     .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 
    channel_pulsolo_direction:         .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    channel_pulsolo_depth:             .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

    ;.segment "RODATA"
    ; Look-up-table to map tracker channels to
    ; Vera channel start addresses ($C0, C4, etc.)
    channel_address_lut:
      .byte $C0, $C4, $C8, $CC, $D0, $D4, $D8, $DC
      .byte $E0, $E4, $E8, $EC, $F0, $F4, $F8, $FC

    ; How long the looped triangle ends up being
    ; Original values
    ;channel_virbato_length_lut:
    ;  .byte $60, $5A, $54, $4E, $48, $42, $3C, $36
    ;  .byte $30, $2A, $24, $1E, $18, $12, $0C, $06

    ;channel_virbato_length_lut:
    ;  .byte $20, $1E, $1C, $1A, $18, $16, $14, $12
    ;  .byte $10, $0E, $0C, $0A, $08, $06, $04, $02

    ;channel_virbato_length_lut:
    ;  .byte $10, $0F, $0E, $0D, $0C, $0B, $0A, $09
    ;  .byte $08, $07, $06, $05, $04, $03, $02, $01


    ; How long the looped triangle ends up being 
    ; for Tremolo and Pulsolo
    ;channel_trempulsolo_length_lut:
      ;.byte $40, $3C, $38, $34, $30, $2C, $38, $34
      ;.byte $20, $1C, $18, $14, $10, $0C, $08, $04

      ;.byte $20, $1E, $1C, $1A, $18, $16, $14, $12
      ;.byte $10, $0E, $0C, $0A, $08, $06, $04, $02
    ;  .byte $10, $0F, $0E, $0D, $0C, $0B, $0A, $09
    ;  .byte $08, $07, $06, $05, $04, $03, $02, $01
      
      ;.byte $30, $2D, $2A, $27, $24, $21, $1E, $1B
      ;.byte $18, $15, $12, $0F, $0C, $09, $06, $03
 
    .include "library/sound/vera/update_channel_instrument.asm"
    .include "library/sound/vera/init_channel_registers.asm"
    .include "library/sound/vera/update_channel_registers.asm"
    .include "library/sound/vera/init.asm"
    .include "library/sound/vera/set_channel_volume_register.asm"
    .include "library/sound/vera/set_channel_wave_register.asm"
    ;.include "library/sound/vera/set_channel_pitch_register.asm"
    .include "library/sound/vera/update_note.asm"
    

    .scope effects
      .include "library/sound/vera/effects/pitch_slide_down.asm"
      .include "library/sound/vera/effects/pitch_slide_up.asm"
      .include "library/sound/vera/effects/pitch_sweep_down.asm"
      .include "library/sound/vera/effects/pitch_sweep_up.asm"
      .include "library/sound/vera/effects/volume_slide.asm"
      .include "library/sound/vera/effects/volume_sweep.asm"
      .include "library/sound/vera/effects/pwm_slide.asm"
      .include "library/sound/vera/effects/pwm_sweep.asm"
      .include "library/sound/vera/effects/pwm_set_value.asm"
      .include "library/sound/vera/effects/arpeggio.asm"
      .include "library/sound/vera/effects/arp_dirspeed.asm"
      .include "library/sound/vera/effects/glide.asm"
      .include "library/sound/vera/effects/legato.asm"
      .include "library/sound/vera/effects/vibrato.asm"
      .include "library/sound/vera/effects/tremolo.asm"
      .include "library/sound/vera/effects/pulsolo.asm"
      .include "library/sound/vera/effects/reset_channel.asm"
      .include "library/sound/vera/effects/pitch_speed.asm"
      .include "library/sound/vera/effects/update_channel_register.asm"
      .include "library/sound/vera/effects/note_delay.asm"
      .include "library/sound/vera/effects/finetune.asm"
      .include "library/sound/vera/effects/linear_finetune.asm"
      .include "library/sound/vera/effects/vol_env_speed.asm"
      .include "library/sound/vera/effects/wave_env_speed.asm"
      .include "library/sound/vera/effects/pitch_env_speed.asm"
      .include "library/sound/vera/effects/channel_attenuation.asm"
      .include "library/sound/vera/effects/pan.asm"
    .endscope
  .endscope
  