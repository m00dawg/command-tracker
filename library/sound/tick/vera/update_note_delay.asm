; Update note delay and play note at end of delay
; x = Channel

.proc update_note_delay
	lda sound::vera::channel_note_delay_note,x
	cmp #NOTENULL
	bne @continue
	rts
@continue:
	dec sound::vera::channel_note_delay,x
	lda sound::vera::channel_note_delay,x
	beq @play
	rts
@play:
	lda sound::vera::channel_note_delay_note,x
	tay
	lda #NOTENULL
	sta sound::vera::channel_note_delay_note,x
	jsr sound::vera::update_note
	rts
.endproc