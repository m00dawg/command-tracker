; Update pulsolo for channel, if required
; x = Channel
.proc update_pulsolo
pwm = EFFECT_TEMP1_VALUE
pulsolo_depth = EFFECT_TEMP2_VALUE
length = EFFECT_TEMP3_VALUE

@start:
    ; Only update pulsolo if depth is non-zero
    lda sound::vera::channel_pulsolo_depth,x
    bne @get_registers
    rts

; Get current PWM and depth for later on
@get_registers:
    and #%00111111
    sta pulsolo_depth
    lda sound::vera::channel_wave,x
    and #%00111111
    sta pwm

@evaluate_pulsolo:
; Look at the direction
; if 0, ramp up (add)
; if 1, ramp down (subtract)
    lda sound::vera::channel_pulsolo_direction,x
    bne @subtract_pwm
@add_pwm:
    add pwm, pulsolo_depth
    cmp #%00111111
    bge @set_add_max
    bra @store_pwm
@set_add_max:
    lda #%00111111
    bra @store_pwm
@subtract_pwm:
    sub pwm, pulsolo_depth
    bmi @set_sub_min
    bra @store_pwm
@set_sub_min:
    lda #%00000000
    ;bra @store_pwm
@store_pwm:
    sta pwm
    lda sound::vera::channel_wave,x
    and #%11000000
    ora pwm 
    sta sound::vera::channel_wave,x
    
@update_loop_position:
    lda sound::vera::channel_pulsolo_loop_position,x
    cmp sound::vera::channel_pulsolo_loop_length,x
    bne @store_loop_position
@flip_direction:
    lda sound::vera::channel_pulsolo_direction,x
    eor #%10000000
    sta sound::vera::channel_pulsolo_direction,x
    stz sound::vera::channel_pulsolo_loop_position,x
    rts
@store_loop_position:
    inc sound::vera::channel_pulsolo_loop_position,x
    rts
.endproc