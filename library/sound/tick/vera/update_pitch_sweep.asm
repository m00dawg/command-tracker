; Update pitch sweep for channel, if required
; x = Channel

.proc update_pitch_sweep
    channel_pitch = EFFECT_TEMP0_VALUE
    channel_glide_pitch = EFFECT_TEMP1_VALUE
    sweep_value = EFFECT_TEMP2_VALUE
    channel_flags = EFFECT_TEMP3_VALUE
    result = EFFECT_TEMP4_VALUE
    glide_result = EFFECT_TEMP5_VALUE

    ; Only update sweep if it's non-zero
    lda sound::vera::channel_pitch_sweep_value,x
    bne @check_pitch_speed
    rts

@check_pitch_speed:
    ; Shift by the sweep speed
    sta sweep_value
    stz sweep_value + 1

    lda sound::vera::channel_pitch_speed,x
    tay
    beq @fetch_channel_pitch
@pitch_speed_loop:
    asl16 sweep_value
    dey
    bne @pitch_speed_loop

@fetch_channel_pitch:
    lda sound::vera::channel_semitone,x
    sta channel_pitch
    lda sound::vera::channel_note,x
    sta channel_pitch + 1
    lda sound::vera::channel_glide_semitone,x
    sta channel_glide_pitch
    lda sound::vera::channel_glide_note,x
    sta channel_glide_pitch + 1

@sweep_direction:
    lda sound::vera::channel_flags,x
    sta channel_flags
    ; Bit 5 of channel flags is pitch (see sound.asm)
    bbr 5, channel_flags, @sweep_down
@sweep_up:
    ;add16to8 channel_pitch, sweep_value, result
    add16 channel_pitch, sweep_value, result
    add16 channel_glide_pitch, sweep_value, glide_result
    jmp @update_channel
@sweep_down:
    ;sub16from8 channel_pitch, sweep_value, result
    sub16 channel_pitch, sweep_value, result
    sub16 channel_glide_pitch, sweep_value, glide_result
@update_channel:
    lda result
    sta sound::vera::channel_semitone,x
    lda result + 1
    sta sound::vera::channel_note,x
    lda glide_result
    sta sound::vera::channel_glide_semitone,x
    lda glide_result + 1
    sta sound::vera::channel_glide_note,x
@end:
    rts
.endproc
