.scope vera

.include "library/sound/tick/vera/update_volume_env.asm"
.include "library/sound/tick/vera/update_wave_env.asm"
.include "library/sound/tick/vera/update_pitch_env.asm"
.include "library/sound/tick/vera/update_pitch_sweep.asm"
.include "library/sound/tick/vera/update_volume_sweep.asm"
.include "library/sound/tick/vera/update_pwm_sweep.asm"
.include "library/sound/tick/vera/update_arp.asm"
.include "library/sound/tick/vera/update_glide.asm"
.include "library/sound/tick/vera/update_vibrato.asm"
.include "library/sound/tick/vera/update_tremolo.asm"
.include "library/sound/tick/vera/update_pulsolo.asm"
.include "library/sound/tick/vera/update_note_delay.asm"

.endscope