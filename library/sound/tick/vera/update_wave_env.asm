; Update wave envelope for channel, if required

.proc update_wave_env
    ; Only update wave envelope if it's enabled
    lda sound::vera::channel_wave_envelope_flags,x
    sta FLAGS
    bbs 7, FLAGS,@check_speed_nonzero
    rts

; If speed is >0, we need to wait X ticks
@check_speed_nonzero:
    lda sound::vera::channel_wave_envelope_speed,x
    beq @check_end_of_env
; See if speed counter is zero. If so, check evalute env
; otherwise decrement
@evalute_speed:
    lda sound::vera::channel_wave_envelope_speed_counter,x
    beq @speed_reached
    dec sound::vera::channel_wave_envelope_speed_counter,x
    rts
@speed_reached:
    lda sound::vera::channel_wave_envelope_speed,x
    sta sound::vera::channel_wave_envelope_speed_counter,x

; If we aren't looping, check if we're at the end of the 
; specified length and stop
@check_end_of_env:
    ; Check if we've reached the 
    ; specified length to evaluate the env
    ; otherwise increment envelope
    lda sound::vera::channel_wave_envelope_position,x
    ; If we're at the envelope length, do not progress further
    cmp sound::vera::channel_wave_envelope_length,x
    blt @update_envelope
    rts

@update_envelope:
    sta ENV_POSITION_ADDRESS
    ; Add the env number to the BRAM base
    lda sound::vera::channel_wave_envelope_num,x
    adda #BANK_ADDRESS_TOP
    sta ENV_POSITION_ADDRESS + 1
    ; Load value at env # and position
    lda (ENV_POSITION_ADDRESS)
    ; Mask out wave
    ; Later on we'll want a flag for this
    and #%00111111
    sta ENV_TEMP_VALUE
    lda sound::vera::channel_wave,x  
    and #%11000000
    ora ENV_TEMP_VALUE
    sta sound::vera::channel_wave,x  

; Evaluate for next pass
@evaluate_next_value:
; Check if loop is enabled
@check_loop_enable:
    lda sound::vera::channel_wave_envelope_flags,x
    sta FLAGS
    bbr 6, FLAGS, @inc_envelope
; Check if note-rel
@loop_enabled_check_note_rel:
    lda sound::vera::channel_flags,x
    sta FLAGS
    bbs 6, FLAGS, @inc_envelope
; Loop is enabled and we're not in a note release 
; Check if forward only or ping-pong
@check_loop_type:
    lda sound::vera::channel_wave_envelope_flags,x
    sta FLAGS
    bbr 5, FLAGS, @forward_loop
; Ping-pong loop
@ping_pong_loop:
    ; Check loop direction (0 = forward, 1 = reverse)
    lda sound::vera::channel_wave_envelope_flags,x
    sta FLAGS
    bbs 4, FLAGS, @ping_pong_reverse
@ping_pong_forward:
    lda sound::vera::channel_wave_envelope_position,x
    cmp sound::vera::channel_wave_envelope_loop_end,x
    beq @ping_pong_reset_backwards
    bra @inc_envelope 
@ping_pong_reset_backwards:
    lda sound::vera::channel_wave_envelope_flags,x
    ora #%00010000
    sta sound::vera::channel_wave_envelope_flags,x
    lda sound::vera::channel_wave_envelope_position,x
    rts
@ping_pong_reverse:
    lda sound::vera::channel_wave_envelope_position,x
    cmp sound::vera::channel_wave_envelope_loop_start,x
    beq @ping_pong_reset_forwards
    dec sound::vera::channel_wave_envelope_position,x
    rts
@ping_pong_reset_forwards:
    lda sound::vera::channel_wave_envelope_flags,x
    and #%11101111
    sta sound::vera::channel_wave_envelope_flags,x
    lda sound::vera::channel_wave_envelope_position,x
    rts
; Forward only loop
@forward_loop:
    ; For now we only do forward, so if we reach the end
    ; of the loop end, set position to loop start
    lda sound::vera::channel_wave_envelope_position,x
    cmp sound::vera::channel_wave_envelope_loop_end,x
    beq @reset_loop
    bra @inc_envelope
@reset_loop:
    lda sound::vera::channel_wave_envelope_loop_start,x
    sta sound::vera::channel_wave_envelope_position,x
    rts


@inc_envelope:
    inc sound::vera::channel_wave_envelope_position,x
@end:
    rts
.endproc