; Update PWM sweep for channel, if required
; x = Channel
.proc update_pwm_sweep
channel_pwm = EFFECT_TEMP1_VALUE
sweep_value = EFFECT_TEMP2_VALUE
channel_flags = EFFECT_TEMP3_VALUE

@start:
    ; Only update sweep if it's non-zero
    lda sound::vera::channel_pwm_sweep_value,x
    sta sweep_value
    beq @end
@sweep_direction:
    lda sound::vera::channel_flags,x
    sta channel_flags
    ; Bit 3 of channel flags is PWM (see sound.asm)
    bbr 3, channel_flags, @sweep_down
@sweep_up:
    lda sound::vera::channel_wave,x
    and #%00111111
    adda sweep_value
    sta sweep_value
    ; If bit 6 is set, we overflowed
    bbs 6, sweep_value, @max_sweep_up
    jmp @update_channel
@max_sweep_up:
    lda #%00111111
    jmp @update_channel
@sweep_down:
    lda sound::vera::channel_wave,x
    and #%00111111
    suba sweep_value
    sta sweep_value
    bbs 6, sweep_value, @max_sweep_down
    jmp @update_channel
@max_sweep_down:
    lda #%00000000
@update_channel:
    and #%00111111
    sta sweep_value
    lda sound::vera::channel_wave,x
    and #%11000000
    ora sweep_value
    sta sound::vera::channel_wave,x
@end:
    rts
.endproc
