; Update pitch envelope for channel, if required
; x = Channel
.proc update_glide
channel_pitch = zp_MATH0
glide_target_pitch = zp_MATH1
glide_speed = zp_MATH2
result = zp_MATH3
channel = zp_MATH4
update_note = UPDATE_FM_NOTE

@start:
    ; Only update glide if it's non-zero
    lda sound::fm::channel_glide_speed,x
    sta glide_speed
    bne @continue
    ; If glide isn't enabled, set glide to note
    ; (so we can start gliding whenever we want)
    lda sound::fm::channel_semitone,x
    sta sound::fm::channel_glide_semitone,x
    lda sound::fm::channel_note,x
    sta sound::fm::channel_glide_note,x
    rts

@continue:
    stx channel
    ; Grab the current pitch
    lda sound::fm::channel_semitone,x
    sta channel_pitch
    lda sound::fm::channel_note,x
    sta channel_pitch + 1

    ; Grab the pitch of the glide note
    lda sound::fm::channel_glide_semitone,x
    sta glide_target_pitch
    lda sound::fm::channel_glide_note,x
    sta glide_target_pitch + 1

; Compare which pitch is larger as this tells us which
; direction to glide
; 16-bit comparison taken from http://6502.org/tutorials/compare_beyond.html#3
@direction_to_glide:
    lda channel_pitch + 1   ; compare high bytes
    cmp glide_target_pitch + 1    
    bcc @pitch_up           ; channel < glide
    bne @pitch_down         ; channel != glide (thus channel >= glide)
    lda channel_pitch
    cmp glide_target_pitch
    bcc @pitch_up            ; channel < glide

; if channel pitch is larger, channel pitch needs to go down
@pitch_down:
    sub16from8 channel_pitch, glide_speed, result       
    lda result + 1
    cmp glide_target_pitch + 1
    bcc @set_to_glide       ; result < glide target, overshot
    bne @store_result       ; result != glide target, result >= glide, use result
    lda result
    cmp glide_target_pitch
    bcc @set_to_glide       ; result < glide, overshot
    bra @store_result

; If the glide value is larger, the channel pitch needs to go up
@pitch_up:
    ; Add the speed to the pitch
    add16to8 channel_pitch, glide_speed, result
    ; if greater than glide_target_pitch, overshot so set to glide
    lda result + 1
    cmp glide_target_pitch + 1
    bcc @store_result       ; result < glide target, use result
    bne @set_to_glide       ; result != glide, result >= glide, overshot
    lda result
    cmp glide_target_pitch
    bcc @store_result       ; result < glide, use result
    ;bra @set_to_glide

; Store glide or result
@set_to_glide:
    lda glide_target_pitch
    sta sound::fm::channel_semitone,x
    lda glide_target_pitch + 1
    sta sound::fm::channel_note,x
    bra @end
@store_result:
    lda result
    sta sound::fm::channel_semitone,x
    lda result + 1
    sta sound::fm::channel_note,x

@end:
    ;lda #$01
    inc update_note
    rts
.endproc