; Update note delay and play note at end of delay
; x = Channel

.proc update_note_delay
update_note = UPDATE_FM_NOTE
	lda sound::fm::channel_note_delay_note,x
	cmp #NOTENULL
	bne @continue
	rts
@continue:
	; dec sets z flag so we can immediately compare
	; as we are decrementing down to 0
	dec sound::fm::channel_note_delay,x
	beq @play
	rts
@play:
	lda sound::fm::channel_note_delay_note,x
	sta sound::fm::channel_note,x
	stz sound::fm::channel_semitone,x

	lda #NOTENULL
	sta sound::fm::channel_note_delay_note,x
	stz sound::fm::channel_note_delay,x
	
	; We don't use the update_note flag here because
	; we're already playing the note
	;inc update_note
	
	lda sound::fm::channel_flags,x
	ora #%10000000
	and #%10011111
	sta sound::fm::channel_flags,x
	sta zp_NOTE_FLAGS

	stx zp_CHANNEL_COUNT
	jsr sound::fm::update_note
	jsr sound::fm::key_control

	


	
	rts
.endproc