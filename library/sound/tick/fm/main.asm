.scope fm

  .include "library/sound/tick/fm/update_pitch_sweep.asm"
  .include "library/sound/tick/fm/update_glide.asm"
  .include "library/sound/tick/fm/update_note_delay.asm"


.endscope