.scope tick
FLAGS = zp_TMP0
ENV_POSITION_ADDRESS = zp_ADDR0
ENV_TEMP_VALUE = zp_MATH0
EFFECT_TEMP0_VALUE = zp_MATH0
EFFECT_TEMP1_VALUE = zp_MATH1
EFFECT_TEMP2_VALUE = zp_MATH2
EFFECT_TEMP3_VALUE = zp_MATH3
EFFECT_TEMP4_VALUE = zp_MATH4
EFFECT_TEMP5_VALUE = zp_MATH5


UPDATE_FM_NOTE = zp_ARG0

.include "library/sound/tick/fm/main.asm"
.include "library/sound/tick/vera/main.asm"

; Update internal state of the sound engine based on the base timing
; (Presumably vsync)
.proc update
@start:
	push_rambank
	rambank zp_BANK_ENVS

	lda #%00010001
	sta VERA_addr_high
	lda #sound::vera::HIGH_BYTE
	sta VERA_addr_med
	
	ldx #$00
@vera_channels_loop:
	jsr vera::update_volume_env
	jsr vera::update_wave_env
	jsr vera::update_arp
	jsr vera::update_pitch_sweep
	jsr vera::update_volume_sweep
	jsr vera::update_pwm_sweep
	jsr vera::update_glide
	jsr vera::update_vibrato
	jsr vera::update_tremolo
	jsr vera::update_pulsolo
	jsr vera::update_pitch_env
	jsr vera::update_note_delay
	jsr sound::vera::update_channel_registers
  jsr sound::zsm::write_vera_registers
@channels_loop_end:    
	inx
	cpx #sound::vera::NUM_CHANNELS
	bne @vera_channels_loop

	ldx #$00
@fm_channels_loop:
	stz UPDATE_FM_NOTE
	jsr fm::update_pitch_sweep
	jsr fm::update_glide
	jsr fm::update_note_delay
	lda UPDATE_FM_NOTE
	beq @fm_channels_loop_end
	jsr sound::fm::update_note
@fm_channels_loop_end:
	inx
	cpx #sound::fm::NUM_CHANNELS
	bne @fm_channels_loop

  ; PCM
	; Moved directly into the ISRs
	; This was causing issues with MIDI sync being here
	;jsr sound::pcm::update_pcm

@end:
	pop_rambank
	jsr sound::zsm::update_delay
	; Reset ZSM active flag
	; Turning this off for now which means every delay is writen which is a bit less
	; efficient but helps avoid wonky issues with FM
  ; stz zp_ZSM_TICK_ACTIVE
	rts
.endproc


.endscope


