; Update the PCM offset in the ZSM header with the start address
; of the PCM header.
; zp_ZSM_SIZE
.proc update_zsm_header
  jsr calculate_number_of_samples
  lda num_samples
  bne @open_for_random_io
  ; No PCM used, nothing to write
  rts
@open_for_random_io:
  lda #ZSM_FILE_NUMBER
  ldx #DEVICE
  ldy #SECONDARY_ADDRESS_RANDOM_IO
  jsr SETLFS
  lda #ZSM_RANDOM_IO_FILE_LENGTH
  ldx #<zsm_filename
  ldy #>zsm_filename
  jsr SETNAM
  jsr OPEN
  
@seek:
  ; Filename is P<sec addr><4-byte-offset>
  ; <sec addr> is pre-filled
  lda #KEY_P
  sta seek_command
  lda #SECONDARY_ADDRESS_RANDOM_IO
  sta seek_command + 1
  lda #ZSM_PCM_OFFSET_HEADER_LOCATION
  sta seek_command + 2
  stz seek_command + 3
  stz seek_command + 4
  stz seek_command + 5
  lda #ZSM_SEEK_COMMAND_LENGTH
  ldx #<seek_command
  ldy #>seek_command
  jsr SETNAM

  ; 15,8,15 ($F,$8,$F)
  ; First 15 by convention, 8 is device #, and
  ; second 15 indicates DOS command
  lda #COMMAND_FILE_NUMBER
  ldx #DEVICE
  ldy #SECONDARY_ADDRESS_DOS_COMMAND
  jsr SETLFS
  jsr OPEN
  lda #COMMAND_FILE_NUMBER
  jsr CLOSE

@write_pcm_start_header:
  ldx #ZSM_FILE_NUMBER
  jsr CHKOUT
  lda zp_ZSM_SIZE
  jsr CHROUT
  lda zp_ZSM_SIZE + 1
  jsr CHROUT
  lda zp_ZSM_SIZE + 2
  jsr CHROUT
  
  lda #ZSM_FILE_NUMBER
  jsr CLOSE
  jsr CLRCHN

  rts
.endproc
