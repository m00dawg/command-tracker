; If ZSM export is enabled, open the file and write header
.proc init
  bbs 0, zp_ZSM_ENABLE,@enabled
  rts
@enabled:
  jsr open_zsm_file
  stz zp_ZSM_SIZE
  stz zp_ZSM_SIZE + 1
  stz zp_ZSM_SIZE + 2
  jsr write_header
@clear_vera_registers:
  ldx #$00
  lda #$00
@clear_vera_registers_loop:
  sta vera_shadow_registers,x
  inx
  cpx #NUMBER_OF_VERA_REGISTERS
  bne @clear_vera_registers_loop
@end:
  stz zp_ZSM_TICK_ACTIVE
  stz sample_offset + 1
  stz sample_offset + 2
  rts
.endproc