; Helper routine to write out the delay value
; (called when we're about to do something)
.proc write_delay
  lda zp_ZSM_DELAY_COUNTER
  jsr write_byte
  stz zp_ZSM_DELAY_COUNTER
  rts
.endproc