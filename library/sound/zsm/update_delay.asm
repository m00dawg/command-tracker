; Increment delay if tick wasn't active (nothing was written to 
; any of the sound sources)
; If something WAS written, send $81 to denote the end of the tick.
; Otherise increment, unless we reach $FF
; in which case we need to write the delay then start over

.proc update_delay
  bbs 0, zp_ZSM_ENABLE,@enabled
  rts
@enabled:
  lda zp_ZSM_TICK_ACTIVE
  beq @tick_inactive
@tick_active:
  lda #DELAY_START
  sta zp_ZSM_DELAY_COUNTER
  jsr write_byte
  rts
@tick_inactive:
  lda zp_ZSM_DELAY_COUNTER
  inc
  beq @rollover
  sta zp_ZSM_DELAY_COUNTER
  rts
@rollover:
  lda #DELAY_MAX
  jsr write_byte
  lda #DELAY_START
  sta zp_ZSM_DELAY_COUNTER
  rts
.endproc