; Open ZSM file for export
.proc open_zsm_file
@start:
; Get non-random filename (without the extra flags)
@set_filename:
  lda #ZSM_FILE_LENGTH
  ldx #<zsm_filename
  ldy #>zsm_filename
  jsr SETNAM
; Open the file for write
@set_parameters:
  lda #ZSM_FILE_NUMBER
  ldx #DEVICE
  ldy #SECONDARY_ADDRESS_OPEN_FOR_WRITE
  jsr SETLFS

@open:
  jsr OPEN
@checkout:
  ldx #ZSM_FILE_NUMBER
  jsr CHKOUT

@end:
  rts
.endproc