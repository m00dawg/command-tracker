; Write byte to file in A and increment size counter
; mostly this is for PCM so we know what the
; start byte of the PCM data is to place back into the header
;
; Uses:
; zp_ZSM_SIZE
.proc write_byte
@start:
  phx
  jsr CHROUT
  ldx #$00
@increment_size:
  clc
  lda zp_ZSM_SIZE,x
  adc #$01
  sta zp_ZSM_SIZE,x
  bcs @overflow
@end:
  plx
  rts
@overflow:
  inx
  bra @increment_size
.endproc