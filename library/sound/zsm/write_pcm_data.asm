; Write the PCM data, if needed
; Uses
; num_samples
; sample_offset
.proc write_pcm_data
@start:
  jsr calculate_number_of_samples
  lda num_samples
  bne @samples
  ; No PCM used, nothing to write
  rts
@samples:
  lda #PCM_HEADER1  ; 'P'
  jsr CHROUT
  lda #PCM_HEADER2  ; 'C'
  jsr CHROUT
  lda #PCM_HEADER3  ; 'M'
  jsr CHROUT
  ; Number of samples
  lda num_samples
  jsr CHROUT
  jsr write_instrument_definitions
  jsr write_sample_blob
  rts
.endproc

.proc write_instrument_definitions
@start:
  ldx #$00
@loop:
  ; instrument number
  txa
  jsr CHROUT
  ; Bitrate and stereo/mono
  lda #PCM_AUDIO_CTRL_BITMASK
  jsr CHROUT
  ; Offset of PCM data
  lda sample_offset
  jsr CHROUT
  lda sample_offset + 1
  jsr CHROUT
  lda sample_offset + 2
  jsr CHROUT
  ; PCM length (8k)
  lda #$00
  jsr CHROUT
  lda #$20
  jsr CHROUT
  lda #$00
  jsr CHROUT
  ; PCM feature bitmask
  lda #PCM_FEATURE_BITMASK
  jsr CHROUT
  ; Loop position (unused for now)
  lda #$00
  jsr CHROUT
  jsr CHROUT
  jsr CHROUT
  ; unsed
  jsr CHROUT
  jsr CHROUT
  jsr CHROUT
  jsr CHROUT
  ; Next sample offset
  ; Calculate the next sample address offset to start at for the next sample
  ; Since we're adding 8192 each time, that's $2000. So the low byte will 
  ; always be $00 and we can just worry about 16-bit math.
  add16to8 sample_offset + 1, #$20, sample_offset + 1
  inx
  cpx num_samples
  bne @loop
@end:
  rts
.endproc

.proc write_sample_blob
@start:
  ldx #$00
@sample_loop:
  ldy #BANK_ADDRESS_TOP
  clc
  txa
  adc sound::pcm::bank_start
  sta RAM_BANK
  phx
  lda #<PAGE_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PAGE_ADDRESS
  sta zp_PAGE_POINTER + 1
  jsr files::write_page
  plx
@sample_loop_bottom:
  inx
  cpx num_samples
  bne @sample_loop
  rts
.endproc
