; Stop ZSM export and cleanup
.proc stop
  bbs 0, zp_ZSM_ENABLE,@enabled
  rts
@enabled:
  lda #ZSM_NON_PCM_EOF
  jsr write_byte
@close_file:
  stz zp_ZSM_TICK_ACTIVE
  jsr write_pcm_data
  lda #ZSM_FILE_NUMBER
  jsr CLOSE
  jsr CLRCHN
  ; After the close becuase we re-open the file 
  ; with random I/O so we can seek to the headers
  jsr update_zsm_header
  rts
.endproc