; Safely send a message (not using the buffer)
; (when Transmitter Holding Register is empty)
; Or when bit 5, the THRE, is set

.proc send_sync
	pha
@check:
	ldy #LINE_STATUS_OFFSET
	lda (zp_MIDI_BASE),y
	and #%00100000
	beq @check
@go:
	pla
	ldy #TX_HOLDING_OFFSET
	sta (zp_MIDI_BASE),y
	rts
.endproc