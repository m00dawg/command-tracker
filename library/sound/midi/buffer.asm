; a = byte to write (send to buffer)
; Reads zp_MIDI_DEVICE
; Uses x,y
.proc write_buffer
	ldx zp_MIDI_DEVICE
	cpx #$00
	bne @wt
@midi:
	ldy zp_MIDI_BUFFER_WRITE_POINTER
	sta midi_tx_buffer,y
	inc zp_MIDI_BUFFER_WRITE_POINTER
	bra @end 

@wt:
	ldy zp_WT_BUFFER_WRITE_POINTER
	sta wt_tx_buffer,y
	inc zp_WT_BUFFER_WRITE_POINTER

@end:
	rts
.endproc

; Run within interrupt to read 
; If buffer is non-empty attempt to send
.proc read_midi_interrupt
@check_buffer_size:
	lda zp_MIDI_BUFFER_WRITE_POINTER
	sec
	sbc zp_MIDI_BUFFER_READ_POINTER
	bne @check_ready
	rts
@check_ready:
	ldy #LINE_STATUS_OFFSET
	lda (zp_MIDI_BASE),y
	and #%00100000
	bne @go
	rts
@go:
	ldy zp_MIDI_BUFFER_READ_POINTER
	lda midi_tx_buffer,y
	inc zp_MIDI_BUFFER_READ_POINTER
	ldy #TX_HOLDING_OFFSET
	sta (zp_MIDI_BASE),y
	rts
.endproc

; Run within interrupt to read 
; If buffer is non-empty attempt to send
.proc read_wt_interrupt
@check_buffer:
	lda zp_WT_BUFFER_WRITE_POINTER
	sec
	sbc zp_WT_BUFFER_READ_POINTER
	bne @check_ready
	rts
@check_ready:
	ldy #LINE_STATUS_OFFSET
	lda (zp_WAVETABLE_BASE),y
	and #%00100000
	bne @go
	rts
@go:
	ldy zp_WT_BUFFER_READ_POINTER
	lda wt_tx_buffer,y
	inc zp_WT_BUFFER_READ_POINTER
	ldy #TX_HOLDING_OFFSET
	sta (zp_WAVETABLE_BASE),y
	sta USER_DEBUG_1
	rts
.endproc