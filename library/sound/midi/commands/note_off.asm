; Send Note OFF for given MIDI channel with velociy

; x = velocity
; zp_ROW_NOTE = Row note from pattern

; Uses:
; zp_ARG0 = MIDI channel (not DT channel!)
; zp_ARG1 = MIDI note

; 0x80 to 0x8F where the low nibble is the MIDI channel. 
	; Then note
	; then velocity 

.proc note_off
	ldy #TX_HOLDING_OFFSET

	lda #message::NOTE_OFF
	ora zp_ARG0
	jsr send
	;sta (zp_MIDI_BASE),y
	lda zp_ARG1
	jsr send
	;sta (zp_MIDI_BASE),y
	txa	; Velocity
	jsr send
	;sta (zp_MIDI_BASE),y

	rts
.endproc