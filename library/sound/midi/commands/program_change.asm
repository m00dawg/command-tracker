; zp_ROW_INSTRUMENT = Row instrument from pattern

; Uses:
; zp_ARG0 = MIDI channel (not DT channel!)


.proc program_change
	ldy #TX_HOLDING_OFFSET

	lda #message::PROGRAM_CHANGE
	ora zp_ARG0
	sta (zp_MIDI_BASE),y
	lda zp_ROW_INSTRUMENT
	sta (zp_MIDI_BASE),y
	rts
.endproc