; Send MIDI reset

.proc reset
	ldy #TX_HOLDING_OFFSET
	lda #message::RESET
	jsr send
	rts
.endproc