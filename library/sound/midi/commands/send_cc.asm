; channel = zp_MIDI_CHANNEL
; a = value
; x = cc number
.proc send_cc
	pha
	phx

	ldy #TX_HOLDING_OFFSET

	lda #midi::message::CC_CHANGE
	ora zp_MIDI_CHANNEL
	jsr send

	; CC number
	pla
	jsr send

	; CC value
	pla 
	jsr send

	rts
.endproc

