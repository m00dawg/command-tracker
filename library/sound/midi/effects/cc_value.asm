; Send CC value
; Send out given CC value for the currently set
; MIDI channel and CC parameter.

; a = channel
; y = value

.proc cc_value
return = zp_ADDR_RETURN

@start:
    tax ; Channel
    lda sound::midi::channel_midi_channel,x
    sta zp_MIDI_CHANNEL

    ; a = CC value
    ; x = CC parameter
    lda sound::midi::channel_cc_parameter,x
    tax
    tya
    jsr sound::midi::commands::send_cc
    jmp (return)
.endproc