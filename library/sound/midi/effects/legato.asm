; Set Legato Mode

; bits 1-0 arelegato, see sound::midi

; a = channel
; y = legato value
.proc legato
  return = zp_ADDR_RETURN

	tax

  lda sound::midi::channel_midi_channel,x
  sta zp_MIDI_CHANNEL
	lda sound::midi::channel_flags,x
  
  ; Check y value for legato
  cpy #$00
  beq @disable_legato
	cpy #$02
	beq @midi_legato
@poly:
	and #%11111100
  ora #%00000001
  sta sound::midi::channel_flags,x
  jmp (return)

@midi_legato:
  ora #%00000010
  sta sound::midi::channel_flags,x

  ; Send MIDI CC to enable Legato
  lda #LEGATO_ON
  ldx #message::cc::LEGATO
  jsr commands::send_cc
  jmp (return)

@disable_legato:
  and #%11111100
  sta sound::midi::channel_flags,x

; Send MIDI CC to enable Legato
  lda #LEGATO_OFF
  ldx #message::cc::LEGATO
  jsr commands::send_cc
  jmp (return)
.endproc
