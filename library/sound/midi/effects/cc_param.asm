; Set CC Param
; This sets up the CC param for the channel

; a = channel
; y = param

.proc cc_param
return = zp_ADDR_RETURN

@start:
    tax ; Channel
    tya ; Speed
    sta sound::midi::channel_cc_parameter,x
    jmp (return)
.endproc