; Set Cutoff for the SAM chip
; Inverted compared to the SAM
; Where low values = lower frequency, higher values = higher frequency

; a = channel
; y = cutoff value

.proc sam_cutoff
return = zp_ADDR_RETURN

@start:
	tax
  lda sound::midi::channel_midi_channel,x
  sta zp_MIDI_CHANNEL

	; a = CC value
	; x = CC parameter	
	tya	; value
	; Invert
	eor #%01111111
	ldx #midi::message::cc::SAM_CUTOFF
	jsr sound::midi::commands::send_cc

	jmp (return)
.endproc