
.proc init
	; MIDI
	lda #$00
	jsr set_address
	jsr setup_uart
	; Send all notes off
	jsr commands::reset

	; Wavetable
	lda #$01
	jsr set_address
	jsr setup_uart
	; Send all notes off
	jsr commands::reset
	jsr setup_filter

	stz zp_MIDI_STATE
	stz zp_MIDI_CLOCK_COUNTER
	

	; Reset Stuff
	ldx #$00
@reset_loop:
	lda #$00
	sta channel_midi_channel,x
	sta channel_instrument,x
	sta channel_cc_parameter,x
	sta channel_note,x
	sta channel_flags,x
	sta channel_attenuation,x
	lda #$7F
	sta channel_vol,x
	inx
	cpx #TRACKER_CHANNELS
	bne @reset_loop

	; Reset MIDI channe specific registers
	ldx #$00
	lda #$FF
@reset_midi_chans_loop:
	sta midi_channel_nrpn_coarse,x
	sta midi_channel_nrpn_fine,x
	inx
	cpx #MIDI_CHANNELS
	bne @reset_midi_chans_loop

	rts
.endproc

.proc setup_uart
	; Enable Divisor Latch
	lda #%10000000
	ldy #LINE_CONTROL_OFFSET
	sta (zp_MIDI_BASE),y

	lda #<MIDI_BAUD_RATE_DIVISOR
	ldy #DIVISOR_LATCH_LOW
	sta (zp_MIDI_BASE),y

	lda #>MIDI_BAUD_RATE_DIVISOR
	ldy #DIVISOR_LATCH_HI
	sta (zp_MIDI_BASE),y

	; Disable Divisor Latch & Set word length
	lda #LCR_SETUP
	ldy #LINE_CONTROL_OFFSET
	sta (zp_MIDI_BASE),y

	; Setup FIFO
	lda #FIFO_CLEAR
	ldy #FIFO_CONTROL_OFFSET
	sta (zp_MIDI_BASE),y

	lda #FIFO_SETUP
	ldy #FIFO_CONTROL_OFFSET
	sta (zp_MIDI_BASE),y
	
	; Setup Modem (Disable)
	lda #MODEM_SETUP
	ldy #MODEM_CONTROL_OFFSET
	sta (zp_MIDI_BASE),y
	
	; Setup Interrupts (Disable)
	lda #INTR_SETUP
	ldy #INTERRUPT_ENABLE_OFFSET
	sta (zp_MIDI_BASE),y

	rts
.endproc

; Setup filter for wavetable
; Full Sysex strings:
; Assign SAM CC1 to CC #74
; F0h 41h 00h 42h 12h 40h 1ph 1Fh 4Ah xxh F7h
; Attach cutoff to SAM CC1
; F0h 41h 00h 42h 12h 40h 2ph 41h vvh xxh F7h		
; Where p = channel # (0-15), vv = cutoff weight, xx = checksum
.proc setup_filter
	CHECKSUM = zp_TMP0
	NUM_CHANNELS = $10
	CUTOFF_WEIGHT = $00

	STRING = zp_ADDR0
	LENGTH = zp_ARG0

; SAM CC1 to CC #74 assgin SysEx
@cc1:
	lda #$1F
	sta param2
	lda #$4A
	sta param3

	lda #$47
	sta CHECKSUM

	ldx #$00
@cc1_loop:
	txa
	adda #$10
	sta param1

	lda CHECKSUM
	sta checksum
	dec CHECKSUM

	lda #<sysex
	sta STRING
	lda #>sysex
	sta STRING + 1
	lda #SYSEX_LENGTH
	sta LENGTH
	jsr commands::send_multibyte
@cc1_loop_bottom:
	inx
	cpx #NUM_CHANNELS
	bne @cc1_loop

; Cutoff Assign SysEx
@cutoff_assign:
	lda #$41
	sta param2
	lda #$00
	sta param3

	lda #$5F
	sta CHECKSUM

	ldx #$00
@cutoff_assign_loop:
	txa
	adda #$20
	sta param1

	lda CHECKSUM
	sta checksum
	dec CHECKSUM

	lda #<sysex
	sta STRING
	lda #>sysex
	sta STRING + 1
	lda #SYSEX_LENGTH
	sta LENGTH
	jsr commands::send_multibyte

@cutoff_assign_loop_bottom:
	inx
	cpx #NUM_CHANNELS
	bne @cutoff_assign_loop

@end:
	rts

	; Message Constants
; Assign SAM CC1 to CC #74
; F0h 41h 00h 42h 12h 40h 1ph 1Fh 4Ah xxh F7h
; Attach cutoff to SAM CC1
; F0h 41h 00h 42h 12h 40h 2ph 41h vvh xxh F7h		
; Where p = channel # (0-15), vv = cutoff weight, xx = checksum


	SYSEX_LENGTH = $0B
	sysex:
		.byte message::SYSEX_START,$41,$00,$42,$12,$40 
	param1:
		.byte $10
	param2:
		.byte $1F
	param3:
		.byte $4A
	checksum:
		.byte $FF
	end:
		.byte message::SYSEX_END
.endproc