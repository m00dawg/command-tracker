; Compare the provided 
; A = sample rate

; Based on https://docs.google.com/spreadsheets/d/1GdV2P1DjWWLfRROK9WA_AK4HYTbeWE_9FTiT7bq8dxo/edit#gid=0
; Calculated as (Samples Per Int Rate) * (Int Rate) * Tick Rate
; Or (48.8Khz / 128) * (VERA Int Rate) * (1/60 for Vsync)

.proc calc_num_chonks
  cmp #SAMPLE_RATE_FOR_4_CHONKS
  bge @four_chonks
@test_three_chonks:
  cmp #SAMPLE_RATE_FOR_3_CHONKS
  bge @three_chonks
@test_two_chonks:
  cmp #SAMPLE_RATE_FOR_2_CHONKS
  bge @two_chonks
@one_chonk:
  lda #$01
  sta zp_PCM_NUM_CHONKS
  rts

@four_chonks:
  lda #$04
  sta zp_PCM_NUM_CHONKS
  rts
@three_chonks:
  lda #$03
  sta zp_PCM_NUM_CHONKS
  rts
@two_chonks:
  lda #$02
  sta zp_PCM_NUM_CHONKS
  rts
.endproc