.proc update_pcm
    FLAGS = zp_TMP0
@start:
  ; Check if the channel is active (bit 7)
  bbs 7, zp_PCM_FLAGS, @active
  rts

  ; Only the high byte since the chonks (low-byte) are 256
  lda #>sound::pcm::SAMPLE_ADDRESS
  adda zp_PCM_CHONK_COUNT
  sta zp_PCM_SAMPLE_ADDRESS + 1

@active:
  ; We're active, so check if we have more bytes to send
  lda zp_PCM_CHONK_COUNT
  cmp #sound::pcm::SAMPLE_CHONK_SIZE
  bne @play
@cleanup:
  ; If channel is active but the position and size
  ; equal, we're done playing things, so clean up
  ;Check if the FIFO is empty
  lda VERA_audio_ctrl
  sta FLAGS
  bbs 6, FLAGS, @disable_audio
  rts
@disable_audio:
  ; The FIFO is empty so let's turn off the PCM 
  ; and set the channel as inactive until it's needed
  stz VERA_audio_ctrl
  stz VERA_audio_rate
  rmb 7,zp_PCM_FLAGS
  rts

@play:
  ; Check to see if the buffer is nearly full
  ; if not, don't do anything to avoid overfilling the buffer
  lda VERA_isr
  and #VERA_AFLOW_MASK
  bne @continue
  rts
@continue:
  push_rambank
  rambank zp_PCM_SAMPLE_PAGE
  ; If it's active, we load some bytes. 
  ; The inner loop reads 256 bytes at a time.
  ; We loop through these bytes up to the 
  ; chonk size
  ldx #$00
@outer_loop:
  ldy #$00
@inner_loop:
  lda (zp_PCM_SAMPLE_ADDRESS),y
  sta VERA_audio_data
  iny
  lda (zp_PCM_SAMPLE_ADDRESS),y
  sta VERA_audio_data
  iny
  lda (zp_PCM_SAMPLE_ADDRESS),y
  sta VERA_audio_data
  iny
  lda (zp_PCM_SAMPLE_ADDRESS),y
  sta VERA_audio_data
  iny
  bne @inner_loop
@outer_loop_bottom:
  inc zp_PCM_CHONK_COUNT
  inc zp_PCM_SAMPLE_ADDRESS + 1
  inx
  ; Calculated based on sample rate
  ;cpx #sound::pcm::NUM_CHONKS_PER_TICK
  cpx zp_PCM_NUM_CHONKS
  bne @outer_loop
@play_end:
  ; Take the raw channel value and use it as the 
  ; sample rate, noting it doesn't correspond to 
  ; the actual note pitch.
  lda zp_PCM_SAMPLE_RATE
  sta VERA_audio_rate
  lda zp_PCM_VOLUME
  sta VERA_audio_ctrl
  pop_rambank
  rts
.endproc