; Setup VERA PCM for playback

.proc init
  stz zp_PCM_SAMPLE_NUMBER
  stz zp_PCM_SAMPLE_PAGE
  stz zp_PCM_SAMPLE_ADDRESS
  stz zp_PCM_FLAGS
  stz zp_PCM_SAMPLE_RATE
  stz zp_PCM_VOLUME
  stz zp_PCM_CHONK_COUNT
  stz zp_PCM_NUM_CHONKS
  rts
.endproc