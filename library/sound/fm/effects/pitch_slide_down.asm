; Pitch Slide Down
;
; This is intended to be called from 
; the pattern/row playback

; Trying something different here. To get the final KC and KF,
; we put the MIDI note and current fractional value together
; into a 16-bit value.
;
; Then we subtract the slide value from this, update the note
; and fraction then send that to the KC/KF values.

; a = channel
; y = pitch slide value

.proc pitch_slide_down
channel_pitch = zp_MATH0
pitch_slide_value = zp_MATH1
result = zp_MATH2
channel = zp_MATH3

return = zp_ADDR_RETURN
@start:
    tax ; Channel #
    sta channel

    lda sound::fm::channel_semitone,x
    sta channel_pitch
    lda sound::fm::channel_note,x
    sta channel_pitch + 1
    
    tya ; value
    sta pitch_slide_value
    sub16from8 channel_pitch, pitch_slide_value, result

    lda result
    sta sound::fm::channel_semitone,x
    lda result + 1
    sta sound::fm::channel_note,x

    rombank #AUDIO_ROM_BANK
    lda sound::fm::channel_note,x
    tax
    jsr notecon_midi2fm
    stz ROM_BANK
    txa
    ldx channel
    ;sta sound::fm::channel_key_code,x

    ; Send note to FM
    tay
    add #YM_KEY_CODE_BASE,channel
    jsr sound::fm::write_register

    ; Send fraction to FM
    ldy result
    add #YM_KEY_FRACTION_BASE,channel
    jsr sound::fm::write_register

    jmp (return)
.endproc
