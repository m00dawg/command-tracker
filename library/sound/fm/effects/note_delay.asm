; Delay Note by n ticks
; Evaluated in synth engine

; a = channel
; y = delay
; zp_ROW_NOTE = Row note from pattern
.proc note_delay
	tax ; Channel
	tya ; Delay
	sta sound::fm::channel_note_delay,x
	lda zp_ROW_NOTE
	sta sound::fm::channel_note_delay_note,x
	rts
.endproc