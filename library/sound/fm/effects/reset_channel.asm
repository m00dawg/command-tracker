; Channel Reset
;
; Clear all the persistent registers
; (such as vibrato)
; a = channel

.proc reset_channel
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    stz sound::fm::channel_attenuation,x
    stz sound::fm::channel_volume_sweep_value,x
    stz sound::fm::channel_pitch_sweep_value,x
    stz sound::fm::channel_glide_speed,x
    stz sound::fm::channel_note_delay,x
    lda #NOTENULL
    sta sound::fm::channel_note_delay_note,x
    jmp (return)
.endproc
