; Glide to note (Glissando)

; a = channel
; y = speed
; zp_ROW_NOTE = Row note from pattern

; This sets up the glide speed, but the actual glide
; pitch is set by the read_note routine as part of the
; tracker::play_row

.proc glide
return = zp_ADDR_RETURN

@start:
    tax ; Channel
    tya ; Speed
    sta sound::fm::channel_glide_speed,x
    rts
.endproc