; Enable/disable Legato

; bit 3 is legato, see sound::fm

; a = channel
; y = legato enable/disable
.proc legato
	tax
	lda sound::fm::channel_flags,x
	cpy #$00
  beq @disable_legato
@enable_legato:
  ora #%00001000
  sta sound::fm::channel_flags,x
  rts
@disable_legato:
  and #%11110111
  sta sound::fm::channel_flags,x
  rts
.endproc
