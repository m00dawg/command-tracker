; Pitch Sweep Up
;
; This is intended to be called from 
; the pattern/row playback

; a = channel
; y = pitch sweep value

; Unlike pitch slide, sweep runs at synth speed
; and is controlled by the tick ISR. 
; More precise and useful for kicks and things

.proc pitch_sweep_up
return = zp_ADDR_RETURN

@start:
    tax ; Channel #
    tya ; value    
    sta sound::fm::channel_pitch_sweep_value,x
    lda sound::fm::channel_flags,x
    ; turn on bit 5 = sweep direction
    ora #%00100000
    sta sound::fm::channel_flags,x
    jmp (return)
.endproc