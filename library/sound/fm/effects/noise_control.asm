; Noise Control
;
; Enables/Disables noise on channel 7 and 
; sets the noise frequency
; y = noise control
;     Bit 7: Enable/disable
;     Bit 4-0: Noise Frequency
;     Range: $80 - $9F

.proc noise_control
return = zp_ADDR_RETURN

@start:
  ; a = address
  ; y = data  
  lda #NOISE_CONTROL
  jsr sound::fm::write_register
  jmp (return)
.endproc