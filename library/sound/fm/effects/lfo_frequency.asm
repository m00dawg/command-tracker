; Set LFO Frequency
; $00 = ~0.008Hz
; $FF = ~32.6Hz
;
; y = depth

.proc lfo_frequency
return = zp_ADDR_RETURN

@start:
  ; a = address
  ; y = data  
  lda #YM_LFO_FREQ
  jsr sound::fm::write_register
  jmp (return)
.endproc