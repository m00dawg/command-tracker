; Meta effect that updates the FM registers for a channel directly
;
; Uses:
; a = channel 
; x = param
; y = value

; zp_ADDR0 
; zp_MATH0
; zp_MATH1
; zp_MATH2
; zp_MATH3



.proc update_channel_register
  channel = zp_MATH0
  param = zp_MATH1
  value = zp_MATH2
  temp = zp_MATH3
  channel_register_address = zp_ADDR0

  sta channel
  stx param
  sty value

; Check to make sure the parameter is within the range
  txa ; param
@check_for_valid_effect:
  cmp #sound::fm::REGISTER_EFFECTS_START
  bge @min_value
  rts
@min_value:
  cmp #sound::fm::REGISTER_EFFECTS_STOP + 1
  blt @less_than_max_value
  rts
@less_than_max_value:
  ; Subtract the effect start to get a base value for the LUTs
  suba #sound::fm::REGISTER_EFFECTS_START
  tax
@update_channel:
  ; Find the start of the data
  lda effect_to_channel_register_lut_lo,x
  sta channel_register_address
  lda effect_to_channel_register_lut_high,x
  sta channel_register_address + 1
  lda param
  ldy channel
  sta (channel_register_address),y
@update_fm_registers:
  ; Convert row value to attenuated equaivlent if required
  lda attenuation_mask_lut,x
  sta temp
  lda value
  eor temp
  tay

  ; Get the register offset by the channel
  lda effect_to_fm_register_lut,x
  adda channel
  ; a = address, y = data
  jsr write_register
@end:
  rts
.endproc