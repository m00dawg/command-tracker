; Load and play insturment on channel 0
; At middle-C

; Provided args:
; zp_ARG1 = instrument

; Used args:
; zp_ARG0 = channel (needed for other routines)

.proc preview_instrument
  channel = zp_ARG0
  instrument = zp_ARG1
  MIDDLE_C = %01001110

  stz channel
  lda zp_INSTRUMENT
  sta instrument
  jsr update_channel_instrument
  
  ; Middle C
  lda #MIDDLE_C
  tay
  lda #YM_KEY_CODE_BASE
  jsr write_register

  ; Release
  lda #%00000000
  tay
  lda #YM_KEY_CONTROL
  jsr write_register

  ; All operators, channel 0
  lda #%01111000
  tay
  lda #YM_KEY_CONTROL
  jsr sound::fm::write_register

  rts

.endproc