.proc init
  rombank #AUDIO_ROM_BANK
  jsr ym_init
  stz ROM_BANK
  ldx #$00
@channel_instrument_loop:
  stz sound::fm::channel_volume,x
  stz sound::fm::channel_attenuation,x
  stz sound::fm::channel_note_delay,x

  lda #INSTNULL
  sta sound::fm::channel_instrument,x
  ;stx zp_ARG0
  ;sta zp_ARG1
  ;phx
  ;jsr update_channel_instrument
  ;plx
  inx
  cpx #sound::fm::NUM_CHANNELS
  bne @channel_instrument_loop
@load_globals:
  push_rambank
  rambank zp_BANK_MISC
  lda fm_global_ch7_noise_control
  sta fm::global_ch7_noise_control
  lda fm_global_lfo_frequency
  sta fm::global_lfo_frequency
  lda fm_global_lfo_am_depth
  sta fm::global_lfo_am_depth
  lda fm_global_lfo_pm_depth
  sta fm::global_lfo_pm_depth
  lda fm_global_lfo_waveform
  sta fm::global_lfo_waveform
  pop_rambank

  lda #LFO_RESET
  ldy #$00
  jsr write_register

  lda #NOISE_CONTROL
  ldy fm::global_ch7_noise_control
  jsr write_register

  lda #LFO_FREQUENCY
  ldy fm::global_lfo_frequency
  jsr write_register

  lda fm::global_lfo_am_depth
  and #%01111111
  tay
  lda #LFO_AMPLITUDE
  jsr write_register

  lda fm::global_lfo_pm_depth
  ora #%10000000
  tay
  lda #LFO_AMPLITUDE
  jsr write_register

  lda #LFO_WAVEFORM
  ldy fm::global_lfo_waveform
  jsr write_register

@end:
  rts

.endproc