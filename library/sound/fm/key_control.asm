; Start or release operators for channel

; zp_CHANNEL_COUNT = channel offset
; zp_TMP0 = channel flags

.proc key_control
    bbs 7, zp_NOTE_FLAGS,@check_legato

@release:
    ;lda #%00000000
    ;ora zp_CHANNEL_COUNT
    ;tay
    ldy zp_CHANNEL_COUNT
    lda #YM_KEY_CONTROL
    jsr write_register
    rts
    
@check_legato:
    ldx zp_CHANNEL_COUNT
    lda sound::fm::channel_flags,x
    ;sta zp_TMP0
    ; If the legato bit flag is set, skip the off section
    ;bbs 3, zp_TMP0,@play
@off:    
    ;lda #%00000000
    ;ora zp_CHANNEL_COUNT
    ;tay
    ldy zp_CHANNEL_COUNT
    lda #YM_KEY_CONTROL
    jsr write_register
@play:
    lda #%01111000
    ora zp_CHANNEL_COUNT
    tay
    lda #YM_KEY_CONTROL
    jsr write_register

    rts
.endproc