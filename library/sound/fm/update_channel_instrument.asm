; Update FM channel's instrument
; Basically apply the defaults from the 
; instrument to the channel

; EOR is used to flip some of the registers 
; which are "attenuators" on the FM chip.
; In the tracker, we store "volume" (so the reverse)
; and need to flip it before we write.

; zp_ARG0 = channel
; zp_ARG1 = instrument

.proc update_channel_instrument
    CHANNEL = zp_ARG0
    INSTRUMENT = zp_ARG1
@start:
    push_rambank
    rambank zp_BANK_FINS

    ldy CHANNEL
    lda INSTRUMENT
    sta sound::fm::channel_instrument,y
    
    ldx INSTRUMENT
    lda fm_instruments_global_volume,x
    sta sound::fm::channel_volume,y

    lda fm_instruments_global_to_op_volume,x
    sta sound::fm::channel_global_to_op_volume,y

    ; FM Registers
    lda fm_instruments_pan_feedback_algo,x
    sta sound::fm::channel_pan_feedback_algo,y   
    tay
    add #YM_PAN_FEEDBACK_ALGO_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_modulation_sensitivity,x
    sta sound::fm::channel_modulation_sensitivity,y
    ; Have to do this one at a time due to high bit
    ; (see https://github.com/X16Community/x16-docs/blob/master/X16%20Reference%20-%2009%20-%20Sound%20Programming.md)
    tay
    add #YM_MODULATION_SENSITIVITY_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_detune_fine_frequency_mul_m1,x
    
    sta sound::fm::channel_detune_fine_frequency_mul_m1,y
    tay
    add #YM_DETUNE_FINE_FREQUENCY_MUL_M1_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_detune_fine_frequency_mul_m2,x
    sta sound::fm::channel_detune_fine_frequency_mul_m2,y
    tay
    add #YM_DETUNE_FINE_FREQUENCY_MUL_M2_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_detune_fine_frequency_mul_c1,x
    sta sound::fm::channel_detune_fine_frequency_mul_c1,y
    tay
    add #YM_DETUNE_FINE_FREQUENCY_MUL_C1_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_detune_fine_frequency_mul_c2,x
    sta sound::fm::channel_detune_fine_frequency_mul_c2,y
    tay
    add #YM_DETUNE_FINE_FREQUENCY_MUL_C2_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_volume_m1,x
    sta sound::fm::channel_volume_m1,y
    eor #%11111111
    tay
    add #YM_ATTENUATION_M1_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_volume_m2,x
    sta sound::fm::channel_volume_m2,y
    eor #%11111111
    tay
    add #YM_ATTENUATION_M2_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_volume_c1,x
    sta sound::fm::channel_volume_c1,y
    eor #%11111111
    tay
    add #YM_ATTENUATION_C1_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_volume_c2,x
    sta sound::fm::channel_volume_m2,y
    eor #%11111111
    tay
    add #YM_ATTENUATION_C2_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_ks_attack_m1,x
    sta sound::fm::channel_ks_attack_m1,y
    eor #%00111111
    tay
    add #YM_KS_ATTACK_M1_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_ks_attack_m2,x
    sta sound::fm::channel_ks_attack_m2,y
    eor #%00111111
    tay
    add #YM_KS_ATTACK_M2_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_ks_attack_c1,x
    sta sound::fm::channel_ks_attack_c1,y
    eor #%00111111
    tay
    add #YM_KS_ATTACK_C1_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL
    lda fm_instruments_ks_attack_c2,x
    sta sound::fm::channel_ks_attack_c2,y
    eor #%00111111
    tay
    add #YM_KS_ATTACK_C2_BASE, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_am_decay1_m1,x
    sta sound::fm::channel_am_decay1_m1,y
    eor #%00011111
    tay
    add #YM_AM_DECAY1_M1, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_am_decay1_m2,x
    sta sound::fm::channel_am_decay1_m2,y
    eor #%00011111
    tay
    add #YM_AM_DECAY1_M2, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_am_decay1_c1,x
    sta sound::fm::channel_am_decay1_c1,y
    eor #%00011111
    tay
    add #YM_AM_DECAY1_C1, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_am_decay1_c2,x
    sta sound::fm::channel_am_decay1_c2,y
    eor #%00011111
    tay
    add #YM_AM_DECAY1_C2, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_detune_coarse_decay2_m1,x
    sta sound::fm::channel_detune_coarse_decay2_m1,y
    eor #%00011111
    tay
    add #YM_DETUNE_COARSE_DECAY2_M1, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_detune_coarse_decay2_m2,x
    sta sound::fm::channel_detune_coarse_decay2_m2,y
    eor #%00011111
    tay
    add #YM_DETUNE_COARSE_DECAY2_M2, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_detune_coarse_decay2_c1,x
    sta sound::fm::channel_detune_coarse_decay2_c1,y
    eor #%00011111    
    tay
    add #YM_DETUNE_COARSE_DECAY2_C1, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_detune_coarse_decay2_c2,x
    sta sound::fm::channel_detune_coarse_decay2_c2,y
    eor #%00011111
    tay
    add #YM_DETUNE_COARSE_DECAY2_C2, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_decay1_release_m1,x
    sta sound::fm::channel_decay1_release_m1,y
    eor #%11111111
    tay
    add #YM_DECAY1_RELEASE_M1, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_decay1_release_m2,x
    sta sound::fm::channel_decay1_release_m2,y
    eor #%11111111
    tay
    add #YM_DECAY1_RELEASE_M2, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_decay1_release_c1,x
    sta sound::fm::channel_decay1_release_c1,y
    eor #%11111111
    tay
    add #YM_DECAY1_RELEASE_C1, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    lda fm_instruments_decay1_release_c2,x
    sta sound::fm::channel_decay1_release_c2,y
    eor #%11111111
    tay
    add #YM_DECAY1_RELEASE_C2, CHANNEL
    jsr sound::fm::write_register
    ldy CHANNEL

    jsr sound::fm::update_volume
    ; Restore previous bank
    pop_rambank

    rts
.endproc