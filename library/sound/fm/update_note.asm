; Update the FM chip from the channel registers
; Inputs:
; x = channel

; ZPs
; zp_MATH0 = channel

.proc update_note
  channel = zp_MATH0
  stx channel

  rombank #AUDIO_ROM_BANK
  lda sound::fm::channel_note,x
  tax
  jsr notecon_midi2fm
  stz ROM_BANK
  txa
  ldx channel
;  sta sound::fm::channel_key_code,x

  ; Send note to FM
  tay
  add #YM_KEY_CODE_BASE,channel
  jsr sound::fm::write_register

  ; Send fraction to FM
  lda sound::fm::channel_semitone,x
  tay
  add #YM_KEY_FRACTION_BASE,channel
  jsr sound::fm::write_register

  rts
.endproc