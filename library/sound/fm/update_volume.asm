; Apply the channel volume
; to the operators that have global volume changes enabled
; zp_ARG0 = channel #
; zp_MATH0 = volume
; zp_MATH1 = global_to_op_vlume

.proc update_volume
  channel = zp_ARG0
  volume = zp_MATH0
  global_to_op_volume = zp_MATH1

  ; Subtract attenuation from volume
  ldx channel
  lda channel_attenuation,x
  sta volume
  lda channel_volume,x
  suba volume
  sta volume

  lda channel_global_to_op_volume,x
  sta global_to_op_volume
@m1_check:  
  bbs 7, global_to_op_volume,@m1_enable
  bra @m2_check
@m1_enable:
  lda volume
  sta channel_volume_m1,x
  ; Convert to attenuation
  eor #%11111111
  tay
  add #YM_ATTENUATION_M1_BASE, channel
  jsr sound::fm::write_register
@m2_check:
  bbs 6, global_to_op_volume,@m2_enable
  bra @c1_check
@m2_enable:
  lda volume
  sta channel_volume_m2,x
  ; Convert to attenuation
  eor #%11111111
  tay
  add #YM_ATTENUATION_M2_BASE, channel
  jsr sound::fm::write_register
@c1_check:
  bbs 5, global_to_op_volume,@c1_enable
  bra @c2_check
@c1_enable:
  lda volume
  sta channel_volume_c1,x
  ; Convert to attenuation
  eor #%11111111
  tay
  add #YM_ATTENUATION_C1_BASE, channel
  jsr sound::fm::write_register
@c2_check:
  bbs 4, global_to_op_volume,@c2_enable
  bra @end
@c2_enable:
  lda volume
  sta channel_volume_c2,x
  ; Convert to attenuation
  eor #%11111111
  tay
  add #YM_ATTENUATION_C2_BASE, channel
  jsr sound::fm::write_register
@end:
  rts
.endproc
