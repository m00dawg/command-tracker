.segment "CODE"
; Memory management routines
.scope memory

; Clear contents of 8k bank
.proc clear_ram_bank
  lda #<BANK_ADDRESS
  sta r0
  lda #>BANK_ADDRESS
  sta r0 + 1
  ; $2000 (size of bank)
  stz r1
  lda #$20
  sta r1 + 1
  lda #$00
  jsr memory_fill
  rts
.endproc

.endscope