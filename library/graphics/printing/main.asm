; Printing rountines
.scope printing
    .include "library/graphics/printing/print_character.asm"
    .include "library/graphics/printing/print_hex.asm"
    .include "library/graphics/printing/print_alpha_char.asm"
    .include "library/graphics/printing/print_string.asm"
    .include "library/graphics/printing/print_string_with_length.asm"
    .include "library/graphics/printing/print_string_with_colors.asm"
    .include "library/graphics/printing/print_screencodes_with_colors.asm"
    .include "library/graphics/printing/chars_to_number.asm"
.endscope
