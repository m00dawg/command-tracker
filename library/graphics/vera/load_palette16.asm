; Load 16 color palette
.proc load_palette_16

.IF CPU_65816
load_palette_16:
  acc16b

; This took a bit to figure out.
; Words still read normally but are still loaded
; as little endian. So when we sta to VERA_addr_med,
; $FA is first but then the $21 gets stored into VERA_addr_high
;
; To setup 16-bit writes, we have to stagger data0 and data1
; which is the ldx/dex into both ctrl and low. That's just convenient
; coincidence which we use to make the loop shorter.
; Basically data1 needs to be offset by one from data0 for 16-bit
; writes to work.
  lda #$21FA
  ldx #$01
@vera_loop:
  stx VERA_ctrl
  stx VERA_addr_low
  sta VERA_addr_med
  dex
  bpl @vera_loop 

  ldx #$00
@loop:
  lda palette,x
  sta VERA_data0
  inx
  inx
  cpx #$20
  bne @loop
  acc8b  
  rts

; C02 Mode
.ELSE

load_palette_16:
  lda #$11
  sta VERA_addr_high
  lda #$FA
  sta VERA_addr_med
  stz VERA_addr_low
  ldx #$00
@loop:
  lda palette,x
  sta VERA_data0
  inx
  cpx #$20
  bne @loop
  rts

.ENDIF
.endproc
