; KERNAL Math ROM bank
; Bank 4

.scope math

; Format Conversions
AYINT		:=	$FE00	;	convert floating point to integer (signed word)
GIVAYF	:=	$FE03	;	convert integer (signed word) to floating point
FOUT		:=	$FE06	;	convert floating point to ASCII string
VAL_1		:=	$FE09	;	convert ASCII string in .X:.Y length in .A, to floating point in FAC. Caveat! Read below!
GETADR	:=	$FE0C	;	convert floating point to an address (unsigned word)
FLOATC	:=	$FE0F	;	convert address (unsigned word) to floating point
; X16 Additions
FLOAT		:=	$FE87	;	FAC = (s8).A convert signed byte to float	
FLOATS	:=	$FE8A	;	FAC = (s16)facho+1:facho	
QINT		:=	$FE8D	;	facho:facho+1:facho+2:facho+3 = u32(FAC)	
FOUTC		:=	$FE93	;	Convert FAC to ASCIIZ string at fbuffr - 1 + .Y	

; Movement
CONUPK	:=	$FE5A	;	move RAM MEM to ARG
ROMUPK	:=	$FE5D	;	move ROM/RAM MEM to ARG (use CONUPK)
MOVFRM	:=	$FE60	;	move RAM MEM to FACC (use MOVFM)
MOVFM		:=	$FE63	;	move ROM/RAM MEM to FACC
MOVMF		:=	$FE66	;	move FACC to RAM MEM
MOVFA		:=	$FE69	;	move ARG to FACC
MOVAF		:=	$FE6C	;	move FACC to ARG
; X16 Additions
MOVEF		:=	$FE81	; ARG = FAC (just use MOVAF)

; Math
FSUB		:=	$FE12	;	FACC = MEM - FACC
FSUBT		:=	$FE15	;	FACC = ARG - FACC
FADD		:=	$FE18	;	FACC = MEM + FACC
FADDT		:=	$FE1B	;	FACC = ARG + FACC
FMULT		:=	$FE1E	;	FACC = MEM * FACC
FMULTT	:=	$FE21	;	FACC = ARG * FACC
FDIV		:=	$FE24	;	FACC = MEM / FACC
FDIVT		:=	$FE27	;	FACC = ARG / FACC
LOG			:=	$FE2A	;	FACC = natural log of FACC
INT			:=	$FE2D	;	FACC = INT() truncate of FACC
SQR			:=	$FE30	;	FACC = square root of FACC
NEGOP		:=	$FE33	;	negate FACC (switch sign)
FPWR		:=	$FE36	;	FACC = raise ARG to the MEM power
FPWRT		:=	$FE39	;	FACC = raise ARG to the FACC power
EXP			:=	$FE3C	;	FACC = EXP of FACC
COS			:=	$FE3F	;	FACC = COS of FACC
SIN			:=	$FE42	;	FACC = SIN of FACC
TAN			:=	$FE45	;	FACC = TAN of FACC
ATN			:=	$FE48	;	FACC = ATN of FACC
ROUND		:=	$FE4B	;	FACC = round FACC
ABS			:=	$FE4E	;	FACC = absolute value of FACC
SIGN		:=	$FE51	;	.A = test sign of FACC
FCOMP		:=	$FE54	;	.A = compare FACC with MEM
RND_0		:=	$FE57	;	FACC = random floating point number
; X16 Additions
FADDH		:=	$FE6F	;	FACC += .5
ZEROFC	:=	$FE72	;	FACC = 0
NORMAL	:=	$FE75	;	Normalize FACC
NEGFAC	:=	$FE78	;	FACC = -FACC (just use NEGOP)
MUL10		:=	$FE7B	;	FACC *= 10
DIV10		:=	$FE7E	;	FACC /= 10
SGN			:=	$FE84	;	FACC = sgn(FACC)
FINLOG	:=	$FE90	;	FACC += (s8).A add signed byte to float
POLYX		:=	$FE96	;	Polynomial Evaluation 1 (SIN/COS/ATN/LOG)
POLY		:=	$FE99	;	Polynomial Evaluation 2 (EXP)

.endscope