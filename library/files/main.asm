; File handilng routines

.scope files
  .include "library/files/open_close.asm"
  .include "library/files/load_to_vram.asm"
  .include "library/files/write_page.asm"
  .include "library/files/load_module.asm"
  .include "library/files/enable_fast_io.asm"
.endscope
