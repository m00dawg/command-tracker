; Write an entire page to file
.proc write_page
  ; To count down 8192 bytes, we loop 20 times
  ; As 8192 bytes == $2000 in hex
  ldx #$20
@mci_loop:
  phx
  clc     ; Auto-advance each byte for MCIOUT
  ldx zp_PAGE_POINTER
  ldy zp_PAGE_POINTER + 1
  lda #$00  ; write 256 bytes
  jsr MCIOUT
  plx
  ; When we're done with 256 bytes, we decrement x
  ; and jump to the next 256 bytes and do it again
  dex
  beq @end
  inc zp_PAGE_POINTER + 1
  jmp @mci_loop
@end:
  rts

.endproc