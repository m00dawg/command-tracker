; Load module into allocated code space

.proc load_module

.segment "CODE"
; zp_ADDR0 = file length
; zp_ADDR1 = file pointer lo
; zp_ADDR2 = file pointer hi

FILE_NUMBER = $01
DISK_DEVICE = $08
HEADERLESS_LOAD = $02   ; Open file for reading

@start:
	lda zp_ARG0
	ldx zp_ARG1
	ldy zp_ARG2
	; Open file (a = length, x/y = pointer)
	jsr	SETNAM	;SETNAM A=FILE NAME LENGTH X/Y=POINTER TO FILENAME
	lda	#FILE_NUMBER
	ldx	#DISK_DEVICE
	ldy	#HEADERLESS_LOAD		; Y = headerless load to address
	jsr	SETLFS	;SETLFS A=LOGICAL NUMBER X=DEVICE NUMBER Y=SECONDARY

	lda #$00
	ldx #<MODULE_INIT
	ldy #>MODULE_INIT
	jsr LOAD

	rts	

.endproc