; Enables SD card fast read and write
; Note this may not work at overclocked speeds.
.segment "CODE"

.proc enable_fast_io
FAST_SERIAL_LENGTH = $05

  ; Enable Fast Reads (for all files)
	; Fast Reads
	lda #$0F
  ldx #$08
  ldy #$0F
  jsr SETLFS

	lda #FAST_SERIAL_LENGTH
  ldx #<fast_serial
  ldy #>fast_serial
  jsr SETNAM
	jsr OPEN
	lda #$0F
	jsr CLOSE

	rts

	.segment "DATA"
	fast_serial:
	.byte "u0>b1"
.endproc