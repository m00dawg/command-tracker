; Increment the look-ahead for playback scrolling
; This is here and not in the playback module since
; the ISR calls it and it seemed like things would get
; a bit wonky given the play screen is loaded on demand
.proc inc_scroll_row

@inc_screen:
  inc zp_SCROLL_SCREEN_NUMBER
  ldx zp_SCROLL_SCREEN_NUMBER
  cpx #ROW_MAX
  beq @scroll_max
@inc_row:
  inc zp_SCROLL_ROW_NUMBER
  ldx zp_SCROLL_ROW_NUMBER
  cpx #ROW_MAX       ; see if we're at the row max
  beq @row_max      ; if not, jump to end; if so, go to row_max
  rts

; This is similar to the proc in play_song but
; acrobatics of loading modules meant this is duplicated here.
@row_max:  
  stz zp_SCROLL_ROW_NUMBER
  inc zp_SCROLL_ORDER_NUMBER
	rambank zp_BANK_MISC
@load_pattern:
  ldx zp_SCROLL_ORDER_NUMBER
	lda order_list,x
  beq @start_over
	sta zp_SCROLL_PATTERN_NUMBER
  rts
@start_over:
  stz zp_SCROLL_ORDER_NUMBER
  bra @load_pattern

@scroll_max:
  stz zp_SCROLL_SCREEN_NUMBER
  bra @inc_row
.endproc
