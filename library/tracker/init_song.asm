; Init (clear) song info
.proc init_song
  ;params for MEMORY_FILL
  ADDRESS = r0
  NUM_BYTES = r1
  INIT_SONG_FILENAME_LENGTH = $07

@clear_patterns:
  ldx #$00
@empty_patterns_loop:
  inx
  stx RAM_BANK
  phx
  lda #<PATTERN_ADDRESS
  sta ADDRESS
  lda #>PATTERN_ADDRESS
  sta ADDRESS + 1

  lda #TOTAL_PATTERN_SIZE_LOW
  sta NUM_BYTES
  lda #TOTAL_PATTERN_SIZE_HIGH
  sta NUM_BYTES + 1

  lda #NOTENULL
  jsr memory_fill
  plx
  cpx zp_MAX_PATTERN
  bne @empty_patterns_loop

@blank_filename:
  ldx #$00
  lda #SCREENCODE_BLANK
@blank_loop:
  sta filename,x
  inx
  cpx #FILENAME_MAX_LENGTH
  bne @blank_loop

@load_default_song:
  ldx #$00
@load_loop:
  lda init_song_file,x
  sta filename,x
  inx
  cpx #INIT_SONG_FILENAME_LENGTH
  bne @load_loop
@load_song:
  jsr tracker::load_song

@clear_filename:
  lda #SCREENCODE_BLANK
  ldx #$00
@clear_loop:
  sta filename,x
  inx
  cpx #INIT_SONG_FILENAME_LENGTH
  bne @clear_loop
@end:
  rts

  init_song_file: .byte "new.dtr"
.endproc
