; Evaluate effects from macro

; Calling effects will still use zp_CHANNEL_NUMBER
; as this is safe to do so, though using zp_EFFECT isn't
; since that is coming from the macro

; x = chip (PSG = 0, FM = 1, PCM = 2)
; y = macro number
.proc run_macro
;channel = zp_TMP1
chip = zp_TMP3
;rambank_previous = zp_TMP4
return = zp_ADDR_RETURN

@start:
    push_rambank

    ;sta channel
    stx chip

; Lookup macro (y = macro number)
    jsr find_macro

; Having found the macro, now run through the 8 macro effects
    ldx #EFFECTS_PER_MACRO
    ldy #$00
@macro_loop:
    lda zp_BANK_MISC
    sta RAM_BANK
    lda (zp_MACRO_ADDRESS),y
    sta zp_MACRO_EFFECT_PARAMETER
    iny
    lda (zp_MACRO_ADDRESS),y
    sta zp_MACRO_EFFECT_VALUE
    iny

    phx
    phy

    ldy zp_MACRO_EFFECT_VALUE
    lda chip
    cmp #sound::VERA
    beq @vera_effect
    cmp #sound::FM
    beq @fm_effect
@pcm_effect:
    lda zp_MACRO_EFFECT_PARAMETER
    ;jsr pcm::read_effect
    nop
    bra @macro_loop_bottom
@vera_effect:
    lda zp_MACRO_EFFECT_PARAMETER
    jsr vera::read_effect
    bra @macro_loop_bottom
@fm_effect:
    lda zp_MACRO_EFFECT_PARAMETER
    jsr fm::read_effect

@macro_loop_bottom:
    ply
    plx
    dex
    bne @macro_loop

@end:
    pop_rambank
    jmp (return)
.endproc


; Get the starting memory address of given macro
; y = macro number
.proc find_macro
	rambank zp_BANK_MISC
    lda #<MACROS_START_ADDRESS
    sta zp_MACRO_ADDRESS
    lda #>MACROS_START_ADDRESS
    sta zp_MACRO_ADDRESS+1

    ; Case where y = 0
    cpy #$00
    beq @cleanup

@address_loop:
    clc
    lda zp_MACRO_ADDRESS
    adc #BYTES_PER_MACRO
    sta zp_MACRO_ADDRESS
    lda zp_MACRO_ADDRESS + 1
    adc #$00
    sta zp_MACRO_ADDRESS + 1
    dey
    bne @address_loop

@cleanup:
	rts
.endproc