; Read MIDI note from pattern data
; MATH_TEMP = volume
.proc read_note
  VOLUME = MATH_TEMP

  lda zp_ROW_NOTE
  cmp #NOTENULL
  bne @continue
  rts

@continue:
  ldx zp_CHANNEL_COUNT
@store_volume:
  lda sound::midi::channel_attenuation,x
  sta VOLUME
  lda sound::midi::channel_vol,x
  suba VOLUME
  sta VOLUME

@check_legato:
  lda sound::midi::channel_flags,x
  and #%000000011
  bne @note_on  ; Skip note off, as legato is enabled
  ; If legato is 0, send note off
@mono:
  lda VOLUME
  sta zp_ARG0
  jsr note_release
@note_on:
	; Send MIDI note on
  lda zp_ROW_NOTE
  ldx zp_CHANNEL_COUNT
  sta sound::midi::channel_note,x
  sta zp_ARG1 ; MIDI note
  lda sound::midi::channel_midi_channel,x
  sta zp_ARG0 ; MIDI channel
  ldx VOLUME
  jsr sound::midi::commands::note_on
  rts

.endproc
