; zp_ARG0 = Volume
.proc note_release
  VOLUME = zp_ARG0

	lda VOLUME
	cmp #sound::midi::MAX_VOLUME
	ble @continue
	rts

@continue:
  ldx zp_CHANNEL_COUNT
  lda sound::midi::channel_note,x
  sta zp_ARG1 ; MIDI Note
  lda sound::midi::channel_midi_channel,x
  sta zp_ARG0 ; MIDI channel
  ldx VOLUME
  jsr sound::midi::commands::note_off
  rts
.endproc