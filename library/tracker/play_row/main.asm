; Play a row in a pattern
; This assumes running in an ISR (with interrupts disabled while running)

; Moved to ZP as zp_CHANNEL_COUNT
; CHANNEL_COUNT = zp_TMP0   ; Counter to know when we're done with channels

CHANNEL_OFFSET = zp_TMP1 ; bytes to skip for each channel
; Move to ZP as zp_NOTE_FLAGS
;NOTE_FLAGS = zp_TMP2   ; bit 7 = noteoff, bit 6 = noterel, bit 5 =\

; Temp flags for routines
TEMP_FLAGS = zp_TMP2

MATH_TEMP = zp_MATH0

; For calculating fine pitch (from instrument)
NOTE_SEMITONE = zp_MATH1

; Deprecated
FULL_PITCH_16 = zp_ADDR0

FINE_PITCH_16 = zp_ADDR1
PITCH_RESULT_16 = zp_ADDR2

.scope vera
  .include "library/tracker/play_row/vera/check_flags.asm"
  .include "library/tracker/play_row/vera/read_note.asm"
  .include "library/tracker/play_row/vera/read_instrument.asm"
  .include "library/tracker/play_row/vera/read_volume.asm"
  .include "library/tracker/play_row/vera/read_effect.asm"
  ;.include "library/tracker/play_row/vera/update_vera.asm"
.endscope

.scope pcm
  .include "library/tracker/play_row/pcm/read_note.asm"
  .include "library/tracker/play_row/pcm/read_instrument.asm"
  .include "library/tracker/play_row/pcm/read_volume.asm"
.endscope

.scope fm
  .include "library/tracker/play_row/fm/read_instrument.asm"
  .include "library/tracker/play_row/fm/read_note.asm"
  .include "library/tracker/play_row/fm/read_volume.asm"
  .include "library/tracker/play_row/fm/read_effect.asm"
.endscope

.scope midi
  .include "library/tracker/play_row/midi/check_flags.asm"
  .include "library/tracker/play_row/midi/note_release.asm"
  .include "library/tracker/play_row/midi/read_volume.asm"
  .include "library/tracker/play_row/midi/read_note.asm"
  .include "library/tracker/play_row/midi/read_effect.asm"
  .include "library/tracker/play_row/midi/read_instrument.asm"
.endscope

.include "library/tracker/play_row/read_row.asm"
.include "library/tracker/play_row/run_macro.asm"


.proc play_row 
  
  ; Note flags:
  ; Bit 7 = Note Play
  ; Bit 6 = Release
  ; Bit 5 = Off
  ; Bit 4 = Null
  ; Bit 3 = Legato

  ; Setup for writing to vera fast
  lda #%00010001
  sta VERA_addr_high
  lda #sound::vera::HIGH_BYTE
  sta VERA_addr_med

  sei
  rambank zp_PATTERN_NUMBER
  stz CHANNEL_OFFSET
  jsr vera_channels
  jsr fm_channels
  jsr pcm_channel
  cli

  rts
.endproc

; Loop through VERA channels ($00-$0F)
.proc vera_channels
  stz zp_CHANNEL_COUNT
@channel_loop:
  stz zp_NOTE_FLAGS
  jsr read_row

@check_channel_type:
  ldx zp_CHANNEL_COUNT
  lda sound::vera::channel_mode,x
  cmp #sound::vera::PSG
  beq @psg
  cmp #sound::vera::WAVETABLE
  beq @wavetable
@midi:  ; Not yet implemented
  bra @channel_loop_bottom
@wavetable:
  ; Set from a
  jsr sound::midi::set_address
  jsr midi::check_flags
  
  ; This is becuase we may use a macro in the future
  ; (see the vera section)
  lda zp_ROW_EFFECT_PARAMETER
  ldy zp_ROW_EFFECT_VALUE
  jsr midi::read_effect
  
  jsr midi::read_instrument
  jsr midi::read_volume
  jsr midi::read_note
  bra @channel_loop_bottom
  
@psg:
  jsr vera::check_flags
  ; Evaluate and update channel registers
  jsr vera::read_instrument
  ; Some effects need to trigger _before_ note is read
  ; (finetune, e.g.)

  ; Special check since macro is at the top ($3F)
  ; And if so we run the macro (and skip the normal effect read)
@check_for_macro:
  lda zp_ROW_EFFECT_PARAMETER
  cmp #sound::MACRO
  bne @read_effect
@run_macro:
  lda zp_CHANNEL_COUNT
  ldx #sound::VERA  ; VERA id
  ldy zp_ROW_EFFECT_VALUE
  jsr run_macro
  bra @read_note
@read_effect:
  lda zp_ROW_EFFECT_PARAMETER
  ldy zp_ROW_EFFECT_VALUE
  jsr vera::read_effect
@read_note:
  jsr vera::read_note
  jsr vera::read_volume 

@channel_loop_bottom:
  jsr update_channel_offset
  cmp #sound::vera::NUM_CHANNELS
  beq @end
@channel_loop_jump:
  jmp @channel_loop
@end:
  rts
.endproc

; Loop through FM channels ($10-$17)
.proc fm_channels
  stz zp_CHANNEL_COUNT
@channel_loop:
  stz zp_NOTE_FLAGS
  jsr read_row

@check_note_flags:
  lda zp_ROW_NOTE
  cmp #NOTEREL
  beq @note_rel
  cmp #NOTEOFF
  beq @note_off
  cmp #NOTENULL
  beq @note_null
  jmp @continue

@note_rel:
  smb 6,zp_NOTE_FLAGS
  ldx zp_CHANNEL_COUNT
  lda sound::fm::channel_flags,x
  and #%01111111
  sta sound::fm::channel_flags,x
  jsr sound::fm::key_control
  bra @channel_loop_bottom
@note_off:
  ; Set note off flag, reset note play flag
  smb 5,zp_NOTE_FLAGS
  ; Set channel active flag to off
  ; and volume to zero
  ldx zp_CHANNEL_COUNT
  lda sound::fm::channel_flags,x
  and #%01111111
  sta sound::fm::channel_flags,x
  lda #$00
  sta sound::fm::channel_volume,x
  jsr sound::fm::key_control
  jsr sound::fm::update_volume
  bra @channel_loop_bottom
@note_null:
  smb 4,zp_NOTE_FLAGS

@continue:
  jsr fm::read_instrument

; Special check since macro is at the top ($3F)
  ; And if so we run the macro (and skip the normal effect read)
@check_for_macro:
  lda zp_ROW_EFFECT_PARAMETER
  cmp #sound::MACRO
  bne @read_effect
@run_macro:
  lda zp_CHANNEL_COUNT
  ldx #sound::FM  ; FM id
  ldy zp_ROW_EFFECT_VALUE
  jsr run_macro
  bra @read_note

@read_effect:
  lda zp_ROW_EFFECT_PARAMETER
  ldy zp_ROW_EFFECT_VALUE
  jsr fm::read_effect
@read_note:
  jsr fm::read_note
  jsr fm::read_volume

  ; Play note if note active
  bbr 7, zp_NOTE_FLAGS,@channel_loop_bottom
  ldx zp_CHANNEL_COUNT
  jsr sound::fm::update_note
  jsr sound::fm::key_control

@channel_loop_bottom:
  jsr update_channel_offset
  cmp #sound::fm::NUM_CHANNELS
  bne @channel_loop_jump
@end:
  rts
@channel_loop_jump:
  jmp @channel_loop
.endproc

; Process PCM channel ($18)
.proc pcm_channel
  stz zp_NOTE_FLAGS
  
  ; This should not be here (commented out obviously)
  ; because it messes up the buffer march
  ; DON'T UNCOMMENT THIS!
  ;stz zp_PCM_FLAGS

  jsr read_row
  lda zp_ROW_NOTE
  cmp #NOTEREL
  beq @note_rel
  cmp #NOTEOFF
  beq @note_off
  cmp #NOTENULL
  beq @note_null
  bra @continue  
@note_rel:
  ;TBD
@note_off:
  ; Set note off flag, reset note play flag
  smb 5,zp_NOTE_FLAGS
  ; Set channel active flag to off
  ; and volume to zero
  smb 7,zp_PCM_FLAGS
  stz zp_PCM_VOLUME
  rts
@note_null:
  ; Set null flag and continue since some 
  ; items (like volume) still need to function
  ; with null notes
  smb 4,zp_NOTE_FLAGS
  rts
@continue:
  jsr pcm::read_instrument
  jsr pcm::read_note
  jsr pcm::read_volume
  jsr sound::zsm::trigger_pcm
.endproc

; Update channel offset from pattern and increment
; channel count, noting the channel count is 
; reset for each type (PSG, FM, PCM)
.proc update_channel_offset
  add CHANNEL_OFFSET, #TOTAL_BYTES_PER_CHANNEL
  sta CHANNEL_OFFSET
  inc zp_CHANNEL_COUNT
  lda zp_CHANNEL_COUNT
  rts
.endproc