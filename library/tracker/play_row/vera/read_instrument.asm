; Read instrument from pattern data
.proc read_instrument
  ; If we have a note-off skip
  ; we check for this before
  ; bbs 5, NOTE_FLAGS, @end 

  lda zp_ROW_INSTRUMENT
  cmp #INSTNULL
  beq @end
@inst_check:
  ; If instrument number is different, we need to
  ; load the instrument defaults into the channel
  ldx zp_CHANNEL_COUNT
  lda sound::vera::channel_instrument,x
  cmp zp_ROW_INSTRUMENT
  beq @end
  ; If the instrument is different than the current
  ; instrument for the channel, load it in
@inst_update_channel:
  lda zp_CHANNEL_COUNT
  sta zp_ARG0
  lda zp_ROW_INSTRUMENT
  sta zp_ARG1
  jsr sound::vera::update_channel_instrument
@end:
  rts

.endproc