; Read volume from pattern data
; First we check for note-off and if we have one, skip
; (we handle note off later)


; If note was played
;   if colum has non-volume commands, use instrument volume
;   if colume has volume, use volume
; If note was not played
;   For anything in the volume col, do that.

.proc read_volume  
  ldx zp_CHANNEL_COUNT
  ; Was a note played?
  bbr 7, zp_NOTE_FLAGS, @note_not_played
  
@note_played:
  ; Check if legato is enabled and if so do not update volume
  lda sound::vera::channel_flags,x
  and #%0000010
  beq @note_played_continue
  rts

@note_played_continue:
  ; check if volume column is null
  ; if so, use instrument volume
  lda zp_ROW_VOLUME
  cmp #VOLNULL
  bne @note_played_volume_not_null
@note_played_volume_null:
  jsr use_instrument_volume
  rts

@note_not_played:
  ; check if volume column is null
  ; and there is no note, do nothing.
  ; else set volume
  lda zp_ROW_VOLUME
  cmp #VOLNULL
  bne @note_not_played_volume_not_null
  rts

@note_played_volume_not_null:
  ; Note played. If volume column has volume ($00-3F), use that
  ; Otherwise use instrument volume
  cmp #VOLUME_COLUMN_MAX_VOL + 1
  blt @evaluate_effects
  jsr use_instrument_volume

@note_not_played_volume_not_null:
  ; Note wasn't played but we have a command in the column
  ; So set it
@evaluate_effects:
  jsr check_volume
  jsr check_attenuation
  jsr check_pan
  jsr check_pwm
  jsr check_wave
  rts
.endproc

; Helpers

; Set channel volume from volume column if
; the value is from 00-3F
.proc check_volume
  lda zp_ROW_VOLUME
  cmp #VOLUME_COLUMN_MAX_VOL + 1
  bge @end
  and #%00111111
  sta MATH_TEMP
  lda sound::vera::channel_vol,x
  and #%11000000
  ora MATH_TEMP
  sta sound::vera::channel_vol,x
@end:
  rts
.endproc

; Set channel attenuation from volume column 
; if value is between $40 and $7F
.proc check_attenuation
  lda zp_ROW_VOLUME
  cmp #VOLUME_COLUMN_ATT_START_RANGE
  bge @check_max
  rts
@check_max:
  cmp #VOLUME_COLUMN_PWM_START_RANGE
  blt @continue
  rts
@continue:
  and #%00111111
  sta sound::vera::channel_attenuation,x
@end:
  rts
.endproc


; If the value is C3-C6, set wave
.proc check_wave
@check_wave:
  lda zp_ROW_VOLUME
  cmp #VOLUME_COLUMN_SET_WAVE_PWM
  beq @pwm
  cmp #VOLUME_COLUMN_SET_WAVE_SAW
  beq @saw
  cmp #VOLUME_COLUMN_SET_WAVE_TRI
  beq @tri
  cmp #VOLUME_COLUMN_SET_WAVE_NOISE
  beq @noise
  rts

@pwm:
  lda #%00000000
  bra @set_wave
@saw:
  lda #%01000000
  bra @set_wave
@tri:
  lda #%10000000
  bra @set_wave
@noise:
  lda #%11000000
  bra @set_wave  

@set_wave:
  sta MATH_TEMP
  lda RAM_BANK
  pha
  rambank zp_BANK_VINS
  ldy sound::vera::channel_instrument,x
  lda vera_instruments_wave,y
  sta sound::vera::channel_wave,x
  pla
  sta RAM_BANK

  lda sound::vera::channel_wave,x
  and #%00111111
  ora MATH_TEMP
  sta sound::vera::channel_wave,x
  rts
.endproc

; If value is between $80 and $BF, set PWM
.proc check_pwm
@set_pwm:
  lda zp_ROW_VOLUME
  cmp #VOLUME_COLUMN_PWM_START_RANGE
  bge @check_max
  rts
@check_max:
  cmp #VOLUME_COLUMN_EFFECT_FLAGS_START_RANGE
  blt @continue
  rts
@continue:
  and #%00111111
  sta MATH_TEMP
  lda sound::vera::channel_wave,x
  and #%11000000
  ora MATH_TEMP
  sta sound::vera::channel_wave,x
  rts
.endproc

; If the volume is C0, C1 or C2 - it's pan so do that instead
; (but play instrument volume)
.proc check_pan
@check_pan:
  lda zp_ROW_VOLUME
  cmp #VOLUME_COLUMN_PAN_CENTER
  beq @center
  cmp #VOLUME_COLUMN_PAN_LEFT
  beq @left
  cmp #VOLUME_COLUMN_PAN_RIGHT
  beq @right
  rts
@center:
  lda #%11000000
  jmp @set_pan
@left:
  lda #%01000000
  jmp @set_pan
@right:
  lda #%10000000
@set_pan:
  sta MATH_TEMP
  lda sound::vera::channel_vol,x
  and #%00111111
  ora MATH_TEMP
  sta sound::vera::channel_vol,x
  rts
.endproc

; helper to set instrment volume
.proc use_instrument_volume
  push_rambank
  rambank zp_BANK_VINS

  ldy sound::vera::channel_instrument,x
  lda vera_instruments_vol,y
  and #%00111111
  sta MATH_TEMP
  lda sound::vera::channel_vol,x
  and #%11000000
  ora MATH_TEMP  
  sta sound::vera::channel_vol,x

  pop_rambank
  
  rts
.endproc