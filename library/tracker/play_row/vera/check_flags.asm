.proc check_flags
@check_note_flags:
  lda zp_ROW_NOTE
  cmp #NOTEREL
  beq @note_rel
  cmp #NOTEOFF
  beq @note_off
  cmp #NOTENULL
  beq @note_null
  rts

; same as note off for now
@note_rel:
  smb 6,zp_NOTE_FLAGS
  ; Set channel release flag to on
  ldx zp_CHANNEL_COUNT
  lda sound::vera::channel_flags,x
  ora #%01000000
  sta sound::vera::channel_flags,x
  rts
@note_off:
  ; Set note off flag, reset note play flag
  smb 5,zp_NOTE_FLAGS
  ; Set channel active flag to off
  ; and volume to zero
  ldx zp_CHANNEL_COUNT
  lda sound::vera::channel_flags,x
  and #%01111111
  sta sound::vera::channel_flags,x
  lda sound::vera::channel_vol,x
  and #%11000000
  sta sound::vera::channel_vol,x
  rts
@note_null:
  ; Set null flag and continue since some 
  ; items (like volume) still need to function
  ; with null notes
  smb 4,zp_NOTE_FLAGS

	rts
.endproc