  ; Update VERA
.proc update_vera
  ; If we have a noteoff, write 0's
  bbs 5, zp_NOTE_FLAGS, @channel_inactive 
  phx
  ldx zp_CHANNEL_COUNT
  jsr sound::vera::update_channel_registers
  plx
  rts
@channel_inactive:
;  ; Write 0's to vera channel if inactive
  stz VERA_data0
  stz VERA_data0
  stz VERA_data0
  stz VERA_data0
  rts
.endproc