; Read note from pattern data
; zp_TMP0 = legato flag

.proc read_note
  LEGATO_FLAG = zp_TMP0

  ; Check for null instrument
  lda zp_ROW_INSTRUMENT
  cmp #INSTNULL
  beq @end

  ; Check if note should be delayed
  lda zp_ROW_EFFECT_PARAMETER
  cmp #sound::NOTE_DELAY_EFFECT_NUMBER
  beq @end

  ; Set note to active
  smb 7,zp_NOTE_FLAGS
  
  ; Prep update_note:
  ; x = channel
  ; y = note
  ldx zp_CHANNEL_COUNT
  ldy zp_ROW_NOTE
  jsr sound::vera::update_note
@end:
  rts
.endproc