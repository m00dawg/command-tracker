; Read volume from pattern data
; First we check for note-off and if we have one, skip
; (we handle note off later)


; If note was played
;   if colum has non-volume commands, use instrument volume
;   if colume has volume, use volume
; If note was not played
;   For anything in the volume col, do that.

; Constants
  VOLUME_COLUMN_MAX_SYNTH_VOL = $0F

.proc read_volume  
  ; Was a note played?
  bbr 7, zp_NOTE_FLAGS, @note_not_played
  
@note_played:
  ; check if volume column is null
  ; if so, use instrument volume
  lda zp_ROW_VOLUME
  cmp #VOLNULL
  bne @note_played_volume_not_null
@note_played_volume_null:
  jsr use_instrument_volume
  rts

@note_not_played:
  ; check if volume column is null
  ; and there is no note, do nothing.
  ; else set volume
  lda zp_ROW_VOLUME
  cmp #VOLNULL
  bne @note_not_played_volume_not_null
  rts

@note_played_volume_not_null:
  ; Note played. If volume column has volume ($00-0F), use that
  ; Otherwise use instrument volume
  cmp #VOLUME_COLUMN_MAX_SYNTH_VOL + 1
  blt @evaluate_effects
  jsr use_instrument_volume
  rts
@note_not_played_volume_not_null:
  ; Note wasn't played but we have a command in the column
  ; So set it
@evaluate_effects:
  jsr check_volume
  rts
.endproc

; Helpers

; Set channel volume from volume column if
; the value is from 00-0F
.proc check_volume
  lda zp_ROW_VOLUME
  cmp #VOLUME_COLUMN_MAX_SYNTH_VOL + 1
  bge @end
  and #%00001111
  sta zp_PCM_VOLUME
@end:
  rts
.endproc

; Set channel attenuation
.proc check_attenuation
  rts
.endproc

; helper to set instrment volume
.proc use_instrument_volume
  rambank zp_BANK_MISC
  ldx zp_PCM_SAMPLE_NUMBER
  lda sample_states,x
  and #%00001111
  sta zp_PCM_VOLUME
  rts
.endproc