; Read FM note from pattern data
.proc read_note
  lda zp_ROW_INSTRUMENT
  cmp #INSTNULL
  beq @end

  ; Check if note should be delayed
  lda zp_ROW_EFFECT_PARAMETER
  cmp #sound::NOTE_DELAY_EFFECT_NUMBER
  beq @end

  ldx zp_CHANNEL_COUNT
@note_play:
  stz sound::fm::channel_note_delay,x
  ; Set play flag and channel active
  smb 7,zp_NOTE_FLAGS
  lda sound::fm::channel_flags,x
  ora #%10000000
  sta sound::fm::channel_flags,x

@continue:
; Check for glide and if found update the glide pitch
; instead as long as it's non-zero
  lda zp_ROW_EFFECT_PARAMETER
  cmp #sound::fm::GLIDE_EFFECT_NUMBER
  beq @update_glide_pitch
; If glide speed is set on the channel, also set the glide
; pitch instead
  lda sound::fm::channel_glide_speed,x
  bne @update_glide_pitch

@update_channel_pitch:
  lda #$00
  sta sound::fm::channel_semitone,x
  lda zp_ROW_NOTE
  sta sound::fm::channel_note,x
  rts

@update_glide_pitch:
  ; Check for zero (if zero, we reset glide)
  lda zp_ROW_EFFECT_VALUE
  beq @update_channel_pitch

  lda #$00
  sta sound::fm::channel_glide_semitone,x
  lda zp_ROW_NOTE
  sta sound::fm::channel_glide_note,x
@end:
  rts

.endproc