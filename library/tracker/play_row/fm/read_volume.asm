; Read volume from pattern data
; First we check for note-off and if we have one, skip
; (we handle note off later)

; If note was played
;   if colum has non-volume commands, use instrument volume
;   if colume has volume, use volume
; If note was not played
;   For anything in the volume col, do that.

; zp_ARG0 = channel (for sound::fm::read_volume)
; zp_TMP2 (TEMP_FLAGS) = Update Volume Flag

.proc read_volume
  update_volume_flag = TEMP_FLAGS
  stz TEMP_FLAGS

  ldx zp_CHANNEL_COUNT
  ; Was a note played?
  bbr 7, zp_NOTE_FLAGS, @note_not_played
  
@note_played:
  ; check if volume column is null
  ; if so, use instrument volume
  lda zp_ROW_VOLUME
  cmp #VOLNULL
  bne @note_played_volume_not_null
@note_played_volume_null:
  jsr use_instrument_volume
  jsr write_volume_to_fm
  rts

@note_not_played:
  ; check if volume column is null
  ; and there is no note, do nothing.
  ; else set volume
  lda zp_ROW_VOLUME
  cmp #VOLNULL
  bne @note_not_played_volume_not_null
  rts

@note_played_volume_not_null:
  ; Note played. If volume column has volume ($00-3F), use that
  ; Otherwise use instrument volume
  cmp #VOLUME_COLUMN_MAX_VOL + 1
  blt @evaluate_effects
  ; Same as above, just use volume when we loaded the instrument
  ;jsr use_instrument_volume

@note_not_played_volume_not_null:
  ; Note wasn't played but we have a command in the column
  ; So set it
@evaluate_effects:
  jsr check_volume
  ;jsr check_attenuation
  jsr check_pan
  ;jsr check_legato
  jsr write_volume_to_fm

  rts
.endproc

; Helpers

; Set channel volume from volume column if
; the value is from 00-7F
.proc check_volume
  channel = zp_ARG0
  update_volume_flag = TEMP_FLAGS

  lda zp_ROW_VOLUME
  cmp #$80
  blt @volume
  rts
@volume:
  sta sound::fm::channel_volume,x
  lda #$01
  sta update_volume_flag
.endproc


; If the volume is C0, C1 or C2 - it's pan so do that instead
; (but play instrument volume)
.proc check_pan
@check_pan:
  lda zp_ROW_VOLUME
  cmp #VOLUME_COLUMN_PAN_CENTER
  beq @center
  cmp #VOLUME_COLUMN_PAN_LEFT
  beq @left
  cmp #VOLUME_COLUMN_PAN_RIGHT
  beq @right
  rts
@center:
  lda #%11000000
  jmp @set_pan
@left:
  lda #%01000000
  jmp @set_pan
@right:
  lda #%10000000
@set_pan:
  sta MATH_TEMP
  lda sound::fm::channel_pan_feedback_algo,x
  and #%00111111
  ora MATH_TEMP
  sta sound::fm::channel_pan_feedback_algo,x
  tay
  stx MATH_TEMP
  add #YM_PAN_FEEDBACK_ALGO_BASE, MATH_TEMP
  jsr sound::fm::write_register
  
  rts
.endproc

; helper to set instrment volume
.proc use_instrument_volume
  channel = zp_ARG0
  update_volume_flag = TEMP_FLAGS

  push_rambank
  rambank zp_BANK_FINS
  ldx zp_CHANNEL_COUNT
  ldy sound::fm::channel_instrument,x
  lda fm_instruments_global_volume,y
  sta sound::fm::channel_volume,x

  lda #$01
  sta update_volume_flag
  pop_rambank
  
  rts
.endproc

; Write volume to FM if applicable
.proc write_volume_to_fm
  channel = zp_ARG0
  update_volume_flag = TEMP_FLAGS
  lda update_volume_flag
  bne @update
  rts
@update:
  txa
  sta channel
  jsr sound::fm::update_volume
  rts
.endproc