; Tracker Interrupts
.scope interrupts

  ISR = zp_ADDR0
  VERA_SYNC = $00
  VIA_SYNC = $01
  MIDI_SYNC = $02

  .include "library/tracker/interrupts/vsync.asm"
  .include "library/tracker/interrupts/via.asm"
  .include "library/tracker/interrupts/midi_isr.asm"

  isr_jump_table_lo:
    .byte <vsync
    .byte <via
    .byte <midi_isr
  isr_jump_table_hi:
    .byte >vsync
    .byte >via
    .byte >midi_isr

  ; Setup playback IRQ
  .proc play_irq
  @check_sync:
    push_rambank
    rambank zp_BANK_MISC

    ldx sync_method
    lda isr_jump_table_lo,x
    sta ISR
    lda isr_jump_table_hi,x
    sta ISR + 1

  @enable_irq:
    ; Setup irq handler
    ; We load the address of our interrupt handler into a special memory
    ;   location. Basically when an interrupt triggers, this is the
    ;   routine the CPU will execute.
    ldx #$00
    lda ISR_HANDLER,x
    sta previous_isr_handler,x
    lda ISR
    sta ISR_HANDLER,x
    inx
    lda ISR_HANDLER,x
    sta previous_isr_handler,x
    lda ISR + 1
    sta ISR_HANDLER,x

    ; Enable vsync (it's already on by default)
    lda #VBLANK_MASK
    sta VERA_ien

  @midi_interrupt:
    ; If sync method is 2 (MIDI), enable RX interrupt
    ldx sync_method
    cpx #MIDI_SYNC
    bne @end
    jsr sound::midi::init

    ; Since we don't update VERA, we can also disable the interrupt
    ; stz VERA_ien
    ; We are using Vblank for PCM buffering
    lda #VBLANK_MASK
    sta VERA_ien


    ; Interrupts don't fire on IRQ/NMI without enabling bit 3
    ; of the modem register for some reason
    lda #sound::midi::INTR_ENABLE
    ldy #sound::midi::INTERRUPT_ENABLE_OFFSET
    sta (zp_MIDI_BASE),y

    lda #sound::midi::MODEM_INT_ENABLE
	  ldy #sound::midi::MODEM_CONTROL_OFFSET
	  sta (zp_MIDI_BASE),y

  @end:
    pop_rambank
    rts
  .endproc

  ; Restore the previous IRQ routine
  .proc disable_irq

    lda #VBLANK_MASK
    sta VERA_ien

  @disable_midi:
    ; Disable MIDI interrupts
    push_rambank
    rambank zp_BANK_MISC
    ldx sync_method
    cpx #MIDI_SYNC
    bne @remove_irq_handler
    jsr sound::midi::init

  @remove_irq_handler:
    ldx #$0
    lda previous_isr_handler,x
    sta ISR_HANDLER,x
    inx
    lda previous_isr_handler,x
    sta ISR_HANDLER,x
    pop_rambank
    rts
  .endproc

.endscope