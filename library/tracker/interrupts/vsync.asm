; VSYNC playback routine
.proc vsync
  push_state_disable_interrupts
  ;push_rambank
 
  ; Set native mode
  native


  ; Preserve VERA stuff that might change out of the interrupt
  lda VERA_addr_high
  pha
  lda VERA_addr_med
  pha
  lda VERA_addr_low
  pha
  lda VERA_ctrl
  pha

@vblank_next:
  ; Check to see if the VBLANK was triggered. This is in case the intterupt
  ;   was triggered by something else. We just want VBLANK.
  lda VERA_isr        ; Load contents of VERA's ISR
  and #%00000001      ; Mask for the interrupt
  beq @vblank_end     ; If zero, VERA wasn't the interrupt

@vblank: 
  lda #1
	sta VERA_isr    ;ack VERA VBLANK

  ; Update the sound engine on every vblank
  jsr sound::tick::update
  ; Update PCM on every vblank
  jsr sound::pcm::update_pcm

  ; The rest is based on speed of the song
  ; (speed being how many skips we do)
  ldx zp_VBLANK_SKIP_COUNT
  cpx zp_SPEED
  beq @vblank_work

@inc_vblank_skip:
  inc zp_VBLANK_SKIP_COUNT
  bra @vblank_end

; Here we do actual work in the vblank
; First we zero out the vblank skip then jsr over to whatever routines
; we need to do for the vblank
@vblank_work:
  stz zp_VBLANK_SKIP_COUNT

  ; Only evaluate getting the next pattern if we are playing the entire song.
  ; Otherwise, skip past it.
  lda zp_STATE
  cmp #PLAY_SONG_STATE
  bne @get_row
  jsr tracker::get_next_pattern

@get_row:
  lda zp_ROW_NUMBER
  jsr tracker::get_row             ; get the current row of pattern, put in zp_ROW_POINTER
  lda zp_ARG0
  sta zp_ROW_POINTER
  lda zp_ARG0 + 1
  sta zp_ROW_POINTER + 1
  jsr tracker::play_row
  jsr ui::scroll_up
  jsr tracker::inc_scroll_row
  jsr tracker::effects::evaluate_scroll_effect
  jsr tracker::inc_row

@vblank_end:
  ; Return VERA state
  pla
  sta VERA_ctrl
  pla
  sta VERA_addr_low
  pla
  sta VERA_addr_med
  pla
  sta VERA_addr_high

  ;pop_rambank
  emulated
  plp       ; Reset previous p-state (presumably re-enable interrupts)
  
;jmp (previous_isr_handler)        ; Pass control to the previous handler
; Instead of jumping ot previous ISR, we ignore it here since 
; we don't wanna scan a buncha stuff we don't care about
; (like mouse or joysticks) while playing the song.
; Actually, do nothing. We'll do keys in the outer loop
@kernal_upkeep:
	;jsr ps2data_fetch
  ;jsr kbd_scan
  ;jsr SCNKEY
  
@clear_vblank_interrupt:
; I'm not sure where we pushed these :P but if these aren't here, bad
; things happen. (I think this comes from part of fixing things after
; an interupt that we sort of short circuit with php/plp and skipping
; the kernel isr...maybe.
	ply
	plx
	pla
  rti
.endproc
