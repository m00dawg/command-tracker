; MIDI playback routine
;
; For the first pass, this will use vsync for the synth engine
; and MIDI for the row advancing.
;
; Later versions will adopt VIA timing
.proc midi_isr
  push_state_disable_interrupts
 
  ; Set native mode
  native

  ; Preserve VERA stuff that might change out of the interrupt
  lda VERA_addr_high
  pha
  lda VERA_addr_med
  pha
  lda VERA_addr_low
  pha
  lda VERA_ctrl
  pha

@vblank: 
  ; Check to see if the VBLANK was triggered. This is in case the intterupt
  ;   was triggered by something else. We just want VBLANK.
  lda VERA_isr        ; Load contents of VERA's ISR
  and #%00000001      ; Mask for the interrupt
  beq @check_midi     ; If zero, VERA wasn't the interrupt
  lda #1
	sta VERA_isr    ;ack VERA VBLANK
  ; Update PCM on every vblank
  jsr sound::pcm::update_pcm
  bra @cleanup

; Check if we received a MIDI message
@check_midi:
  ldy #sound::midi::INTERRUPT_IDENT_OFFSET
  lda (zp_MIDI_BASE),y
	and #%00000001
	; If zero, interrupt is pending, so read it (continue)
	bne @cleanup
@read_buffer:
  ; Read the actual buffer
  ldy #sound::midi::RX_BUFFER_OFFSET
  lda (zp_MIDI_BASE),y

  ;sta zp_MIDI_DEBUG
@clock:
	cmp #sound::midi::message::CLOCK
	bne @song_start

; If we got a clock, we need to first increment the clock counter,
; as we only advance a row based on the song speed value
; (Speed of 6 = 32th note per row)
@clock_received:
  ; Check if the song is running (we received a MIDI START at some point)
  lda zp_MIDI_STATE
  beq @cleanup
  ; Here just to see how this might go
  jsr sound::tick::update
  inc zp_MIDI_CLOCK_COUNTER
  lda zp_MIDI_CLOCK_COUNTER
  cmp zp_SPEED
  bne @cleanup

@update_row:
  stz zp_MIDI_CLOCK_COUNTER
  ; Only evaluate getting the next pattern if we are playing the entire song.
  ; Otherwise, skip past it.
  lda zp_STATE
  cmp #PLAY_SONG_STATE
  bne @get_row
  jsr tracker::get_next_pattern
@get_row:
  lda zp_ROW_NUMBER
  jsr tracker::get_row             ; get the current row of pattern, put in zp_ROW_POINTER
  lda zp_ARG0
  sta zp_ROW_POINTER
  lda zp_ARG0 + 1
  sta zp_ROW_POINTER + 1
  jsr tracker::play_row
  jsr ui::scroll_up
  jsr tracker::inc_scroll_row
  jsr tracker::effects::evaluate_scroll_effect
  jsr tracker::inc_row
  bra @cleanup

; If MIDI Start was sent ($FA), start song
@song_start:
  cmp #sound::midi::message::START
  bne @song_stop
  inc zp_MIDI_STATE
  bra @cleanup

; IF MIDI STop was sent ($FC), stop song
@song_stop:
  cmp #sound::midi::message::STOP
  stz zp_MIDI_STATE
  
@cleanup:
  ; Return VERA state
  pla
  sta VERA_ctrl
  pla
  sta VERA_addr_low
  pla
  sta VERA_addr_med
  pla
  sta VERA_addr_high

  emulated
  plp
  
; Instead of jumping to previous ISR, we ignore it here since 
; we don't wanna scan a buncha stuff we don't care about
; (like mouse or joysticks) while playing the song.
; Actually, do nothing. We'll do keys in the outer loop
;jmp (previous_isr_handler)        ; Pass control to the previous handler
@kernal_upkeep:
	;jsr ps2data_fetch
  ;jsr kbd_scan
  ;jsr SCNKEY
  
 ; I'm not sure where we pushed these :P but if these aren't here, bad
; things happen. (I think this comes from part of fixing things after
; an interupt that we sort of short circuit with php/plp and skipping
; the kernel isr...maybe.
	ply
	plx
	pla
  rti
.endproc
