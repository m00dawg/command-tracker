; This effect does nothing but return
; useful for organizing the effects between PSG / FM
; while there are gaps.
.proc null_effect
  rts
.endproc