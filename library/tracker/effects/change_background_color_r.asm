; This is silly.
; Change the play screen background color during playback
;
; y = color

.proc change_background_color_r

	lda VERA_addr_high
	pha
	lda VERA_addr_med
	pha
	lda VERA_addr_low
	pha

  lda #$11
  sta VERA_addr_high
  lda #$FA
  sta VERA_addr_med
	lda #$17
  sta VERA_addr_low
@loop:
  sty VERA_data0

	pla
	sta VERA_addr_low
	pla
	sta VERA_addr_med
	pla
	sta VERA_addr_high

  rts

.endproc