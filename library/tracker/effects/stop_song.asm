; Stop song as a pattern effect

.proc stop_song
    jsr tracker::stop_song
    jmp main_application_loop
.endproc