; Set global transpose
; y = signed transpose value

.proc set_global_transpose
return = zp_ADDR_RETURN

@start:
  lda RAM_BANK
  pha
  rambank zp_BANK_MISC
  sty global_transpose
  pla
  sta RAM_BANK
  jmp (return)
.endproc