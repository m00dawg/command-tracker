.proc stop_song
stop_song:
  push_state_disable_interrupts

  ; Check if we're already stopped (0)
  lda zp_STATE
  beq stopping_song
  jsr tracker::interrupts::disable_irq
stopping_song:
  stz VERA_audio_ctrl
  jsr sound::vera::init
  jsr sound::vera::init_channel_registers
  jsr sound::fm::init
  jsr sound::midi::init

  stz zp_STATE  ; 0 = Stopped


  jsr ui::update_upper_info
  ;jsr ui::update_voices

  jsr sound::zsm::stop
  stz zp_ZSM_ENABLE

  plp
  rts

.endproc
