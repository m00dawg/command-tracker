.proc inc_row

inc_row:
  inc zp_ROW_NUMBER

  lda zp_STATE
  cmp #PLAY_PATTERN_STATE
  beq @continue
  rts
@continue:
  ldx zp_ROW_NUMBER
  cpx #ROW_MAX      ; see if we're at the row max
  beq @row_max      ; if not, jump to end; if so, go to row_max
  rts
@row_max:  
  stz zp_ROW_NUMBER
  rts

.endproc
