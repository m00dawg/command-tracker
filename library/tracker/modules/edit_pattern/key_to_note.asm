.proc key_to_note
@start:
  sbc #$2F      ; Subtract to offset the values for the LUT
  tay
  lda petscii_to_note,y
  sta zp_NOTE_NUMERIC

  cmp #NOTEREL
  beq @key_to_note_print_note
  cmp #NOTEOFF
  beq @key_to_note_print_note
  cmp #NOTENULL
  beq @key_to_note_print_note
@key_to_note_octave:
  ; Add the users' selected octave
  lda user_octave
  jsr math::multiply_by_12
  clc
  adc zp_NOTE_NUMERIC
  sta zp_NOTE_NUMERIC

@key_to_note_print_note:

  lda #PATTERN_NOTE_COLOR
	sta zp_ARG0
	sta zp_ARG1
	sta zp_ARG2
	sta zp_ARG3
  jsr ui::print_note
  rts

.endproc