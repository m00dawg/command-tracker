; Print the block info along the top
.proc print_block
	lda #BLOCK_START_X
	ldy #BLOCK_START_Y
	jsr graphics::drawing::goto_xy
	lda block_source_pattern
	jsr graphics::printing::print_hex

	lda #BLOCK_START_X + $03
	ldy #BLOCK_START_Y
	jsr graphics::drawing::goto_xy
	lda block_begin_channel
	jsr graphics::printing::print_hex

	lda #BLOCK_START_X + $06
	ldy #BLOCK_START_Y
	jsr graphics::drawing::goto_xy
	lda block_begin_row
	jsr graphics::printing::print_hex

	lda #BLOCK_START_X + $09
	ldy #BLOCK_START_Y
	jsr graphics::drawing::goto_xy
	lda block_end_channel
	jsr graphics::printing::print_hex

	lda #BLOCK_START_X + $0C
	ldy #BLOCK_START_Y
	jsr graphics::drawing::goto_xy
	lda block_end_row
	jsr graphics::printing::print_hex
	
	rts
.endproc