; Paste the copied data starting at the current channel/row
.proc paste

	push_state_disable_interrupts

	@start: 
	; Start at block_begin_channel/row address and copy up to block_bytes_per_row
	; We need to get the begin row # and offset it with the row size

	; First we get the address of the source start channel/row
	ldx block_begin_channel
	ldy block_begin_row
	jsr calc_start_address
	cpm16 calc_address, block_begin_address

	; Now we need to get the destination address, which is 
	; where the cursor is. 
	ldx tracker::modules::edit_pattern::channel_number
	ldy row_number
	jsr calc_start_address
	cpm16 calc_address, block_destination_address

	;; Finally LETS GOOOOOO!!!
	; Loop the copy/paste over the number of rows
	; Note that if the user pastes a bigger chunk it can wrap weirdly
	; (there are no checks for that at the moment, call it a "feature")
	@copy_paste_start:
		cpm16 block_begin_address, block_current_row_address
		ldx #$00
	@copy_paste_loop:
	; We have the address to start at so now we copy up to 
	; bytes per row into buffer space in VRAM and then 
	; over to the destination pattern (bank)
		phx
		jsr @block_copy
		jsr @block_paste

	; Check if we need to loop
	@check_row_loop:
		plx
		inx
		cpx block_num_rows
		beq @end
	; If we're looping, update the block_current_row_address
	; to the new row
	@update_current_row_address:
		add16to8 block_current_row_address, #TOTAL_BYTES_PER_ROW, calc_address
		cpm16 calc_address, block_current_row_address
		; We're reusing block_destination_address here because it will
		; get updated anyway soon as the cursor moves
		add16to8 block_destination_address, #TOTAL_BYTES_PER_ROW, calc_address
		cpm16 calc_address, block_destination_address
		bra @copy_paste_loop
		
	@end:
		; Redraw pattern to screen
		jsr print_pattern
		; Pop processor state
		plp
		jmp cleanup

	@block_copy:
		; Prime VERA
		lda #%00010000  ; Stride 1, low VRAM
		sta VERA_addr_high
		lda #VRAM_COPY_BUFFER_ADDRESS_HIGH
		sta VERA_addr_med
		stz VERA_addr_low

		; Switch to pattern block is in
		lda block_source_pattern
		sta RAM_BANK

		; Do the copy
		cpm16 block_current_row_address, r0
		lda #<VERA_data0
		sta r1
		lda #>VERA_data0
		sta r1 + 1
		lda block_bytes_per_row
		sta r2
		stz r2 + 1 ; 8-bit number
		jsr memory_copy
		rts

	@block_paste:    
		; Prime VERA
		lda #%00010000  ; Stride 1, low VRAM
		sta VERA_addr_high
		lda #VRAM_COPY_BUFFER_ADDRESS_HIGH
		sta VERA_addr_med
		stz VERA_addr_low

		; Go to current pattern for pasting
		lda zp_PATTERN_NUMBER
		sta RAM_BANK

		; Do the paste from VRAM to current pattern
		lda #<VERA_data0
		sta r0
		lda #>VERA_data0
		sta r0 + 1
		cpm16 block_destination_address, r1
		lda block_bytes_per_row
		sta r2
		stz r2 + 1 ; 8-bit number, so zero
		jsr memory_copy
		rts

.endproc
