; Increment notes in block up one semitone
.proc inc_notes
		lda #$01
		sta note_offset
		jsr offset_notes
		jmp cleanup
.endproc

.proc dec_notes
		lda #$FF
		sta note_offset
		jsr offset_notes
		jmp cleanup
.endproc


; Offset notes in block by offset provided
; note_offset (zp_ARG1) = 2's compliment offset
.proc offset_notes	
	push_state_disable_interrupts

	; First we get the address of the source start channel/row
	ldx block_begin_channel
	ldy block_begin_row
	jsr calc_start_address
	cpm16 calc_address, block_current_row_address

	; Switch to pattern we're looking at
	rambank zp_PATTERN_NUMBER
	
	; Loop over the number of rows
	@inc_notes_start:
		ldx #$00	; row
	@rows_loop:
		; Update pointer for channel loop
		lda block_current_row_address
		sta BLOCK_ADDRESS_POINTER
		lda block_current_row_address + 1
		sta BLOCK_ADDRESS_POINTER + 1
		stz BLOCK_CHANNEL_OFFSET
		
		ldy #$00	; channel
	@channels_loop:
		phy	; Loop counter

		ldy BLOCK_CHANNEL_OFFSET
		; Grab current note value, increment, store
		lda (BLOCK_ADDRESS_POINTER),y	; note

		; If note is NOTENULL, NOTEOFF or NOTEREL, do nothing
		cmp #NOTENULL
		beq @channel_loop_bottom
		cmp #NOTEOFF
		beq @channel_loop_bottom
		cmp #NOTEREL
		beq @channel_loop_bottom
	@channel_add_offset:
		adda note_offset
		sta (BLOCK_ADDRESS_POINTER),y

	@channel_loop_bottom:
		iny ; note
		iny	; inst
		iny	; vol
		iny ; eff param
		iny ; eff value
		sty BLOCK_CHANNEL_OFFSET

		ply ; Loop counter
		iny
		cpy block_num_channels
		bne @channels_loop

	@rows_loop_bottom:
		inx
		cpx block_num_rows
		beq @end
	; If we're looping, update the block_current_row_address
	; to the new row
	@goto_next_row:
		add16to8 block_current_row_address, #TOTAL_BYTES_PER_ROW, calc_address
		cpm16 calc_address, block_current_row_address
		bra @rows_loop

	@end:
		; Redraw pattern to screen
		jsr print_pattern
		plp
		rts
.endproc

