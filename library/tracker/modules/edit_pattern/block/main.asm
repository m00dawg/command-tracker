; Block Copy/Paste/Edit Functions
; For now skipping all the fancy highlight work in VRAM just to get
; the thing working

.scope block

; Includes
.include "library/tracker/modules/edit_pattern/block/begin.asm"
.include "library/tracker/modules/edit_pattern/block/end.asm"
.include "library/tracker/modules/edit_pattern/block/paste.asm"
.include "library/tracker/modules/edit_pattern/block/inc_dec_notes.asm"
.include "library/tracker/modules/edit_pattern/block/assign_instrument.asm"
.include "library/tracker/modules/edit_pattern/block/print_block.asm"


; Find where to start copy operations from
; Used for finding block begin address and paste begin address
; x = channel_number
; y = row_number
; calc_address (zp_ARG0) = destination (address)
.proc calc_start_address
  @begin:
  @calc_chan_address:
    cpm16 zp_PAGE_POINTER, calc_address
  @chan_address_loop:
    cpx #$00
    beq @calc_row_address
    add16to8 calc_address, #TOTAL_BYTES_PER_CHANNEL, zp_MATH0
    cpm16 zp_MATH0, calc_address
    dex
    jmp @chan_address_loop
  ; Now that we have the starting address for the channel, we need
  ; to then find the start address for the row to start on which means
  ; we need to add the row length to the address up to the row number
  @calc_row_address:
  @calc_row_address_loop:
    cpy #$00    ; compare first since if we only copying 1 chan, we don't loop
    beq @end
    add16to8 calc_address, #TOTAL_BYTES_PER_ROW, zp_MATH0
    cpm16 zp_MATH0, calc_address
    dey
    jmp @calc_row_address_loop
  @end:
    rts
.endproc

.endscope