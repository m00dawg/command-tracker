; Draws the lower UI element for the pattern view

.proc draw_pattern_frame

  ; Locations
  START_X = $01
  STOP_X = DISPLAY_END_X - 1
  CHANNEL_TOP_Y = $07
  CHANNEL_HEADER = $08
  CHANNEL_BOTTOM_Y = $09
  FIRST_CHANNEL_X = $03
  ; Distance between the lines for the channels
  CHANNEL_WIDTH = $0C
  PATTERN_LINES_START_Y = $08
  
  ; Colors
  PSG_CHANNEL_COLOR = $B3
  FM_CHANNEL_COLOR  = $BD
  PCM_CHANNEL_COLOR = $B4
  MIDI_CHANNEL_COLOR = $B7
  WAVETABLE_CHANNEL_COLOR = $B6

@start:
  lda #FIRST_CHANNEL_X + 1
  sta DISPLAY_CHANNEL_START

  add tracker::modules::edit_pattern::start_channel, #NUM_CHANNEL_COLUMNS
  sta STOP_CHANNEL

  ldx tracker::modules::edit_pattern::start_channel
@channel_labels_loop:
  cpx #sound::fm::START_CHANNEL
  bge @fm_channel_label

; PSG channel group which includes WT or MIDI (switchable)
@psg_channel_label:
  stz VERA_addr_high
  lda sound::vera::channel_mode,x
  ; PSG
  beq @psg
  cmp #sound::vera::WAVETABLE
  beq @wavetable
  ; MIDI
@midi:
  print_string_macro_position_color midi_channel_header, DISPLAY_CHANNEL_START, #CHANNEL_HEADER, #MIDI_CHANNEL_COLOR
  lda #MIDI_CHANNEL_COLOR
  bra @print_channel_label
  ; PSG
@psg:
  print_string_macro_position_color psg_channel_header, DISPLAY_CHANNEL_START, #CHANNEL_HEADER, #PSG_CHANNEL_COLOR
  lda #PSG_CHANNEL_COLOR
  bra @print_channel_label
  ; WT
@wavetable:
  print_string_macro_position_color wt_channel_header, DISPLAY_CHANNEL_START, #CHANNEL_HEADER, #WAVETABLE_CHANNEL_COLOR
  lda #WAVETABLE_CHANNEL_COLOR
  bra @print_channel_label

@fm_channel_label:
  cpx #sound::pcm::START_CHANNEL
  bge @pcm_channel_label
  jsr print_fm_label
  lda #FM_CHANNEL_COLOR
  bra @print_channel_label
@pcm_channel_label:
  stz VERA_addr_high
  print_string_macro_position_color pcm_channel_header, DISPLAY_CHANNEL_START, #CHANNEL_HEADER, #PCM_CHANNEL_COLOR
  lda #PCM_CHANNEL_COLOR
@print_channel_label:
  sta zp_TEXT_COLOR
  phx
  txa
  jsr graphics::printing::print_hex

  add DISPLAY_CHANNEL_START, #CHANNEL_WIDTH
  sta DISPLAY_CHANNEL_START

  plx
  inx
  cpx STOP_CHANNEL
  beq @end
@channel_labels_loop_jump:
  jmp @channel_labels_loop
@end:
  rts

print_fm_label:
  stz VERA_addr_high
  print_string_macro_position_color fm_channel_header, DISPLAY_CHANNEL_START, #CHANNEL_HEADER, #FM_CHANNEL_COLOR
  rts

.endproc
