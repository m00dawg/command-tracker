; Write the current row we are on to the pattern
; This pulls the values from VRAM by figuring out
; which CHANNEL_NUMBER offset we are on, finding the beginning
; of that CHANNEL_NUMBER in VRAM, pulling the values, converting
; them and finally storing them back into the pattern data.
.proc save_row_channel

@save_row_channel:
  ; Do not interrupt saving
  push_state_disable_interrupts
  
  ; First, figure out where we are in the pattern
  lda row_number
  sta zp_ROW_NUMBER
  jsr tracker::get_row
  lda zp_ARG0
  sta zp_ROW_POINTER
  lda zp_ARG0 + 1
  sta zp_ROW_POINTER + 1

  ; If we already on channel 0 don't do any math
  lda #$00
  ldx tracker::modules::edit_pattern::channel_number
  beq @store_channel
@channel_skip_loop:
  clc
  adc #TOTAL_BYTES_PER_CHANNEL
  dex
  bne @channel_skip_loop
@store_channel:
  ; This is our channel offset
  ; We'll pop it into Y down below
  ; but push this on the stack for now since we need 'a'
  ;pha
  sta CHANNEL_OFFSET

  ; Then find where we are on the screen
  lda tracker::modules::edit_pattern::screen_channel
  jsr math::multiply_by_12
  clc
  adc #$03  ; For skipping past row numbers

  ; Load the ROW not the screen!
  ; Previous this was a really hard to catch bug. All rows fit in VRAM so
  ; the screen position doesn't matter for storing the actual values.
  ; Previously, SCREEN_ROW was being used here and that mean it kept just
  ; reading the same screen-row and storing it in the actual row. It was
  ; very confusing as it only came up when scrolling.
  ldy row_number

  jsr graphics::drawing::goto_xy

  ; Skip over colors
  lda #$21
  sta VERA_addr_high

  ; Read three to characters of note
  ; We do this first since we want to keep aligned with the rest of the
  ; pattern data (though if we find nulls, we'll skip some things)
@note_store_start:
  ldx VERA_data0  ; Note Name (A-G)
  ldy VERA_data0  ; Sharp or not
  
  ; Convert the octave to raw values
  ; (octave 1 = 12, 2 = 24, etc.).
  ; We will add this to our actual note later
  lda VERA_data0  ; Octave
  sbc #$2F
  jsr math::multiply_by_12
  clc
  sta OCTAVE_TEMP

  ; Check the note for special values
  cpx #SCREENCODE_PERIOD
  beq @note_null
  cpx #SCREENCODE_DASH
  beq @note_off
  cpx #SCREENCODE_ARROW_UP
  beq @note_rel

  ; Convert note name to numeric value
  lda screencode_to_note,x
  ; If there is a #, add one as it is a sharp
  cpy #SCREENCODE_HASH
  bne @not_sharp
  ; sharp, so add 1 to value
  clc
  adc #$01

@not_sharp:
  ; Now we need to find the numerical note value by adding
  ; the octave to the note.
  clc
  adc OCTAVE_TEMP
  sta zp_NOTE_NUMERIC
  jmp @store_note

@note_null:
  lda #NOTENULL
  jmp @store_note
@note_off:
  lda #NOTEOFF
  jmp @store_note
@note_rel:
  lda #NOTEREL

@store_note:
  ; This was our channel offset from above
  ldy CHANNEL_OFFSET
  ;ply

  ; Store note
  sta (zp_ROW_POINTER),y

  ; The next values are all 2-digit hex values
  ; so we can just do the same thing over and over

  ; Loop for instrument, volume, effect, and effect value
  ldx #$04
@store_rest_of_row_loop:
  ; Grab next two values in pattern VRAM
  lda VERA_data0
  sta r0
  lda VERA_data0
  sta r1

  ; Compare r1, if period, jump to storing null
  cmp #SCREENCODE_PERIOD
  beq @null_found

  ; Compare r0, if period, jump to storing null
  lda r0
  cmp #SCREENCODE_PERIOD
  beq @null_found

  ; If no nulls were found, convert value
  jsr graphics::printing::chars_to_number
  jmp @inc_row_pointer

@null_found:
  lda #EFFNULL

@inc_row_pointer:
  ; Inc our row pointer
  iny
  sta (zp_ROW_POINTER),y

  ; If we're on the instrument column (which is first in the loop)
  ; update the user's currently selected instrument
  cpx #$04
  bne @not_instrument
  ; Only store instrument if it's a number in the pattern
  cmp #INSTNULL
  beq @not_instrument
  sta zp_INSTRUMENT
@not_instrument:
  dex
  bne @store_rest_of_row_loop

@done_save:
  lda #$01
  sta VERA_addr_high
  jsr print_current_instrument
  plp
  rts

.endproc