; Edit Pattern Main
.scope edit_pattern


.segment "GOLDEN"

; Golden Variables
; These need to be preserved across module changes

; We store our own row number here because we want to 
; keep track of where we were when we switch to another screen
; This value gets copied to zp_ROW_NUMBER when relevant.
row_number:       .res $01
screen_row:       .res $01

; Copy/Paste
block_source_pattern: .res $01
block_begin_channel: .res $01
block_begin_row: .res $01
block_end_channel: .res $01
block_end_row: .res $01
block_num_channels: .res $01
block_num_rows: .res $01
block_bytes_per_row: .res $01
block_begin_address: .res $02
block_destination_address: .res $02
block_current_row_address: .res $02
block_destination_row_address: .res $02

; Actual channel number
channel_number:   .res $01
; Since we can only display 6 channels at a time, this is the relative 
; channel position on screen (e.g. 0-5)
screen_channel:   .res $01
; The actual channel we start at when showing channels on the screen
start_channel:    .res $01
; Note, Inst, Vol, Eff, Effect Value
column_pos:       .res $01


.segment "EP_MODULE"
.word init
.word keyboard_loop

; Constants
CHANNEL_WIDTH = $0C ; Number of characters that make up a channel
NOTE          = $00
INSTRUMENT    = $01
VOLULME       = $02
EFFECT        = $03
EFFECT_VALUE  = $04
SCREEN_ROW_MAX = $30
NUM_CHANNEL_COLUMNS = $06 ; Number of channels to see at once

CURSOR_X_START = $03  ; We need to skip past the row numbers and things
; What cursor position to start when moving past last screen channel
; This puts the cursor at the beginning of screen channel 6
CURSOR_X_START_OF_LAST_CHNNAEL = $3F
; If col pos is 0, note
; If 3 inst
; If 5 vol
; 7 eff
; 9 eff val
; 10 end of that
NOTE_COLUMN_POSITIION = $00
INSTRUMENT_COLUMN_POSITIION = $03
VOLUMNE_COLUMN_POSITIION = $05
EFFECT_COLUMN_POSITIION = $07
EFFECT_VALUE_COLUMN_POSITIION = $09
LAST_COLUMN_POSITION = $0A

; How many rows to move up or down on page down in pattern view
PATTERN_PAGE_UP_STEP = $10
PATTERN_PAGE_DOWN_STEP = $10
; How many times to cursor over on tab (noting it could be in the middle of a channel)
PATTERN_TAB_RIGHT_STEP = $09
PATTERN_TAB_LEFT_STEP = $09

INSTRUMENT_NUMBER_DISPLAY_X = $06
INSTRUMENT_NUMBER_DISPLAY_Y = $04
INSTRUMENT_NAME_DISPLAY_X = $06
INSTRUMENT_NAME_DISPLAY_Y = $05
BLOCK_START_X = $41
BLOCK_START_Y = $06

; Colors!
;NOTE_COLOR = $0F
;NOTEREL_COLOR = $0C
;NOTEOFF_COLOR = $0C
;NOTENULL_COLOR = $0C



; Temp Vars
;COLOR           = r0
COLOR = zp_TEXT_COLOR
OCTAVE_TEMP           = zp_TMP0
DISPLAY_CHANNEL_START = zp_TMP1
STOP_CHANNEL          = zp_TMP2
CHANNEL_OFFSET        = zp_TMP4
BLOCK_CHANNEL_OFFSET	= zp_TMP5
BLOCK_ADDRESS_POINTER = zp_ADDR0
BLOCK_ADDRESS_TEMP		= zp_ADDR0

; Address returned by block::calc_row_offset
calc_address          = zp_ARG0

; Offset (+ or -) to add to block note paste
note_offset						= zp_ARG1

; Local Variables



; Strings
psg_channel_header: .byte "psg ",0
wt_channel_header: .byte "wt  ",0
midi_channel_header: .byte "mid ",0
fm_channel_header:  .byte " fm ",0
pcm_channel_header: .byte "pcm ",0

;PATTERN_FILENAME:
	;.byte "scr/pattern.scr"
;PATTERN_FILENAME_LENGTH = $0F

; Sub-Scopes
.include "library/tracker/modules/edit_pattern/block/main.asm"
.include "library/tracker/modules/edit_pattern/cursor.asm"

; Includes

.include "library/tracker/modules/edit_pattern/save_row_channel.asm"
.include "library/tracker/modules/edit_pattern/print_pattern.asm"
.include "library/tracker/modules/edit_pattern/draw_pattern_frame.asm"
.include "library/tracker/modules/edit_pattern/find_cursor_position.asm"
.include "library/tracker/modules/edit_pattern/reset_scroll_position.asm"
.include "library/tracker/modules/edit_pattern/delete_channel_row.asm"
.include "library/tracker/modules/edit_pattern/key_to_note.asm"
.include "library/tracker/modules/edit_pattern/print_note_or_alphanumeric.asm"
.include "library/tracker/modules/edit_pattern/print_current_instrument.asm"
.include "library/tracker/modules/edit_pattern/skip_to_channels.asm"
.include "library/tracker/modules/edit_pattern/draw_ui.asm"

; Initial run once stuff
.proc init
init:
	jsr tracker::stop_song
	sei
	lda zp_PATTERN_NUMBER
	sta RAM_BANK
	cli
	;stz start_channel

	jsr draw_ui

	stz column_pos
	jsr find_cursor_position

	lda #$FF
	sta VERA_L0_hscroll_h
	lda #$F8
	sta VERA_L0_hscroll_l
	jsr reset_scroll_position
	stz zp_KEY_PRESSED
	; Fall to keyboard_loop
.endproc

.proc keyboard_loop
keyboard_loop:
	lda zp_KEY_PRESSED
	bne @kbd_continue
	jmp main_application_loop
@kbd_continue:
	cmp #F6
	beq @play_pattern_jump
	cmp #F7
	beq @play_song_at_row_jump
	cmp #KEY_UP
	beq @cursor_up_jump
	cmp #KEY_DOWN
	beq @cursor_down_jump
	cmp #KEY_LEFT
	beq @cursor_left_jump
	cmp #KEY_RIGHT
	beq @cursor_right_jump
	cmp #KEY_PGUP
	beq @page_up_jump
	cmp #KEY_PGDN
	beq @page_down_jump
	cmp #KEY_HOME
	beq @home_jump
	cmp #KEY_END
	beq @end_jump
	cmp #KEY_TAB
	beq @cursor_tab_right_jump
	cmp #SHIFT_TAB
	beq @cursor_tab_left_jump
	cmp #PETSCII_SHIFT_BACKSPACE
	beq @delete_channel_row_jump
	cmp #DEL
	beq @delete_channel_row_jump

	; Sound engine jumps
	; Jump to PSG
	cmp #COMMAND_1
	beq @skip_to_psg_jump
	; Jump to YM2151
	cmp #COMMAND_2
	beq @skip_to_fm_jump
	; Jump to PCM
	cmp #COMMAND_3
	beq @skip_to_pcm_jump

	; Mark start of block for copy
	cmp #COMMAND_B
	beq @begin_block_copy_jump
	cmp #COMMAND_E
	beq @end_block_copy_jump
	cmp #COMMAND_V
	beq @paste_block_copy_jump
	cmp #COMMAND_I
	beq @assign_instrument_block_jump
	cmp #COMMAND_RIGHT_BRACKET
	beq @inc_notes_block_jump
	cmp #COMMAND_LEFT_BRACKET
	beq @dec_notes_block_jump

	cmp #PETSCII_BRACKET_LEFT
	beq @decrease_octave
	cmp #PETSCII_BRACKET_RIGHT
	beq @increase_octave

	; Change PSG channels to MIDI and WT
	cmp #COMMAND_M
	beq @change_psg_channel_jump

	cmp #KEY_MINUS
	beq @goto_previous_order_jump
	cmp #KEY_EQUALS		; Basically plus but no shift
	beq @goto_next_order_jump


	; Rest of the stuff
	cmp #$2E    ; Alphanumeric, period, and a few things we don't want
	bpl @print_note_or_alphanumeric_jump
	jsr ui::update_upper_info
	jmp cleanup


@play_pattern_jump:
	jmp @play_pattern
@play_song_at_row_jump:
	jmp @play_song_at_row
@cursor_up_jump:
	jsr tracker::modules::edit_pattern::cursor::cursor_up
	jmp cleanup
@cursor_down_jump:
	jsr tracker::modules::edit_pattern::cursor::cursor_down
	jmp cleanup
@cursor_left_jump:
	jsr tracker::modules::edit_pattern::cursor::cursor_left
	jmp cleanup
@cursor_right_jump:
	jsr tracker::modules::edit_pattern::cursor::cursor_right
	jmp cleanup
@page_up_jump:
	jmp @page_up
@page_down_jump:
	jmp @page_down
@home_jump:
	jmp @home
@end_jump:
	jmp @end

@cursor_tab_right_jump:
	jmp @cursor_tab_right
@cursor_tab_left_jump:
	jmp @cursor_tab_left
@delete_channel_row_jump:
	jmp delete_channel_row
@print_note_or_alphanumeric_jump:
	jmp print_note_or_alphanumeric
@skip_to_psg_jump:
	jmp tracker::modules::edit_pattern::skip_to_psg_channel
@skip_to_fm_jump:
	jmp tracker::modules::edit_pattern::skip_to_fm_channel
@skip_to_pcm_jump:
	jmp tracker::modules::edit_pattern::skip_to_pcm_channel
@begin_block_copy_jump:
	jmp tracker::modules::edit_pattern::block::begin
@end_block_copy_jump:
	jmp tracker::modules::edit_pattern::block::end
@paste_block_copy_jump:
	jmp tracker::modules::edit_pattern::block::paste
@assign_instrument_block_jump:
	jmp tracker::modules::edit_pattern::block::assign_instrument
@inc_notes_block_jump:
	jmp tracker::modules::edit_pattern::block::inc_notes
@dec_notes_block_jump:
	jmp tracker::modules::edit_pattern::block::dec_notes

; Toggle PSG channel to MIDI/WT
@change_psg_channel_jump:
	jmp change_psg_channel

; Jump to next/prev order
@goto_previous_order_jump:
  jmp goto_previous_order
@goto_next_order_jump:
	jmp goto_next_order

; Inc/Dec octave and update UI
@decrease_octave:
	lda user_octave
	beq @update_octave_end
	dec user_octave
	jmp @update_ui_octave
@increase_octave:
	lda user_octave
	cmp #MAX_OCTAVE
	beq @update_octave_end
	inc user_octave
	jmp @update_ui_octave
@update_ui_octave:
	jsr ui::print_current_octave
@update_octave_end:
	jmp cleanup

; Play pattern only (loop pattern over and over)
@play_pattern:
	jsr tracker::stop_song
	  ; Reset scroll to beginning
	lda #PATTERN_SCROLL_START_H
	sta VERA_L0_vscroll_h
	lda #PATTERN_SCROLL_START_L
	sta VERA_L0_vscroll_l
	lda #PLAY_PATTERN_STATE
	sta zp_STATE
	; Set row to 0 to make sure we start scrolling at the start of the pattern
	stz zp_ROW_NUMBER
	jsr tracker::interrupts::play_irq
	jsr print_current_instrument
	;jmp @play_pattern_loop
	;jmp cleanup
; Fall into the play pattern loop, where we disable all keys
; except F8 (stop) on purpose to avoid shenaigans.
@play_pattern_loop:
  ; call ps2data_fetch
  lda #$08
  jsr extapi
  jsr SCNKEY
  wai
; Check for input from the keyboard, and do something if
; a key was pressed (return value is something other than 0)
@play_pattern_check_keyboard:
  jsr GETIN  ;keyboard
  beq @play_pattern_loop
  sta zp_KEY_PRESSED
; Check for stop
	@f8_key:
  cmp #F8
  bne @play_pattern_loop
  jsr tracker::stop_song
  jmp main_application_loop
	bra @play_pattern_loop


@play_song_at_row:
	jsr tracker::stop_song
	lda row_number
	sta zp_ROW_NUMBER
	ldx #PLAY_SONG_MODULE
	stx zp_CURRENT_MODULE
	jmp init_jump

@page_down:
	ldx #PATTERN_PAGE_DOWN_STEP
@page_down_loop:
	jsr tracker::modules::edit_pattern::cursor::cursor_down
	dex
	cpx #$00
	bne @page_down_loop
	jmp cleanup

@page_up:
	ldx #PATTERN_PAGE_UP_STEP
@page_up_loop:
	jsr tracker::modules::edit_pattern::cursor::cursor_up
	dex
	cpx #$00
	bne @page_up_loop
	jmp cleanup

@home:
  jsr graphics::drawing::cursor_unplot
	jsr reset_scroll_position
	jmp cleanup

@end:
	jsr graphics::drawing::cursor_unplot
	jsr cursor::goto_bottom
	jmp cleanup


@cursor_tab_left:
	; If screen channel is 0, and column pos is 0, just move left once
	lda screen_channel
	bne @cursor_tab_left_loop_start
	lda column_pos
	bne @cursor_tab_left_loop_start
	jsr tracker::modules::edit_pattern::cursor::cursor_left
	bra cleanup
@cursor_tab_left_loop_start:
	ldx #PATTERN_TAB_LEFT_STEP
@cursor_tab_left_loop:
	phx
	jsr tracker::modules::edit_pattern::cursor::cursor_left
	plx
	dex
	cpx #$00
	bne @cursor_tab_left_loop
	bra cleanup

@cursor_tab_right:
	ldx #PATTERN_TAB_RIGHT_STEP
@cursor_tab_right_loop:
	phx
	jsr tracker::modules::edit_pattern::cursor::cursor_right
	plx
	dex
	cpx #$00
	bne @cursor_tab_right_loop
	bra cleanup

.endproc

; Find the next non-zero order in the list
; and then jump to it.
.proc goto_next_order
	sei
	rambank zp_BANK_MISC
	ldy zp_ORDER_NUMBER
@check_for_zero:
	iny
	lda order_list,y
	beq @check_for_zero
	cli
	jmp load_pattern_from_order
.endproc

; Find the previous non-zero order in the list
; and then jump to it.
.proc goto_previous_order
	sei
	rambank zp_BANK_MISC
	ldy zp_ORDER_NUMBER
@check_for_zero:
	dey
	lda order_list,y
	beq @check_for_zero
	cli
	jmp load_pattern_from_order
.endproc

.proc load_pattern_from_order
  sty zp_ORDER_NUMBER
  lda order_list,y
  sta zp_PATTERN_NUMBER
  jmp init
.endproc

.proc change_psg_channel
	push_rambank
	rambank zp_BANK_MISC

	ldx channel_number
	cpx #sound::fm::START_CHANNEL
	blt @is_psg_channel
	bra @end
@is_psg_channel:
	lda sound::vera::channel_mode,x
	inc
	cmp #sound::vera::MIDI
	beq @midi
	bgt @psg
@wavetable:
	lda #sound::vera::WAVETABLE
	bra @save
@midi:
	lda #sound::vera::MIDI
	bra @save
@psg:
	lda #sound::vera::PSG

@save:
	sta sound::vera::channel_mode,x
	sta psg_channel_modes,x
  pop_rambank

@end:
	jsr draw_ui
	jmp cleanup
.endproc

.proc cleanup
	jsr print_current_instrument
	jmp main_application_loop
.endproc

.endscope
