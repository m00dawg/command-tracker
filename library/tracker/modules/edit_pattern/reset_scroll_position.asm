; Reset pattern scroll/position
.proc reset_scroll_position
@start:
  lda #STATIC_PATTERN_SCROLL_H
  sta VERA_L0_vscroll_h
  lda #STATIC_PATTERN_SCROLL_L
  sta VERA_L0_vscroll_l
  stz row_number
  stz screen_row
  lda #$01
  sta cursor_layer
  lda cursor_x
  lda #$00
  sta cursor_y
  jsr graphics::drawing::cursor_plot
  rts
.endproc