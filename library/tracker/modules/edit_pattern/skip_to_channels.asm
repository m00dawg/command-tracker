
; Skip to the first PSG channel
.proc skip_to_psg_channel
  ; Make sure we're on layer 1
  lda #$01
  sta VERA_addr_high
  lda #sound::vera::START_CHANNEL
  sta channel_number
  sta start_channel
  ; Start from left
  stz screen_channel
  stz column_pos
  stz row_number

  jsr draw_pattern_frame
  jsr print_pattern
  jsr find_cursor_position
  jsr reset_scroll_position
  jmp cleanup
.endproc

; Skip to the first YM2151 channel
.proc skip_to_fm_channel
  ; Make sure we're on layer 1
  lda #$01
  sta VERA_addr_high
  lda #sound::fm::START_CHANNEL
  sta channel_number
  sta start_channel
  ; Start from left
  stz screen_channel
  stz column_pos
  stz row_number

  jsr draw_pattern_frame
  jsr print_pattern
  jsr find_cursor_position
  jsr reset_scroll_position

  jmp cleanup
.endproc

; Skip to the first PCM channel
; This one is a bit odd. Since we only have one PCM channel and it's
; the last one, we need to move the display over so it's the last
; channel and the cursor is on that channel.
.proc skip_to_pcm_channel
  ; Make sure we're on layer 1
  lda #$01
  sta VERA_addr_high

  lda #sound::pcm::START_CHANNEL
  sta channel_number
  lda #(sound::pcm::START_CHANNEL - NUM_CHANNEL_COLUMNS + 1)
  sta start_channel
  lda #NUM_CHANNEL_COLUMNS - 1
  sta screen_channel
  stz column_pos
  stz row_number

  jsr draw_pattern_frame
  jsr print_pattern
  jsr find_cursor_position
  jsr reset_scroll_position

  jmp cleanup
.endproc