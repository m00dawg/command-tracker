.proc find_cursor_position
  ; Figure out where the cursor goes based on 
  ; what channel we were last on.
  ; CURSOR_X_START + (CHANNEL_WIDTH * screen_channel)
  lda #$00
  ldx #$00
@cursor_position_loop:
  cpx screen_channel
  beq @end
  adda #CHANNEL_WIDTH
  inx
  bra @cursor_position_loop
@end:
  adda #CURSOR_X_START
  sta cursor_x
  stz cursor_y
  rts
.endproc
