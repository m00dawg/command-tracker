.proc draw_ui
	CHANNEL_WIDTH = $0B
	NUM_COLUMNS = $07
	divider_x_offset = zp_MATH0

	turn_on_layer_0
	vera_layer0_16_color

	; Change $0B pallete color to a deep purple
	ldx #$16
	lda #$01
	sta palette,x
	inx
	lda #$01
	sta palette,x
	jsr graphics::vera::load_palette_16

  ; Set VERA Stride and bank
  lda #$10          ; Set primary address bank to 0, stride to 1
  sta VERA_addr_high 

	; Draw Frame
	stz zp_ARG0	;X1
	stz zp_ARG1	;Y1
	lda #$4F		;X2
	sta zp_ARG2
	lda #$3B		;Y2
	sta zp_ARG3
	lda #FRAME_COLOR
	sta zp_ARG4
	jsr graphics::drawing::draw_rounded_box

	; Clear inner frame
	lda #$01
	sta zp_ARG0
	sta zp_ARG1
	lda #$4E
	sta zp_ARG2
	lda #$3A
	sta zp_ARG3
	lda #$B1
	sta zp_ARG4
	jsr graphics::drawing::draw_solid_box
	
	jsr ui::print_header
  jsr ui::print_song_info
  jsr ui::print_speed
  jsr ui::print_current_order
  jsr ui::print_current_pattern_number
	
	print_string_macro display_elements

	; Clear out for bottom layer (pattern)
	lda #$01
	sta zp_ARG0
	lda #$0A
	sta zp_ARG1
	lda #$4A
	sta zp_ARG2
	lda #$3A
	sta zp_ARG3
	lda #$01
	sta zp_ARG4
	jsr graphics::drawing::draw_solid_box

@dividers:
	; Draw pattern dividers
	lda #FRAME_COLOR
	sta zp_TEXT_COLOR

	; loop 7 times, adding $0B each time
	lda #$03
	sta divider_x_offset
	ldx #$00
@divider_loop:
	phx
	ldx divider_x_offset
	ldy #$0A
	lda #$3B	; Length
	jsr graphics::drawing::draw_vertical_line
	lda divider_x_offset
	adc #CHANNEL_WIDTH
	sta divider_x_offset
	plx
	inx
	cpx #NUM_COLUMNS
	bne @divider_loop

	jsr ui::print_song_info
	jsr ui::update_upper_info
	jsr block::print_block
	jsr draw_pattern_frame
	jsr print_pattern
	

	rts

display_elements:
orders_header:
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte SCREENCODE_XY,$01,$04
	.byte "inst:"
	.byte SCREENCODE_XY,$01,$05
	.byte "name:"
	.byte SCREENCODE_XY,$3B,$06
	.byte "block:  :  ,  -  ,  "


	; Set screencode-mode
	.byte SCREENCODE_PRINT_MODE,$01
	.byte SCREENCODE_COLOR,FRAME_COLOR
	.byte SCREENCODE_XY,$00,$07
	; Top of channel border
	; Row Column
	.byte $6B,$43,$43,$72
	; Channel 0
	.byte $43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$72
	; Channel 1
	.byte $43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$72
	; Channel 2
	.byte $43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$72
	; Channel 3
	.byte $43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$72
	; Channel 4
	.byte $43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$72
	; Channel 5
	.byte $43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$72
	; Right Column
	.byte $43,$43,$43,$73
	; Middle of channel
	.byte SCREENCODE_RETURN
	; Row Column
	.byte $42,$23,$23,$42
	; Channel 0
	.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$42
	; Channel 1
	.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$42
	; Channel 2
	.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$42
	; Channel 3
	.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$42
	; Channel 4
	.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$42
	; Channel 5
	.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$42
	; Right Column
	.byte $20,$20,$20,$42
	; Bottom of channel border
	.byte SCREENCODE_RETURN
	; Row Column
	.byte $6B,$43,$43,$5B
	; Channel 0
	.byte $43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$5B
	; Channel 1
	.byte $43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$5B
	; Channel 2
	.byte $43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$5B
	; Channel 3
	.byte $43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$5B
	; Channel 4
	.byte $43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$5B
	; Channel 5
	.byte $43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$43,$5B
	; Right Column
	.byte $43,$43,$43,$73
	; Go to bottom of frame and draw connectors
	.byte SCREENCODE_XY,$03,$3B,$71
	.byte SCREENCODE_XY,$0F,$3B,$71
	.byte SCREENCODE_XY,$1B,$3B,$71
	.byte SCREENCODE_XY,$27,$3B,$71
	.byte SCREENCODE_XY,$27,$3B,$71
	.byte SCREENCODE_XY,$33,$3B,$71
	.byte SCREENCODE_XY,$3F,$3B,$71
	.byte SCREENCODE_XY,$4B,$3B,$71
	; Draw scroll arrows
	.byte SCREENCODE_XY,$4C,$20,$1F,$1F,$1F

	.byte 0

.endproc