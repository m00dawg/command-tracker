.proc delete_channel_row
; Delete the entire channel/row
@start:
  lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy

  ldx column_pos
  beq @print_periods
@goto_start_of_channel:
  jsr ui::cursor_left
  dex
  bne @goto_start_of_channel
@print_periods:
  stz column_pos
  ; Jump past colors
  lda #$21
  sta VERA_addr_high
  lda #SCREENCODE_PERIOD
  ldx #$0B  ; There's 11 dots in a pattern
@print_periods_loop:
  sta VERA_data0
  dex
  bne @print_periods_loop
  ; Disable stride
  lda #$01
  sta VERA_addr_high
  jsr save_row_channel
  jsr tracker::modules::edit_pattern::cursor::cursor_down
  jmp cleanup
.endproc