.scope cursor

.proc cursor_up
  lda mode
  ; Select Env Mode (0)
  bne @edit_env
@select_env:
  jsr decrement_envelope
  rts
@edit_env:
  ldy position
  lda (envelope_address),y
  inc
  ;cmp max_positive_env_value
  ;beq @end
  sta (envelope_address),y
  jsr draw_envelope
  jsr update_envelope_info
@end:
  rts
.endproc

.proc cursor_down
  lda mode
  ; Select Env Mode (0)
  bne @edit_env
@select_env:
  jsr increment_envelope
  rts
@edit_env:
  ldy position
  lda (envelope_address),y
  dec
  ;cmp #$FF
  ;beq @end
  sta (envelope_address),y
  jsr draw_envelope
  jsr update_envelope_info
@end:
  rts
.endproc

.proc cursor_left
  lda mode
  ; Select Env Mode (0)
  bne @edit_env
  rts
@edit_env:
  jsr remove_arrow
  lda position
  beq @end    
  dec position
  jsr update_envelope_info
  jsr add_arrow
@end:
  rts
.endproc

.proc cursor_right
  lda mode
  ; Select Env Mode (0)
  bne @edit_env
  rts
@edit_env:
  jsr remove_arrow
  lda position
  cmp #MAX_POSITION
  beq @end
  inc position
  jsr update_envelope_info
  jsr add_arrow
@end:
  rts
.endproc

; Increase envelope by #CURSOR_CHUNK
.proc cursor_up_by_chunk
  lda mode
  bne @edit_env
  rts
@edit_env:
  ldx #$00
  ldy position
  lda (envelope_address),y
@chunk_loop:
  inc
  inx
  ;cmp max_positive_env_value
  ;bge @max_value
  cpx #CURSOR_CHUNK
  bne @chunk_loop
  bra @end_loop
@max_value:
  lda max_positive_env_value
@end_loop:
  sta (envelope_address),y
  jsr clear_blocks
  jsr draw_envelope
  jsr update_envelope_info
  rts
.endproc

; Decrease envelope by #CURSOR_CHUNK
.proc cursor_down_by_chunk
  lda mode
  bne @edit_env
  rts
@edit_env:
  ldx #$00
  ldy position
  lda (envelope_address),y
@chunk_loop:
  dec
  inx
  ; This works because we would have rolled over if <0
  ;cmp max_positive_env_value
  ;bge @min_value
  cpx #CURSOR_CHUNK
  bne @chunk_loop
  bra @end_loop
@min_value:
  lda #$00
@end_loop:
  sta (envelope_address),y
  jsr clear_blocks
  jsr draw_envelope
  jsr update_envelope_info
  rts
.endproc


; Set current position to the max value
.proc cursor_max
  lda mode
  ; Select Env Mode (0)
  bne @edit_env
@select_env:
  ; "Max" in our env list is actually the top of the screen,
  ; so env 0
  ; Switch to upper layer
  lda #%00010000	; Inc set to 1, low ram
  sta VERA_addr_high
  stz envelope_number
  jsr highlight_envelope_number
  jsr load_envelope
  rts
@edit_env:
  ldy position
  lda max_positive_env_value
  sta (envelope_address),y
  jsr clear_blocks
  jsr draw_envelope
  jsr update_envelope_info
  rts
.endproc

; Set current position to the min value
.proc cursor_min
  lda mode
  ; Select Env Mode (0)
  bne @edit_env
@select_env:
  ; "Min" in our env list is actually the bottom of the screen,
  ; so max
  ; Switch to upper layer
  lda #%00010000	; Inc set to 1, low ram
  sta VERA_addr_high
  lda #MAX_ENVELOPES
  sta envelope_number
  jsr highlight_envelope_number
  jsr load_envelope
  rts
@edit_env:
  ldy position

  lda env_mode
  beq @short
@full:
  lda #$80
  jmp @end
@short:
  lda #$00
@end:
  sta (envelope_address),y
  jsr clear_blocks
  jsr draw_envelope
  jsr update_envelope_info
  rts
.endproc

; Remove arrow at current env position
.proc remove_arrow
  lda #%00010000	; Inc set to 1, low ram
  sta VERA_addr_high
  lda #ENV_MARKER_START_X
  adda position
  tax
  ldy env_marker_start_y
  lda #SCREENCODE_BLANK
  jsr graphics::printing::print_character
  rts
.endproc

; Add arrow at current env position
.proc add_arrow
  lda #%00010000	; Inc set to 1, low ram
  sta VERA_addr_high
  lda #ENV_MARKER_START_X
  adda position
  tax
  ldy env_marker_start_y
  lda #SCREENCODE_ARROW_UP
  jsr graphics::printing::print_character
  rts
.endproc


.endscope