.proc save_info
; Misc bank is where the title, composer, and order list live
  push_rambank
  rambank zp_BANK_MISC

; Update title, composer, speed, and filename
  ;sei
  ; Skip over colors
  lda #$20
  sta VERA_addr_high

@save_song_title:
  lda #SONG_TITLE_INPUT_X
  ldy #SONG_TITLE_INPUT_Y
  jsr graphics::drawing::goto_xy
  ldx #SONG_TITLE_MAX_LENGTH
  ldy #$00
@save_song_title_loop:
  lda VERA_data0
  sta song_title,y
  iny
  dex
  bne @save_song_title_loop

@save_composer:
  lda #COMPOSER_INPUT_X
  ldy #COMPOSER_INPUT_Y
  jsr graphics::drawing::goto_xy
  ldx #COMPOSER_MAX_LENGTH
  ldy #$00
@save_composer_loop:
  lda VERA_data0
  sta composer,y
  iny
  dex
  bne @save_composer_loop

@save_speed:
  lda #SPEED_INPUT_X
  ldy #SPEED_INPUT_Y
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  sta r0
  lda VERA_data0
  sta r1
  jsr graphics::printing::chars_to_number
  sta starting_speed
  sta zp_SPEED

@save_sync_method:
  lda #SYNC_METHOD_X
  ldy #SYNC_METHOD_Y
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  jsr graphics::printing::convert_to_number
  sta sync_method

@save_bpm:
  lda #BPM_INPUT_X
  ldy #BPM_INPUT_Y
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  sta r0
  lda VERA_data0
  sta r1
  jsr graphics::printing::chars_to_number
  sta starting_bpm

; We don't save the noise toggle here since that's 
; done via the space routine
@save_noise_freq:
  lda fm_global_ch7_noise_control
  and #%10000000
  sta math_temp

  lda #NOISE_FREQ_X
  ldy #NOISE_FREQ_Y
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  sta r0
  lda VERA_data0
  sta r1
  jsr graphics::printing::chars_to_number
  and #%00011111
  ora math_temp
  sta fm_global_ch7_noise_control
  
@save_lfo_freq:
  lda #LFO_FREQ_X
  ldy #LFO_FREQ_Y
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  sta r0
  lda VERA_data0
  sta r1
  jsr graphics::printing::chars_to_number
  sta fm_global_lfo_frequency

@save_lfo_am_depth:
  lda #LFO_AM_DEPTH_X
  ldy #LFO_AM_DEPTH_Y
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  sta r0
  lda VERA_data0
  sta r1
  jsr graphics::printing::chars_to_number
  sta fm_global_lfo_am_depth

@save_lfo_pm_depth:
  lda #LFO_PM_DEPTH_X
  ldy #LFO_PM_DEPTH_Y
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  sta r0
  lda VERA_data0
  sta r1
  jsr graphics::printing::chars_to_number
  sta fm_global_lfo_pm_depth

@save_lfo_waveform:
  lda #LFO_WAVEFORM_X
  ldy #LFO_WAVEFORM_Y
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  jsr graphics::printing::convert_to_number
  sta fm_global_lfo_waveform

@save_global_transpose:
  lda #GLOBAL_TRANSPOSE_X
  ldy #GLOBAL_TRANSPOSE_Y
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  sta r0
  lda VERA_data0
  sta r1
  jsr graphics::printing::chars_to_number
  sta global_transpose


@save_description:
  lda #<song_description
  sta description_address
  lda #>song_description
  sta description_address + 1

  ldx #DESCRIPTION_START_X
  ldy #DESCRIPTION_START_Y
  stz temp_line_pos
@line_loop:
  stz temp_char_pos
@character_loop:
  txa
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  phy
  ldy #$00
  sta (description_address),y
  ply
@character_loop_end:
  add16to8 description_address, #$01, temp_address
  lda temp_address
  sta description_address
  lda temp_address + 1
  sta description_address + 1
  inx
  inc temp_char_pos
  lda temp_char_pos
  cmp #DESCRIPTION_MAX_LINE_LENGTH
  bne @character_loop
@line_loop_end:
  iny
  ldx #DESCRIPTION_START_X
  inc temp_line_pos
  lda temp_line_pos
  cmp #DESCRIPTION_MAX_LINE
  bne @line_loop


@end:

  ;jsr ui::draw_frame
  jsr ui::print_song_info
  jsr ui::print_speed
  jsr update_info

  lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy
  jsr graphics::drawing::cursor_plot

  ;jsr update_position
  ;cli
  pop_rambank
  rts

.endproc