.segment "SONG_OPTS_MODULE"
.scope song_options

.word init
.word keyboard_loop


  ; Constants
  SONG_OPTIONS_FILENAME:
    .byte "scr/options.scr"
  SONG_OPTIONS_FILENAME_LENGTH = $0F

  NOISE_ENABLE_COLOR = $CD
  NOISE_DISABLE_COLOR = $CB
  DESCRIPTION_TEXT_COLOR = $01

  ; Variables
  edit_position = zp_TMP0
  descrip_line = zp_TMP1
  temp_line_pos = zp_TMP2
  temp_char_pos = zp_TMP3
  description_address = zp_MATH0
  temp_address = zp_MATH1
  math_temp    = zp_MATH2

  ; Calling Args
  ; For ui::instruments::print_flag
  ; which is used by the noise toggle
  enable_color            = zp_ARG6
  disable_color           = zp_ARG7
  flag                    = zp_ARG8

.include "library/tracker/modules/song_options/luts.inc"
.include "library/tracker/modules/song_options/update_info.asm"
.include "library/tracker/modules/song_options/save_info.asm"
.include "library/tracker/modules/song_options/cursor.asm"

.proc init
init:
	lda #SONG_OPTIONS_FILENAME_LENGTH
	ldx #<SONG_OPTIONS_FILENAME
	ldy #>SONG_OPTIONS_FILENAME
	jsr files::load_to_vram
  jsr ui::update_upper_info
  jsr ui::print_song_info
 
  turn_off_layer_0
  vera_layer0_16_color

  lda #$10
  sta VERA_addr_high ; Set primary address bank to 1, stride to 2
  jsr update_info

  stz zp_KEY_PRESSED
  stz edit_position
  stz descrip_line

@cursor_start_position:
  stz cursor_layer

  lda #CURSOR_START_X
  sta cursor_x
  lda #CURSOR_START_Y
  sta cursor_y
  jsr graphics::drawing::cursor_plot

  jsr update_position
.endproc

.proc keyboard_loop
@keyboard_loop:
  lda zp_KEY_PRESSED
  cmp #KEY_UP
  beq @cursor_up_jump
  cmp #KEY_PGUP
  beq @cursor_up_jump
  cmp #KEY_DOWN
  beq @cursor_down_jump
  cmp #KEY_PGDN
  beq @cursor_down_jump
  cmp #KEY_LEFT
  beq @cursor_left_jump
  cmp #KEY_RIGHT
  beq @cursor_right_jump
  cmp #KEY_RETURN
  beq @cursor_return_jump
  cmp #SPACE
  beq @space_jump
  cmp #PETSCII_BACKSPACE
  beq @backspace_jump
  ; Start at !
  cmp #KEY_BANG
  bpl @print_letters_jump
  jmp main_application_loop

@cursor_up_jump:
  jmp cursor::up
@cursor_down_jump:
  jmp cursor::down
@cursor_left_jump:
  jmp cursor::left
@cursor_right_jump:
  jmp cursor::right
@cursor_return_jump:
  jmp cursor::return
@space_jump:
  jmp space
@print_letters_jump:
  jmp print_letters
@backspace_jump:
  jmp backspace
.endproc

.proc print_letters
@print_letters:
  pha
  lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy

  lda edit_position
  cmp #DESCRIPTION
  bne @text_color
@description_color:
  ldx #DESCRIPTION_TEXT_COLOR
  bra @color
@text_color:
  ldx #EDITABLE_TEXT_COLORS
@color:
  stx zp_TEXT_COLOR
  pla
  jsr graphics::printing::print_alpha_char

  ; If we're on a text field, we can save immediately
  lda edit_position
  cmp #TITLE
  beq @text  
  cmp #COMPOSER
  beq @text  
  cmp #DESCRIPTION
  beq @text
  bra @continue
@text:
  jsr save_info

@continue:
  ; If we're at the max column, don't go more right
  ldx edit_position
  lda x_stop_pos_lut,x
  cmp cursor_x
  beq @at_max
  jsr ui::cursor_right
  jmp main_application_loop

; We're at max so it should be safe to save
@at_max:
  ;jsr ui::cursor_right
  jsr save_info
  jmp main_application_loop
.endproc

.proc backspace
  ; If we're at the beginning, don't move left
  lda edit_position
  cmp #DESCRIPTION
  beq @description
@not_description:
  lda cursor_x
  cmp #CURSOR_START_X
  bne @continue
  jmp main_application_loop 
@description:
  lda cursor_x
  cmp #DESCRIPTION_START_X
  bne @continue
  jmp main_application_loop 
@continue:
  jsr ui::cursor_left
  lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy
  lda #SCREENCODE_BLANK
  sta VERA_data0



  jsr save_info
  ;jsr update_info

@backspace_end:
  jmp main_application_loop
.endproc

.proc space
  ; If we're on Noise Enable, toggle.
  lda edit_position
  cmp #FM_CH7_NOISE_EN
  beq @toggle_noise

  ; If we are on title, composer, or descrip, act
  ; like a normal space
  cmp #TITLE
  beq @string
  cmp #COMPOSER
  beq @string
  cmp #DESCRIPTION
  beq @string
  jmp main_application_loop
  
@string:
  lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy
  lda #SCREENCODE_BLANK
  sta VERA_data0

  jsr ui::cursor_right
  jmp main_application_loop

@toggle_noise:
  push_rambank
  rambank zp_BANK_MISC
  lda fm_global_ch7_noise_control
  jsr ui::instruments::toggle_first_flag
  sta fm_global_ch7_noise_control
  jsr update_info
  pop_rambank
  jmp main_application_loop
.endproc

.proc update_position
  jsr graphics::drawing::cursor_unplot
  ldx edit_position

  lda y_pos_lut,x
  sta cursor_y
  tay
  lda x_pos_lut,x
  sta cursor_x
  jsr graphics::drawing::goto_xy
  jsr graphics::drawing::cursor_plot
  rts
.endproc

.endscope
