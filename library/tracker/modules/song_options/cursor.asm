.scope cursor

.proc up
  lda edit_position
  ;cmp #DESCRIPTION + 1
  cmp #DESCRIPTION
  blt @up 
@description_up:
  jmp description_up
@up:
  lda edit_position
  beq @up_end
  dec edit_position
  jsr update_position
@up_end:
  jmp main_application_loop
.endproc

.proc description_up
  lda descrip_line
  beq @at_top
  dec descrip_line
  jsr ui::cursor_up
  jmp main_application_loop
@at_top:
  dec edit_position
  jsr update_position
  jmp main_application_loop
.endproc

.proc down
  lda edit_position
  cmp #DESCRIPTION
  blt @down 
@description_down:
  jmp description_down
@down:
  inc edit_position
  jsr update_position
@down_end:
  jmp main_application_loop
.endproc

.proc description_down
  lda descrip_line
  cmp #DESCRIPTION_MAX_LINE
  beq @end
  inc descrip_line
  jsr ui::cursor_down
@end:
  jmp main_application_loop
.endproc

.proc left
  ldx edit_position
  lda x_pos_lut,x
  cmp cursor_x
  beq @cursor_left_end


  jsr ui::cursor_left
@cursor_left_end:
  jmp main_application_loop
.endproc

.proc right
  ldx edit_position
  lda x_stop_pos_lut,x
  cmp cursor_x
  beq @cursor_right_end

  jsr ui::cursor_right
@cursor_right_end:
  jmp main_application_loop
.endproc

.proc return
  lda edit_position
  cmp #DESCRIPTION
  beq @description_return
  jmp down
@description_return:
  jsr graphics::drawing::cursor_unplot
  ldx edit_position
  lda x_pos_lut,x
  sta cursor_x
  jmp description_down

.endproc


.endscope