.scope convert_3_to_4

.proc run
	print_string_macro channel_type_string


	push_rambank
	rambank zp_BANK_MISC
	ldx #$00
	lda #$00
	@channel_type_loop:
		sta psg_channel_modes,x
		inx
		cpx #sound::vera::NUM_CHANNELS
		bne @channel_type_loop
  pop_rambank

@end:
	rts

channel_type_string:
	.byte SCREENCODE_XY,LOADING_INFO_X,LOADING_INFO_Y + $03
  .byte SCREENCODE_COLOR,TITLE_COLORS
  .byte "setting psg channel modes to zero ",0

.endproc

.endscope