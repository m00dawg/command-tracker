; Save current loaded song to disk

; Current Rough process:
; 1. Call SETNAM, a = len, x/y hi/lo pointers to filename
; 2. Close the file, which indicates we want to overwrite it (apparently)
;    Note you still have to prefix the filename is @: for that to work
; 3. Call SETLFS, a = logical file #, x = dev #, y = secondary addr
;   a = file id (if opening multiple filess) - for now, 1
;   x = 8, host drive on emu
;   y = 0
; 4. OPEN and CHKOUT file
; 5. Write the file version and speed - these are easy single byte writes
; 6. Write the title, author, both hard aligned to 16 bytes
; 7. Write the complete order list (255 bytes)
;    Source of optimization here, we don't necessarily need to do that, but
;    if someone wanted to jump around the order list (allowable via a pattern
;    effect), this allows them to jump _anywhere_. But that's a minor benefit
;    and it's mostly just to make things simpler.
; 8. Sort the order list and put it somewhere. This is so we have all the
;    patterns in order
; 9. Start writing patterns, by looking at the sorted order list. We ignore
;    00 patterns and also keep track of the previously saved pattern so we
;    only write unique rows. Write the pattern number first, then write
;    the entire HiRAM page. We only need 8000 bytes but for now are writing
;    all 8192. It makes the looping easier
;    (we just loop over 256 bytes 20 times)
; 10. Write a $00 indicating there are no more patterns
; 11. Close file.

.proc save_song
  ; Vars
  sample_state = zp_TMP0

  ; Constants
  PATTERN_TERMINATOR = $00
  SAMPLE_TERMINATOR = $FF

  push_state_disable_interrupts

  jsr files::open_for_write

@write_bytes:
  ; Version of CMT save file format
  lda #SONG_FILE_VERSION
  jsr CHROUT

; Misc bank is where the speed, title, composer, and order list live
@change_to_misc_bank:
  rambank zp_BANK_MISC

; Zero out sorted list first
@clear_sorted_list:
  ldx #$00
@clear_sorted_list_loop:
  stz order_list_sorted,x
  inx
  bne @clear_sorted_list_loop
; Sort the order list first, which will be used to store only the patterns
; that have been defined in the order list
@sort_order_list:
  ; Copy order list to the sorted area
  ldx #$00
@copy_order_list_loop:
  lda order_list,x
  sta order_list_sorted,x
  inx
  bne @copy_order_list_loop
@copy_order_list_loop_done:
  lda #<order_list_sorted
  sta zp_ADDR0
  lda #>order_list_sorted
  sta zp_ADDR0+1
  lda #$FF
  sta zp_MATH1
  jsr math::sort8

@write_misc_bank:
  lda #<PAGE_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PAGE_ADDRESS
  sta zp_PAGE_POINTER + 1
  jsr files::write_page

; Write Envelopes
  rambank zp_BANK_ENVS
  lda #<PAGE_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PAGE_ADDRESS
  sta zp_PAGE_POINTER + 1
  jsr files::write_page
; Write VERA Instruments
  rambank zp_BANK_VINS
  lda #<PAGE_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PAGE_ADDRESS
  sta zp_PAGE_POINTER + 1
  jsr files::write_page
; Write FM Instruments
  rambank zp_BANK_FINS
  lda #<PAGE_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PAGE_ADDRESS
  sta zp_PAGE_POINTER + 1
  jsr files::write_page


@write_patterns:
  ; Set pattern number to 00 since we haven't saved any patterns yet.
  stz zp_PATTERN_NUMBER

; Pattern loop which reads the sorted order list and writes unique patterns
; to the file, writing the pattern number first and then the full hi-mem
; page data for that pattern (pattern number == page number)
; Set loop counter for sorter order list array
  ldy #$00
@get_pattern_loop:
  rambank zp_BANK_MISC
  lda order_list_sorted,y
  ; Push our order list index
  phy
  ; If we see a zero, we skip. This is becuse we are currently sorting
  ; the ENTIRE order list, so all the zero's get pushed up to the beginning.
  beq @get_pattern_end
  ; Now check to see if we have already saved the pattern (skip duplicates)
  cmp zp_PATTERN_NUMBER
  beq @get_pattern_end
  sta zp_PATTERN_NUMBER
  sta RAM_BANK
  ; Write the pattern number
  jsr CHROUT

@write_patterns_page_loop:
  ; Reset pattern pointer back to default
  lda #<PATTERN_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PATTERN_ADDRESS
  sta zp_PAGE_POINTER + 1
  jsr files::write_page

@get_pattern_end:
  ; Pull y, which is our order list index, off the stack, increment it so we
  ; can look at the next item in the sorted order list array
  ply
  iny
  ; Keep looping until we rollover (meaning we've been through all 256 orders)
  bne @get_pattern_loop

; Finally store a $00 to indicate there are no more patterns
; when we load the file back later. This avoids having to track the number
; of patterns in the file.
@write_patterns_end:
  lda #PATTERN_TERMINATOR
  jsr CHROUT

; For each sample, check to see if it is active (from misc page)
; and if so, write the sample number and page to disk
@write_samples:
  ldx #$00
@active_samples_loop:
  rambank zp_BANK_MISC
  lda sample_states,x
  sta sample_state
  bbs 7, sample_state, @active
  bra @active_samples_loop_bottom
@active:
  ; If active, store the current sample number
  txa
  jsr CHROUT
  ; Then add the sample number to our base page offset
  adda sound::pcm::bank_start
  sta RAM_BANK
  ; Then store the page
  lda #<PAGE_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PAGE_ADDRESS
  sta zp_PAGE_POINTER + 1
  phx
  jsr files::write_page
  plx
@active_samples_loop_bottom:
  inx
  cpx #sound::pcm::NUM_SAMPLES
  bne @active_samples_loop
@write_samples_end:
  lda #SAMPLE_TERMINATOR
  jsr CHROUT



@cleanup:
; Close the file and return
  jsr files::close_file

  ; Reset pattern pointer back to default
  lda #<PATTERN_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PATTERN_ADDRESS
  sta zp_PAGE_POINTER + 1

  ; Pop processor state (interrupts)
  plp

  rts
.endproc


