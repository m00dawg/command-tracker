.proc draw_ui
	vera_layer0_16_color

	; Change $0B pallete color to a just grey
	ldx #$16
	lda #$22
	sta palette,x
	inx
	lda #$02
	sta palette,x
	jsr graphics::vera::load_palette_16

  ; Set VERA Stride and bank
  lda #$10          ; Set primary address bank to 0, stride to 1
  sta VERA_addr_high 

	; Draw Frame
	stz zp_ARG0	;X1
	stz zp_ARG1	;Y1
	lda #$4F		;X2
	sta zp_ARG2
	lda #$3B		;Y2
	sta zp_ARG3
	lda #FRAME_COLOR
	sta zp_ARG4
	jsr graphics::drawing::draw_rounded_box

	; Clear inner frame
	lda #$01
	sta zp_ARG0
	sta zp_ARG1
	lda #$4E
	sta zp_ARG2
	lda #$3A
	sta zp_ARG3
	lda #$B1
	sta zp_ARG4
	jsr graphics::drawing::draw_solid_box
	
	jsr ui::print_header
  jsr ui::print_song_info
  jsr ui::print_speed
  jsr ui::print_current_order
  jsr ui::print_current_pattern_number
	
	print_string_macro display_elements


	rts

display_elements:
	.byte SCREENCODE_COLOR,HEADER_COLOR
	.byte SCREENCODE_XY,$21,$08
	.byte "file"
	.byte SCREENCODE_XY,$05,$0D
	.byte "filename:"
	.byte SCREENCODE_XY,$1F,$2C
	.byte "commands"
	.byte SCREENCODE_XY,$05,$2D
	.byte "alt-s: save"
	.byte SCREENCODE_RETURN
	.byte "alt-l: load"
	.byte SCREENCODE_RETURN
	.byte "alt-n: nuke (careful!)"
	.byte SCREENCODE_XY,$1E,$32
	.byte "zsm export"
	.byte SCREENCODE_XY,$05,$34
	.byte "filename: zsm.out"
	.byte SCREENCODE_RETURN
	.byte SCREENCODE_RETURN
	.byte "alt-z: play full song and write zsm"
	.byte SCREENCODE_RETURN
	.byte "must use stop song effect (c0) at end of song"
	.byte 0


.endproc