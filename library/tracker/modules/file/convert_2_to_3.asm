.scope convert_2_to_3

; Convert from song version to 2 to 3
; This is the effect remap, so we have to take all the original effects
; and remap them to the new ones.

; FM Register Effects at 40xx = same

; Approach:
; For each pattern (from orders_list_sorted perhaps, but cannot rescan duplicate patterns)
; read each row, and look at the effect column 
; For channels 0-15 (VERA), remap to VERA list
; For channels 16-24 (FM), remap to FM list
; For channel 25 (PCM) pretty much ignore as we had no effects yet in song version 2

; Constants

OLD_VERA_MAX_EFFECT = $12
OLD_FM_MAX_EFFECT = $10

; Uses:
CURRENT_CHANNEL = zp_TMP0
CHANNEL_OFFSET = zp_TMP1
ROW_ADDRESS = zp_ARG0


.proc run

print_string_macro converting_pattern_string

@read_patterns:
  ; Set pattern number to 00 since we haven't saved any patterns yet.
  stz zp_PATTERN_NUMBER

; Pattern loop which reads the sorted order list and to get unique patterns
; to scan for effect changes
; Modified from save_song.asm
  ldy #$00
@get_pattern_loop:
  rambank zp_BANK_MISC
  lda order_list_sorted,y
  ; Push our order list index
  phy
  ; If we see a zero, we skip. This is becuse we are currently sorting
  ; the ENTIRE order list, so all the zero's get pushed up to the beginning.
  beq @get_pattern_loop_upkeep
  ; Now check to see if we have already saved the pattern (skip duplicates)
  cmp zp_PATTERN_NUMBER
  beq @get_pattern_loop_upkeep
  ; We have a pattern to update, so let's do that
	sta zp_PATTERN_NUMBER
  jsr convert_pattern

@get_pattern_loop_upkeep:
  ; Pull y, which is our order list index, off the stack, increment it so we
  ; can look at the next item in the sorted order list array
  ply
  iny
  ; Keep looping until we rollover (meaning we've been through all 256 orders)
  bne @get_pattern_loop

@clear_macros_area:
	print_string_macro clear_macros_string

	rambank zp_BANK_MISC
	lda #<MACROS_START_ADDRESS
	sta r0
	lda #>MACROS_START_ADDRESS
	sta r0 + 1
	lda #<MACROS_SIZE
	sta r1
	lda #>MACROS_SIZE
	sta r1 + 1
	lda #$00
	jsr memory_fill

@end:
	rts


converting_pattern_string:
	.byte SCREENCODE_XY,LOADING_INFO_X,LOADING_INFO_Y + $03
  .byte SCREENCODE_COLOR,TITLE_COLORS
  .byte "converting pattern: ",0

clear_macros_string:
	.byte SCREENCODE_XY,LOADING_INFO_X,LOADING_INFO_Y + $04
  .byte SCREENCODE_COLOR,TITLE_COLORS
  .byte "clearing macros area",0

.endproc

; Update the effects within a pattern
.proc convert_pattern
  rambank zp_PATTERN_NUMBER

	; Display current pattern being converted
	lda #LOADING_INFO_X + $14
	ldy #LOADING_INFO_Y + $03
	jsr graphics::drawing::goto_xy
	lda zp_PATTERN_NUMBER
	jsr graphics::printing::print_hex


	; Loop through the pattern rows
	ldx #$00
@row_loop:

	txa
	phx
	; get_row is from tracker scope
	jsr get_row	; Returns position in ROW_ADDRESS (zp_ARG0)

	stz CURRENT_CHANNEL
	stz CHANNEL_OFFSET

@channel_loop:
	jsr read_row
  add CHANNEL_OFFSET, #TOTAL_BYTES_PER_CHANNEL
  sta CHANNEL_OFFSET
	inc CURRENT_CHANNEL
	lda CURRENT_CHANNEL
	cmp #NUMBER_OF_CHANNELS
	bne @channel_loop
@row_loop_bottom:
	plx
	inx
	cpx #ROW_MAX
	bne @row_loop
	rts

.endproc


; Read row and if we see an effect param to update, write it back to the row position
.proc read_row
  ; note
  ldy CHANNEL_OFFSET     
  lda (ROW_ADDRESS),y
  ;sta zp_ROW_NOTE

  ; instrument
  iny
  ;lda (zp_ROW_POINTER),y
  ;sta zp_ROW_INSTRUMENT
  ; volume
  iny
  ;lda (zp_ROW_POINTER),y
  ;sta zp_ROW_VOLUME

  ; effect param
  iny
  lda (ROW_ADDRESS),y

	; If effect is null, skip
	cmp #EFFNULL
	beq @end

  sta zp_ROW_EFFECT_PARAMETER
	; effect value
  iny
  lda (ROW_ADDRESS),y
  sta zp_ROW_EFFECT_VALUE

	; Go back one byte to the effect param
	; so we write to the correct byte in the pattern
	dey

	; If channel is FM, use FM LUT
	lda CURRENT_CHANNEL
	cmp #sound::fm::START_CHANNEL
	bge @check_fm_effects
	; Else if channel is VERA, use VERA LUT
	blt @check_vera_effects
	; Else it's PCM so return
	rts
@check_fm_effects:
	jsr check_fm_effects
	rts
@check_vera_effects:
	jsr check_vera_effects
@end:
  rts
.endproc

.proc check_fm_effects
	ldx zp_ROW_EFFECT_PARAMETER
	cpx #OLD_FM_MAX_EFFECT
	bgt @end

	lda fm_effects_lut,x
	; if effect is 04, we need to look at the value
	sta (ROW_ADDRESS),y
@end:
	rts

fm_effects_lut:
	; Effects 00-07
	.byte $00, $01, $02, $03, $04, $05, EFFNULL, EFFNULL
	; Effects 08-0F
	.byte $06, $5A, $5B, $5C, $00, $00, $5D, $5E
	; Effect 10
	.byte $12
.endproc

.proc check_vera_effects
	ldx zp_ROW_EFFECT_PARAMETER
	cpx #OLD_VERA_MAX_EFFECT
	bgt @end
	lda vera_effects_lut,x
	sta (ROW_ADDRESS),y

@check_effect_04:	
	; if effect is 04 (volue slide/sweep), we need to look at the value
	cmp #$04
	beq @fix_effect_04
@check_effect_05:
	; 45 is new slide (was 05)
	cmp #$45
	beq @fix_effect_05

@end:
	rts

; Check if the effect range was a sweep
@fix_effect_04:
	lda zp_ROW_EFFECT_VALUE
	cmp #$80
	bge @yes_vol_sweep
	bra @cleanup_fixins
@yes_vol_sweep:
	; If yes, store sweep effect value instead
	lda #$05
	sta (ROW_ADDRESS),y
	bra @cleanup_fixins
; Check if the effect range was a sweep
@fix_effect_05:
	lda zp_ROW_EFFECT_VALUE
	cmp #$80
	bge @yes_pwm_sweep
	bra @cleanup_fixins
@yes_pwm_sweep:
	; If yes, store sweep effect value instead
	lda #$46
	sta (ROW_ADDRESS),y
@cleanup_fixins:
	; Mask bit 7 since we now no longer use it for slide/sweep
	lda zp_ROW_EFFECT_VALUE
	and #%01111111
	iny
	sta (ROW_ADDRESS),y
	dey
	rts


; Note that effect 04 is special and we'll have to
; take care of it in the loop
vera_effects_lut:
	; Effects 00-07
	.byte $00, $01, $02, $03, $04, $45, $44, $0A
	; Effects 08-0F
	.byte $06, $08, $09, $47, $C0, $C2, $C1, $C3
	; Effects 10-12
	.byte $12, $C5, $4A
.endproc


; Update channel offset from pattern and increment
; channel count, noting the channel count is 
; reset for each type (PSG, FM, PCM)
; 
.proc update_channel_offset
  add CHANNEL_OFFSET, #TOTAL_BYTES_PER_CHANNEL
  sta CHANNEL_OFFSET
  inc zp_CHANNEL_COUNT
  lda zp_CHANNEL_COUNT
  rts
.endproc


.endscope