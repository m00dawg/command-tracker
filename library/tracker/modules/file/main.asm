.segment "FILE_MODULE"

.scope file

.word init
.word keyboard_loop


.include "library/tracker/modules/file/draw_ui.asm"
.include "library/tracker/modules/file/convert_2_to_3.asm"
.include "library/tracker/modules/file/convert_3_to_4.asm"
.include "library/tracker/modules/file/save_song.asm"

; For directory, you can use $ as the filename
; $=C would be name of current dir
; Open like you would any file to read via CHRIN
; but note you can't open any other files while doing
; a directory listing
; You CAN have a file already open however

; Constants
FILENAME_INPUT_X = $12
FILENAME_INPUT_Y = $0D

LOADING_INFO_X = $05
LOADING_INFO_Y = $10

CURSOR_START_X = $12
CURSOR_START_Y = $0D
CURSOR_STOP_Y = $0D

HEADER_COLOR = $B1

.proc init
init:
  jsr draw_ui

  jsr ui::update_upper_info
  ;jsr ui::print_song_info

  lda #$20
  sta VERA_addr_high ; Set primary address bank to 0, stride to 2
  vera_layer0_16_color
  ;jsr ui::clear_lower_frame
  jsr draw_save_frame
  stz zp_KEY_PRESSED

@cursor_start_position:
  stz cursor_layer

  lda #CURSOR_START_X
  sta cursor_x
  lda #CURSOR_START_Y
  sta cursor_y
  jsr graphics::drawing::cursor_plot
.endproc

.proc keyboard_loop
@keyboard_loop:
  lda zp_KEY_PRESSED
  cmp #COMMAND_S
  beq @save_song_jump
  cmp #COMMAND_L
  beq @load_song_jump
  cmp #COMMAND_N
  beq @nuke_song_jump
  cmp #ALT_Z
  beq @record_to_zsm
  cmp #KEY_LEFT
  beq @cursor_left_jump
  cmp #KEY_RIGHT
  beq @cursor_right_jump
  cmp #SPACE
  beq @cursor_right_jump
  cmp #PETSCII_BACKSPACE
  beq @backspace
  ; Start at !
  cmp #KEY_BANG
  bpl @print_letters
  jmp main_application_loop

@save_song_jump:
  jmp @save_song

@load_song_jump:
  jmp @load_song

@nuke_song_jump:
  jmp @nuke_song

@record_to_zsm:
  lda #$01
  sta zp_ZSM_ENABLE
  stz zp_ROW_NUMBER
  stz zp_ORDER_NUMBER
  ldx #PLAY_SONG_MODULE
  ;jmp (init_jump_table,x)

@cursor_left_jump:
  jmp @cursor_left
@cursor_right_jump:
  jmp @cursor_right

@print_letters:
  pha
  lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy
  ldx #EDITABLE_TEXT_COLORS
  stx r0
  pla
  jsr graphics::printing::print_alpha_char
  jsr ui::cursor_right
  jsr @update_filename
@print_end:
  jmp main_application_loop

@backspace:
  ; If we're at the beginning, don't move left
  lda cursor_x
  cmp #CURSOR_START_X
  beq @backspace_end

  jsr ui::cursor_left
  lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy
  lda #SCREENCODE_BLANK
  sta VERA_data0
  jsr @update_filename

@backspace_end:
  jmp main_application_loop

@cursor_left:
  lda cursor_x
  cmp #CURSOR_START_X
  beq @cursor_left_end
  jsr ui::cursor_left
@cursor_left_end:
  jmp main_application_loop

@cursor_right:
  jsr ui::cursor_right
@cursor_right_end:
  jmp main_application_loop

; Misc bank is where the title, composer, and order list live
@change_to_misc_bank:
  rambank zp_BANK_MISC

; Update filename
@update_filename:
  lda #$20
  sta VERA_addr_high
  lda #FILENAME_INPUT_X
  ldy #FILENAME_INPUT_Y
  jsr graphics::drawing::goto_xy
  ldx #FILENAME_MAX_LENGTH
  ldy #$00
; Screencode to PETSCII conversion logic happening here
@update_filename_loop:
  lda VERA_data0
  cmp #SCREENCODE_BLANK
  beq @update_char
@continue:
  cmp #PETSCII_PERIOD
  bcs @update_char
  clc
  adc #$40
@update_char:
  sta filename,y
  iny
  dex
  bne @update_filename_loop
@update_filename_end:
  rts


@load_song:
  rambank zp_BANK_MISC
  stz VERA_addr_high
  print_string_macro load_text
  jsr tracker::load_song
  print_string_macro done_text
  print_string_macro version_text
  lda song_file_version
  jsr graphics::printing::print_hex
  lda song_file_version
  cmp #SONG_FILE_VERSION
  blt @convert_song

@finish_loading:
  rambank zp_BANK_MISC
  
  lda starting_speed
  sta zp_SPEED
  ; Start on first pattern (should the user immediately hit F2)
  ldx #$00
  lda order_list,x
  sta zp_PATTERN_NUMBER

  ; Make sure we update the global FM data
  jsr sound::fm::init

  jsr ui::print_song_info
  jsr ui::print_speed
  jsr draw_save_frame
  jmp main_application_loop

@convert_song:
  jsr convert_song
  bra @finish_loading



; Save song
@save_song:
  stz VERA_addr_high
  print_string_macro save_text
  jsr save_song
  print_string_macro done_text
  jmp main_application_loop

; Obliterate Song
@nuke_song:
  print_string_macro nuke_text
  jsr tracker::init_song
  print_string_macro done_text
  jmp main_application_loop

; Strings

;.byte SCREENCODE_XY,$19,$10

save_text: 
  .byte SCREENCODE_XY,LOADING_INFO_X,LOADING_INFO_Y
  .byte SCREENCODE_COLOR,TITLE_COLORS
  .byte "saving...",0

load_text: 
  .byte SCREENCODE_XY,LOADING_INFO_X,LOADING_INFO_Y
  .byte SCREENCODE_COLOR,TITLE_COLORS
  .byte "loading...",0

nuke_text:
  .byte SCREENCODE_XY,LOADING_INFO_X,LOADING_INFO_Y
  .byte SCREENCODE_COLOR,TITLE_COLORS
  .byte "nuking...",0

done_text: 
  .byte SCREENCODE_XY,LOADING_INFO_X+$0A,LOADING_INFO_Y
  .byte SCREENCODE_COLOR,TITLE_COLORS
  .byte "done!",0

version_text: 
  .byte SCREENCODE_XY,LOADING_INFO_X,LOADING_INFO_Y + $01
  .byte SCREENCODE_COLOR,TITLE_COLORS
  .byte "song version: ",0
.endproc

.proc convert_song
  print_string_macro convert_song_text

  lda song_file_version
  cmp #$02
  beq @convert_2_to_3
  cmp #$03
  beq @convert_3_to_4
  rts

  ; We only have one routine to convert from version 2 to 3
  ; so no if checks here (yet)
  @convert_2_to_3:
    jsr convert_2_to_3::run
  @convert_3_to_4:
    jsr convert_3_to_4::run
  ; Done!
  print_string_macro done_text
  rts

convert_song_text:
  .byte SCREENCODE_XY,LOADING_INFO_X,LOADING_INFO_Y + $02
  .byte SCREENCODE_COLOR,TITLE_COLORS
  .byte "converting song to latest format...",0

done_text: 
  .byte SCREENCODE_XY,LOADING_INFO_X+35,LOADING_INFO_Y + $02
  .byte "done!",0
.endproc


.proc draw_save_frame
  ; Constants
  SAVE_HEADER_X = $20
  SAVE_HEADER_Y = $08

  FILENAME_INPUT_X = $12
  FILENAME_INPUT_Y = $0D

  FILENAME_EXT_HEADER_X = $20
  FILENAME_EXT_HEADER_Y = $0D

  SAVE_ACCEPT_X = $12
  SAVE_ACCEPT_Y = $0E

start:
  lda #$10
  sta VERA_addr_high
  print_string_macro_position_color filename, #FILENAME_INPUT_X, #FILENAME_INPUT_Y, #EDITABLE_TEXT_COLORS

end:
  rts

.endproc



.endscope
