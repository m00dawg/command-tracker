.segment "LOADING_MODULE"

.scope loading

.proc init

  ; Try CP437
  ;lda #$07
  ;jsr screen_set_charset
  ;lda #$0F
  ;jsr CHROUT

  ;jsr files::enable_fast_io

  stz VERA_ctrl      ; Select primary VRAM address
  stz VERA_addr_med  ; Set primary address med byte to 0
  stz VERA_addr_low  ; Set Primary address low byte to 0
  stz VERA_addr_high ; Set primary address bank to 0, stride to 0

  lda #RES128x128x16      ; L0 is the pattern scroll, instrument edit, env edit space
  sta VERA_L0_config
  lda #RES128x64x16       ; L1 is the UI
  ;lda #RES128x64x256       ; L1 is the UI
  sta VERA_L1_config

  ; Set display to 640x480
  lda #%10000000
  sta VERA_dc_hscale
  lda #%10000000
  sta VERA_dc_vscale

  ; L0 = Pattern Data 
  ; ($10000 start of HiVRAM)
  lda #L0_MAPBASE
  sta VERA_L0_mapbase
  ; L1 = UI
  ; ($00000 start of LoVRAM)
  stz VERA_L1_mapbase

  ; Set the default character tiles
  lda #TILEBASE
  sta VERA_L0_tilebase
  sta VERA_L1_tilebase

  ; Turn on both layers
  ; Field : Sprit E : L1 E : L2 E : NC : Chroma/HV : Output x2
  lda VERA_dc_video
  ora #%00110000
  sta VERA_dc_video

  ; Clear pattern VRAM area
  stz r1
  lda #$80
  sta r2
  lda #%00000001 ; Hi RAM
  jsr graphics::vera::clear_vram

  ; Load custom palette
  jsr graphics::vera::load_palette_16

  ; Set VERA Stride and bank
  lda #$10          ; Set primary address bank to 0, stride to 1
  sta VERA_addr_high 

	; Draw Frame
	stz zp_ARG0	;X1
	stz zp_ARG1	;Y1
	lda #$4F		;X2
	sta zp_ARG2
	lda #$3B		;Y2
	sta zp_ARG3
	lda #FRAME_COLOR
	sta zp_ARG4
	jsr graphics::drawing::draw_rounded_box

	; Clear inner frame
	lda #$01
	sta zp_ARG0
	sta zp_ARG1
	lda #$4E
	sta zp_ARG2
	lda #$3A
	sta zp_ARG3
	lda #$B1
	sta zp_ARG4
	jsr graphics::drawing::draw_solid_box

	print_screencodes_macro_with_colors title_row1, title_colors, #$03, #$02
	print_screencodes_macro_with_colors title_row2, title_colors, #$03, #$03
	print_string_macro_with_colors version_string, title_colors, #$03, #$04
	print_string_macro_with_colors tagline_string, title_colors, #$03, #$05
  print_string_macro text_strings  

  ; Set ROM Bank to 0 (Kernel)
  ; This speeds up calls to kernel routines by avoiding unnecessary jumps
  ; when using the default ROM bank (4) which is for BASIC and we don't need
  ; here.
  stz ROM_BANK

  ; Calculate bank pages based on available RAM
  sec ; Otherwise MEMTOP doesn't return correct info
  jsr MEMTOP
  sta zp_BANK_TABLES
  dec 
  sta zp_BANK_FINS
  dec 
  sta zp_BANK_VINS
  dec
  sta zp_BANK_ENVS
  dec
  sta zp_BANK_MISC
  dec
  sta sound::pcm::bank_end
  suba #sound::pcm::NUM_SAMPLES
  sta sound::pcm::bank_start
  dec
  sta zp_MAX_PATTERN

  ; Enable VBLANK Interrupt
  ; We will use VBLANK from the VERA as our time keeper, so we enable
  ; the VBLANK interupt
  lda #VBLANK_MASK
  sta VERA_ien

  ; Set state to 0 (idle/stopped)
  stz zp_STATE

  ; ZSM disabled by default
  stz zp_ZSM_ENABLE

  ; Set pattern pointer
  lda #<PATTERN_ADDRESS
  sta zp_PAGE_POINTER
  lda #>PATTERN_ADDRESS
  sta zp_PAGE_POINTER + 1

  ; Start at first pattern
  lda #$01
  sta zp_PATTERN_NUMBER

  ; Manully set octave (for now)
  lda #$03
  ;sta zp_SPEED
  sta user_octave

  ; Init MIDI
  jsr sound::midi::init
  jsr sound::vera::init
  jsr sound::fm::init

  jsr tracker::init_song
  
  ; Zero out our pattern nav parameters
  stz tracker::modules::edit_pattern::channel_number
  stz tracker::modules::edit_pattern::screen_channel
  stz tracker::modules::edit_pattern::start_channel
  stz tracker::modules::edit_pattern::column_pos
  stz tracker::modules::edit_pattern::block_source_pattern
  stz tracker::modules::edit_pattern::block_begin_channel
  stz tracker::modules::edit_pattern::block_begin_row
  stz tracker::modules::edit_pattern::block_end_channel
  stz tracker::modules::edit_pattern::block_end_row
  stz tracker::modules::macros::current_macro_number
  stz tracker::modules::macros::current_macro_effect
  stz zp_ROW_NUMBER


  ;lda #midi::INTR_ENABLE
  ;ldy #midi::INTERRUPT_ENABLE_OFFSET
  ;sta (zp_MIDI_BASE),y

	rts

title_row1:
	.byte $FE,$6C,$7B,$62,$20,$6C,$7B,$6C,$62,$20,$E1,$7B,$62,$20,$62,$E1,$7E,$FC,$7E,$62,$20,$62,0
title_row2:
	.byte $E2,$7C,$20,$E2,$7E,$7E,$7E,$7E,$7E,$7E,$7C,$20,$7E,$7C,$7C,$7C,$7E,$7E,$7E,$E2,$7E,$7E,0
;title_colors:
;	.byte $B6,$BD,$BD,$B7,$B7,$B8,$B8,$BA,$BA,$B4,$B4,$B4,$B4,$B4,$B4,$B4,$B4,$B4,$B4,$B4,$B4,$B4
version_string: 
  .byte "v0.72 almost beta",0
; Shared by version
title_colors: 
	.byte $B6,$BD,$BD,$B7,$B7,$B8,$B8,$BA,$BA,$BA,$B4,$B4,$B4,$B4,$B4,$B4,$B4,$B4,$B4,$B4,$B4,$B4,$B4,$B4
	;.byte $B6,$B7,$B8,$B6,$B6
	;.byte $B6
	;.byte $B6,$B6,$B6,$B6,$B6
	;.byte $B6
	;.byte $B6,$B6,$B6,$B6,$B6,$B6,$B6,$B6,$B6,$B6,$B6
tagline_string: .byte "midi and stuff",0

text_strings:
author_string: 
  .byte SCREENCODE_XY,$03,$09
  .byte SCREENCODE_COLOR,$B3
  .byte "written by tim soderstrom"
info_string: 
  .byte SCREENCODE_XY,$03,$30
  .byte SCREENCODE_COLOR,$B3
  .byte "visit ",SCREENCODE_COLOR,$B1,"dreamtracker.org"
  .byte SCREENCODE_COLOR,$B3," for latest version and full manual",SCREENCODE_RETURN
  .byte "if you want to support development, consider supporting my band",SCREENCODE_RETURN
  .byte "at "
  .byte SCREENCODE_COLOR,$B1,"victimcache.com"
loading_string: 
  .byte SCREENCODE_XY,$1A,$15
  .byte SCREENCODE_COLOR,$B7
  .byte "loading...",0

.endproc

.endscope
