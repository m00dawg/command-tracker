; Draws the lower UI element for the orders view
.proc draw_orders_frame
@list_orders:
  sei
  push_rambank
  rambank zp_BANK_MISC

  lda #ORDER_LIST_X
  ldy #ORDER_LIST_Y
  jsr graphics::drawing::goto_xy
  ldx order_list_start
@list_orders_loop:
  lda #ORDER_LIST_X
  jsr graphics::drawing::goto_xy
  lda #TITLE_COLORS
  sta r0
  txa
  jsr graphics::printing::print_hex

  print_char_with_color #COLON, #TITLE_COLORS
  print_char_with_color #SPACE, #TITLE_COLORS
  ; Do this up to the song's current orders
  ;cpx order_list_length
  ;bpl @done_with_user_orders
  lda order_list,x
  pha
  lda #EDITABLE_TEXT_COLORS
  sta zp_TEXT_COLOR
  pla
  beq @zero_order
  jsr graphics::printing::print_hex

  bra @end_loop

; Add dashes to orders with 00
@zero_order:
  print_char_with_color #PETSCII_DASH, #EDITABLE_TEXT_COLORS
  print_char_with_color #PETSCII_DASH, #EDITABLE_TEXT_COLORS
@end_loop:
  inx
  iny
  cpy #ORDERS_STOP_Y
  bne @list_orders_loop

@end:
  pop_rambank
  cli
  rts


.endproc
