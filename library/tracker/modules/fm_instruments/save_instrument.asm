.proc save_instrument
@start:
	lda #%00010000	; Inc set to 1, lo ram
  	sta VERA_addr_high
	ldx zp_INSTRUMENT

; global_volume
@global_volume:
    lda #GLOBAL_VOL_X
    ldy #GLOBAL_VOL_Y
    jsr graphics::drawing::goto_xy
    lda VERA_data0
    sta r0
    lda VERA_data0
    lda VERA_data0
    sta r1
    jsr graphics::printing::chars_to_number
    and #%01111111
    sta fm_instruments_global_volume,x

; global_to_op_volume
@global_op_volume:
    ; skip for now since we have to do toggle stuff

; pan_feedback_algo
@algo:
	lda #ALGO_X
	ldy #ALGO_Y
  	jsr graphics::drawing::goto_xy
    lda VERA_data0
    jsr graphics::printing::convert_to_number
    and #%00000111
    sta temp_register0
@pan_left:
	lda #LEFT_X
    ldy #LEFT_Y
	jsr graphics::drawing::goto_xy
    lda VERA_data0 ; Character
    lda VERA_data0 ; Color (which we care about)
    cmp #LEFT_PAN_ACTIVE_COLOR
    beq @left_pan_active
@left_pan_inactive:
    lda #%10111111
    and temp_register0
    sta temp_register0
    bra @pan_right
@left_pan_active:
    lda #%01000000
    ora temp_register0
    sta temp_register0
@pan_right:
	lda #RIGHT_X
    ldy #RIGHT_Y
	jsr graphics::drawing::goto_xy
    lda VERA_data0 ; Character
    lda VERA_data0 ; Color (which we care about)
    cmp #RIGHT_PAN_ACTIVE_COLOR
    beq @right_pan_active
@right_pan_inactive:
    lda #%01111111
    and temp_register0
    sta temp_register0
    bra @m1_feedback
@right_pan_active:
    lda #%10000000
    ora temp_register0
    sta temp_register0
@m1_feedback:
	lda #M1_FEEDBACK_X
	ldy #M1_FEEDBACK_Y
  	jsr graphics::drawing::goto_xy
    lda VERA_data0
    jsr graphics::printing::convert_to_number
    asl3
    and #%00111000
    ora temp_register0
    sta fm_instruments_pan_feedback_algo,x

; modulation_sensitivity
@phase_mod_sense:
    lda #SENSITIVITY_PHASE_X
    ldy #SENSITIVITY_PHASE_Y
    jsr graphics::drawing::goto_xy
    lda VERA_data0
    jsr graphics::printing::convert_to_number
    asl4
    and #%01110000
    sta temp_register0
@am_mod_sense:
    lda #SENSITIVITY_AMP_X
    ldy #SENSITIVITY_AMP_Y
    jsr graphics::drawing::goto_xy
    lda VERA_data0
    jsr graphics::printing::convert_to_number
    and #%00000011
    ora temp_register0
    sta fm_instruments_modulation_sensitivity,x

; Save the rest of the operator values
@m1:
	stz operator_offset
	ldy #M1_VOLUME_Y
	jsr save_operator
@m2:
	inc operator_offset
	ldy #M2_VOLUME_Y
	jsr save_operator
@c1:
	inc operator_offset
	ldy #C1_VOLUME_Y
	jsr save_operator
@c2:
	inc operator_offset
	ldy #C2_VOLUME_Y
	jsr save_operator

@end:
	lda #%00010001	; Inc set to 1, hi ram
  	sta VERA_addr_high
	rts
.endproc

.proc save_operator
; We use volume, *not* attenuation which the actual YM2151
; uses. Volume is converted to attenuation when writing
; to the chip registers (e.g. in read_row)
@volume:
    ;fm_fetch_op_address operator_offset, volume_lo, volume_hi, op_register_address
	lda #OP_VOLUME_X
	jsr graphics::drawing::goto_xy
    lda VERA_data0  ; high value
    sta r0
    lda VERA_data0  ; Color
    lda VERA_data0  ; low value
    sta r1
    jsr graphics::printing::chars_to_number
    fm_store_op operator_offset, volume_lo, volume_hi
@am_mode_and_decay1:
@am_mode:
	iny	; From here on out every other item is under volume_y
    lda #OP_AM_MOD_EN_X
    jsr graphics::drawing::goto_xy
    lda VERA_data0  ; Char
    lda VERA_data0  ; Color
    cmp #PARAM_ACTIVE_COLOR
    beq @am_mode_active
@am_mode_inactive:
    lda #%00000000
    sta temp_register0
    bra @decay1
@am_mode_active:
    lda #%10000000
    sta temp_register0
@decay1:
	phy
	iny
	iny
	iny
	lda #OP_DECAY_1_RATE_X
	jsr graphics::drawing::goto_xy
    lda VERA_data0
    sta r0
    lda VERA_data0
    lda VERA_data0
    sta r1
    jsr graphics::printing::chars_to_number   
    and #%00011111
    ora temp_register0
    fm_store_op operator_offset, am_decay1_lo, am_decay1_hi 
@keyscale_attack:
    ply	; Go back to the 2nd-row Y-position offset
@keyscale:
	lda #OP_KEY_SCALE_X
	jsr graphics::drawing::goto_xy
    lda VERA_data0
    jsr graphics::printing::convert_to_number
    asl6
	sta temp_register0
@attack:
	phy
	iny
	iny
	lda #OP_ATTACK_X
	jsr graphics::drawing::goto_xy
    lda VERA_data0
    sta r0
    lda VERA_data0
    lda VERA_data0
    sta r1
    jsr graphics::printing::chars_to_number   
    and #%00011111
    ora temp_register0
    fm_store_op operator_offset, ks_attack_lo, ks_attack_hi
@detune_fine_freq_mul:
	ply	; Go back to the 2nd-row Y-position offset
@freq_mul:
	lda #OP_FREQ_MULT_X
	jsr graphics::drawing::goto_xy
    lda VERA_data0
    jsr graphics::printing::convert_to_number
    and #%00001111
    sta temp_register0
@detune_fine:
	iny	; No need to push as everything is on this row or lower
	lda #OP_DETINE_FINE_X
	jsr graphics::drawing::goto_xy
    lda VERA_data0
    jsr graphics::printing::convert_to_number
    asl4
	and #%01110000
    ora temp_register0
    fm_store_op operator_offset, detune_fine_frequency_mul_lo, detune_fine_frequency_mul_hi
@detune_coarse_decay2:
@detune_coarse:
	lda #OP_DETUNE_COARSE_X
	jsr graphics::drawing::goto_xy
    lda VERA_data0
    jsr graphics::printing::convert_to_number
	asl6
    and #%11000000
    sta temp_register0
@decay2:
	lda #OP_DECAY_2_RATE_X
	iny
	phy	; From here on out this is where y-positions now start
	iny
	jsr graphics::drawing::goto_xy
    lda VERA_data0
    sta r0
    lda VERA_data0
    lda VERA_data0
    sta r1
    jsr graphics::printing::chars_to_number  
	and #%00011111
    ora temp_register0
    fm_store_op operator_offset, detune_coarse_decay2_lo, detune_coarse_decay2_hi
@decay1_release:
	ply
@release:
	lda #OP_RELEASE_X
	jsr graphics::drawing::goto_xy
    lda VERA_data0
    jsr graphics::printing::convert_to_number    
	and #%00001111
    sta temp_register0
@decay1_level:
	lda #OP_DECAY_1_LEVEL_X
	iny
	jsr graphics::drawing::goto_xy
    lda VERA_data0
    jsr graphics::printing::convert_to_number
	asl4
    and #%11110000
	ora temp_register0
    fm_store_op operator_offset, decay1_release_lo, decay1_release_hi
@end:
	rts
.endproc