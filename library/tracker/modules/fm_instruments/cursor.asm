.scope cursor

.proc cursor_up
    lda edit_mode
    bne @inst_edit
    jsr ui::instruments::cursor::name_cursor_up
    jsr load_instrument
    rts
@inst_edit:
    jsr instrument_edit_cursor_up
    rts
.endproc

.proc cursor_down
    lda edit_mode
    bne @inst_edit
    jsr ui::instruments::cursor::name_cursor_down
    jsr load_instrument
    rts
@inst_edit:
    jsr instrument_edit_cursor_down
    rts
.endproc

.proc cursor_left
    lda edit_mode
    bne @inst_edit
    jsr ui::instruments::cursor::name_cursor_left
    rts
@inst_edit:
    jsr instrument_edit_cursor_left
    rts
.endproc

.proc cursor_right
    lda edit_mode
    bne @inst_edit
    jsr ui::instruments::cursor::name_cursor_right
    rts
@inst_edit:
    jsr instrument_edit_cursor_right
    rts
.endproc

.endscope