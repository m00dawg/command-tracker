; Load sample from a RAW file on disk
; RAW file must be 8-bit signed and less than 8k

.proc import_sample
@update_filename:
  lda #FILENAME_X
  ldy #FILENAME_Y
  jsr graphics::drawing::goto_xy

  lda #$20
  sta VERA_addr_high

  ldx #SAMPLE_FILENAME_MAX_LENGTH
  ldy #$00
; Screencode to PETSCII conversion logic happening here
@update_filename_loop:
  lda VERA_data0
  cmp #SCREENCODE_BLANK
  beq @update_end
  cmp #PETSCII_PERIOD
  bcs @update_char
  clc
  adc #$40
@update_char:
  sta sound::pcm::sample_filename,y
  iny
  dex
  bne @update_filename_loop
@update_end:
  jsr load_from_disk
  rts
.endproc

.proc load_from_disk
FILE_NUMBER = $01  ; set to 1 for now as we will only be opening 1 file at a time
DEVICE = $08  ; set to 8 for host system (emulator)
SECONDARY_ADDR_WRITE = $01 ; Ignore file header, overwrite
SECONDARY_ADDR_READ = $02 ; Ignore file header

@start:
  ldy #$00
; Get the length by finding the first space
@find_filename_length:
  lda sound::pcm::sample_full_filename,y
  cmp #SCREENCODE_BLANK
  beq @set_filename
  iny
  jmp @find_filename_length
@set_filename:
  tya
  ldx #<sound::pcm::sample_full_filename
  ldy #>sound::pcm::sample_full_filename
  jsr SETNAM
  lda #FILE_NUMBER
  ldx #DEVICE
  ldy #SECONDARY_ADDR_READ
  jsr SETLFS
  jsr OPEN
  sta error_code
  ldx #FILE_NUMBER
  jsr CHKIN
  
@find_rambank_location:
  lda zp_INSTRUMENT
  adda sound::pcm::bank_start
  sta RAM_BANK
@clear_bank:
  jsr memory::clear_ram_bank

@load_data:
  lda #$00
  ldx #<sound::pcm::SAMPLE_ADDRESS
  ldy #>sound::pcm::SAMPLE_ADDRESS
  jsr LOAD
  lda #FILE_NUMBER
  jsr CLOSE
  jsr CLRCHN

@clear_filename:
  lda #SCREENCODE_BLANK
  ldx #$00
@clear_filename_loop:
  sta sound::pcm::sample_filename,x
  inx
  cpx #SAMPLE_FILENAME_MAX_LENGTH
  bne @clear_filename_loop

@set_sample_to_active:
  rambank zp_BANK_MISC
  ldx zp_INSTRUMENT
  ; Active and 8 volume
  lda #%10001000
  sta sample_states,x

@print_error:
  stz VERA_addr_high
  lda error_code
  beq @ok
@emu_check_ok:
  cmp #$57
  beq @ok
@error:
  print_string_macro error
  bra @end
@ok:
  print_string_macro ok
@end:
  lda #LOAD_MESSAGE_X
  ldy #LOAD_MESSAGE_Y + 1
  jsr graphics::drawing::goto_xy
  lda error_code
  jsr graphics::printing::print_hex
  rts

@data:
  error: 
    .byte SCREENCODE_XY,LOAD_MESSAGE_X, LOAD_MESSAGE_Y
    .byte SCREENCODE_COLOR,LOAD_MESSAGE_ERROR_COLOR
    .byte "error",0
  ok: 
    .byte SCREENCODE_XY,LOAD_MESSAGE_X, LOAD_MESSAGE_Y
    .byte SCREENCODE_COLOR,LOAD_MESSAGE_OK_COLOR
    .byte "ok   ",0


.endproc