.segment "SAMPLES_MODULE"

; Many routines were copied from vera_instruments
; which means there could be some big space savings
; perhaps by reusing these some of them across the modules

.scope samples

.word init
.word keyboard_loop


.include "library/tracker/modules/samples/cursor.asm"
.include "library/tracker/modules/samples/load_sample.asm"
.include "library/tracker/modules/samples/import_sample.asm"
.include "library/tracker/modules/samples/draw_ui.asm"

; Scope Constants
SAMPLE_FILENAME_MAX_LENGTH = $10

; Positions
VOLUME_X  = $23
VOLUME_Y  = $0E
FILENAME_X = $24
FILENAME_Y = $20
LOAD_MESSAGE_X = $1B
LOAD_MESSAGE_Y = $22
LOAD_MESSAGE_ERROR_COLOR = $B2
LOAD_MESSAGE_OK_COLOR = $BD

; Colors
SAMPLE_TEXT_COLOR = $01
SAMPLE_ACTIVE_COLOR = $01
SAMPLE_INACTIVE_COLOR = $04
VOLUME_COLOR = $CE

; Sample Edit Positions
SAMPLE_EDIT_VOLUME = $00
SAMPLE_EDIT_FILENAME = $01


; Variables
; In main memory so it's preserved across
; UI moves
selected_sample: .byte 00
edit_mode = zp_TMP0
sample_edit_pos = zp_TMP1
error_code = zp_TMP2
bitmask_temp = zp_MATH0

; Calling Args for ui::instruments::list_names
instrument_base_address = zp_ARG0
text_color              = zp_ARG1
number_color            = zp_ARG2
num_instruments         = zp_ARG3
edit_start_x            = zp_ARG4
edit_start_y            = zp_ARG5

; Locations where instrument positions are
; (for cursor hopping)
sample_edit_x_pos_lut:
  ; Volume, Filename
  .byte VOLUME_X,FILENAME_X
sample_edit_y_pos_lut:
  ; Volume, Filename
  .byte VOLUME_Y,FILENAME_Y


.proc init
@start:
  jsr draw_ui
  ;lda #SAMPLES_SCREEN_FILENAME_LENGTH
  ;ldx #<SAMPLES_SCREEN_FILENAME
  ;ldy #>SAMPLES_SCREEN_FILENAME
  ;jsr files::load_to_vram
  
  ; Sample names and metadata is stored in the misc bank
  rambank zp_BANK_MISC


  lda #%00010001	; Inc set to 1, hi ram
  sta VERA_addr_high
  turn_on_layer_0
  vera_layer0_16_color

  lda #<sound::pcm::BASE_NAMES_ADDRESS
  sta instrument_base_address
  lda #>sound::pcm::BASE_NAMES_ADDRESS
  sta instrument_base_address + 1
  lda #SAMPLE_TEXT_COLOR
  sta text_color
  lda #SAMPLE_INACTIVE_COLOR
  sta number_color
  lda #sound::pcm::NUM_SAMPLES
  sta num_instruments
  lda #VOLUME_X
  sta edit_start_x
  lda #VOLUME_Y
  sta edit_start_y
  jsr ui::instruments::list_names

  stz zp_INSTRUMENT
  stz edit_mode

  jsr ui::instruments::cursor::reset_on_names
  jsr load_sample
  
  stz zp_KEY_PRESSED
.endproc

.proc keyboard_loop
  lda zp_KEY_PRESSED
  cmp #KEY_UP
  beq @cursor_up_jump
  cmp #KEY_DOWN
  beq @cursor_down_jump
  cmp #KEY_LEFT
  beq @cursor_left_jump
  cmp #KEY_RIGHT
  beq @cursor_right_jump
  cmp #KEY_TAB
  beq @change_edit_mode
  cmp #SPACE
  beq @spacebar_jump
  cmp #ENTER
  beq @enter_jump
  cmp #PETSCII_BACKSPACE
  beq @backspace_jump
  ; Start at period
  cmp #PETSCII_PERIOD
  bpl @print_characters_jump
  jmp main_application_loop

; Cusor Up
@cursor_up_jump:
  jsr cursor::cursor_up
  jmp main_application_loop

; Cursor Down
@cursor_down_jump:
  jsr cursor::cursor_down
  jmp main_application_loop

; Cursor Left
@cursor_left_jump:
  jsr cursor::cursor_left
  jmp main_application_loop

; Cursor Right
@cursor_right_jump:
  jsr cursor::cursor_right
  jmp main_application_loop

@spacebar_jump:
  jmp spacebar

@backspace_jump:
  jmp backspace

; Enter pressed
@enter_jump:
  lda edit_mode
  bne @import_sample
  jmp main_application_loop
@import_sample:
  jsr import_sample
  jmp main_application_loop

@print_characters_jump:
  jsr print_character
  jmp main_application_loop

@change_edit_mode:
  lda edit_mode
  bne @sample_names
@sample_settings:
  inc edit_mode
  stz sample_edit_pos
  jsr ui::instruments::cursor::reset_on_edit
  jmp @change_edit_mode_end
@sample_names:
  stz edit_mode
  stz sample_edit_pos
  jsr ui::instruments::cursor::reset_on_names
@change_edit_mode_end:
  jmp main_application_loop
.endproc

; If we pressed space in sample name mode, add a space
; otherwise the behavior is dependent on where we are 
; in the instrument edit screen
.proc spacebar
  tax
  lda edit_mode
  bne @sample_edit
  jmp @spacebar_print_character
@sample_edit:
  jmp @spacebar_end
@spacebar_print_character:
  txa  
  jsr print_character
@spacebar_end:
  jmp main_application_loop
.endproc

.proc backspace
backspace:
  lda edit_mode
  bne @filename
@instrument_name:
  ; If we're at the beginning, don't move left
  lda cursor_x
  cmp #ui::instruments::INSTRUMENT_NAMES_CURSOR_START_X
  beq @backspace_end
  bra @backspace
@filename:
  ; If we're at the beginning, don't move left
  lda cursor_x
  cmp #FILENAME_X
  beq @backspace_end
@backspace:
  jsr ui::cursor_left
  lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy
  lda #SCREENCODE_BLANK
  sta VERA_data0
  ; Kinda hacky since it saves the name even if you're typing the file but *shrug*
  lda #<sound::pcm::BASE_NAMES_ADDRESS
  sta instrument_base_address
  lda #>sound::pcm::BASE_NAMES_ADDRESS
  sta instrument_base_address + 1
  jsr ui::instruments::save_names
@backspace_end:
  jmp main_application_loop
.endproc

.proc print_character
  jsr ui::instruments::print_character
  tax
  lda edit_mode
  bne @sample_edit_mode
  txa
  lda #<sound::pcm::BASE_NAMES_ADDRESS
  sta instrument_base_address
  lda #>sound::pcm::BASE_NAMES_ADDRESS
  sta instrument_base_address + 1
  jsr ui::instruments::save_names
  rts
@sample_edit_mode:
  lda sample_edit_pos
  beq @volume
  rts
@volume:
  jsr update_sample
@print_end:
  rts
.endproc

; Update sample parameters
.proc update_sample
  lda #VOLUME_X
  ldy #VOLUME_Y
  jsr graphics::drawing::goto_xy
  lda VERA_data0
  jsr graphics::printing::convert_to_number
  sta bitmask_temp
  ldx zp_INSTRUMENT
  lda sample_states,x
  and #%11110000
  ora bitmask_temp
  sta sample_states,x
  rts
.endproc

.endscope
