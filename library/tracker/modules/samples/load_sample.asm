.proc load_sample
  lda VERA_addr_high
  pha
  lda #%00010000	; Inc set to 1, low ram
  sta VERA_addr_high
  lda #VOLUME_X
  ldy #VOLUME_Y
  jsr graphics::drawing::goto_xy

  lda #VOLUME_COLOR
	sta zp_TEXT_COLOR

  rambank zp_BANK_MISC
  ldx zp_INSTRUMENT
  lda sample_states,x
  and #%00001111
  jsr graphics::printing::print_single_hex
  pla
  sta VERA_addr_high
  rts
.endproc