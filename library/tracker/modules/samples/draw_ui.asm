.proc draw_ui
	PCM_HEADER_X = $02
	PCM_HEADER_Y = $08
	VOLUME_HEADER_X = $1B
	VOLUME_HEADER_Y = $0E
	FILE_HEADER_X = $1B
	FILE_HEADER_Y = $1E
	SAMPLE_NAME_START_X = $03
	SAMPLE_NAME_START_Y = $0D

	HEADER_COLOR = $B1
	LABEL_COLOR = $BC

.IF CPU_65816
	ldx #$16
	acc16b
	lda #$0211
	sta palette,x
	acc8b
.ELSE
	; Change $0B pallete color to a hint of red
	ldx #$16
	lda #$11
	sta palette,x
	inx
	lda #$02
	sta palette,x
.ENDIF

	jsr graphics::vera::load_palette_16

	; Set VERA Stride and bank
  lda #$10          ; Set primary address bank to 0, stride to 1
  sta VERA_addr_high 

	; Draw Frame
	stz zp_ARG0	;X1
	stz zp_ARG1	;Y1
	lda #$4F		;X2
	sta zp_ARG2
	lda #$3B		;Y2
	sta zp_ARG3
	lda #FRAME_COLOR
	sta zp_ARG4
	jsr graphics::drawing::draw_rounded_box

	; Clear inner frame
	lda #$01
	sta zp_ARG0
	sta zp_ARG1
	lda #$4E
	sta zp_ARG2
	lda #$3A
	sta zp_ARG3
	lda #$B1
	sta zp_ARG4
	jsr graphics::drawing::draw_solid_box

	jsr ui::print_header
	jsr ui::print_song_info
  jsr ui::print_speed
  jsr ui::print_current_order
  jsr ui::print_current_pattern_number

	print_string_macro pcm_header
	print_string_macro volume_header
	print_string_macro file_load_header
	
	
	; Print sample name space for the number of samples
	ldx #$00
	lda #SAMPLE_NAME_START_Y
	sta zp_TMP0
@sample_loop:
	print_string_macro_position_color sample_name_block, #SAMPLE_NAME_START_X, zp_TMP0, #HEADER_COLOR
	inc zp_TMP0
	inx
	cpx #sound::pcm::NUM_SAMPLES
	bne @sample_loop
	rts

	pcm_header:
		.byte SCREENCODE_XY,PCM_HEADER_X, PCM_HEADER_Y
		.byte SCREENCODE_COLOR,HEADER_COLOR
		.byte "pcm samples",0
	volume_header:
		.byte SCREENCODE_XY,VOLUME_HEADER_X,VOLUME_HEADER_Y
		.byte SCREENCODE_COLOR,HEADER_COLOR
		.byte "volume:    $0-f",0
	file_load_header:
		.byte SCREENCODE_XY,FILE_HEADER_X,FILE_HEADER_Y
		.byte SCREENCODE_COLOR,HEADER_COLOR
		.byte "load from file",SCREENCODE_RETURN
		.byte SCREENCODE_RETURN
		.byte "filename: "
		.byte SCREENCODE_RETURN
		.byte SCREENCODE_RETURN
		.byte "enter filename then press enter to load"
		.byte SCREENCODE_RETURN
		.byte "file format is 8-bit signed, 8k max.",0
	sample_name_block:
		.byte SCREENCODE_COLOR,$00,"  "
		.byte SCREENCODE_COLOR,LABEL_COLOR,":"
		.byte SCREENCODE_COLOR,$00,"                ",0
.endproc