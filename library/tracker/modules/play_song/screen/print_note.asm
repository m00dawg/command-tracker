; Print routines
.proc print_note
@print_note:
  lda #NOTE_COLOR
  sta zp_TEXT_COLOR
  ; Special note colors (but we'll spruce that up later)
  sta zp_ARG0
  sta zp_ARG1
  sta zp_ARG2
  sta zp_ARG3
  ;bbs 7, FLAGS, @active_print_note
  bbs 7, FLAGS, @active_print_note
@inactive_print_dashes:
  ; Set stride 2
  lda #$20
  sta VERA_addr_high
  lda #PETSCII_DASH
  sta VERA_data0
  sta VERA_data0
  sta VERA_data0
  ; Set stride 1
  lda #$10
  sta VERA_addr_high
  rts
@active_print_note:
  lda NOTE
  sta zp_NOTE_NUMERIC
  phx
  jsr ui::print_note
  plx
  rts
.endproc