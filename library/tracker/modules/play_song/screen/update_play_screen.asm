.proc update_play_screen

@start:
  ; Only update screen if we're in the play song module
  lda zp_CURRENT_MODULE
  cmp #PLAY_SONG_MODULE
  beq @play_song
  rts

@play_song:
  ; Set stride to 1, high bit to 0
  lda #$10
  sta VERA_addr_high

  ldx #$00
  ldy #CHANNEL_Y_START
@vera_channels_loop:
; Print Note / Start Row
  lda #NOTE_X_START
  jsr graphics::drawing::goto_xy
  lda sound::vera::channel_flags,x
  sta FLAGS
  lda sound::vera::channel_note,x
  sta NOTE
  jsr print_note

; Print Inst
; Using part skip_and_print routine here
; because we don't to color the instrument
  jsr skip_two
  lda #INSTRUMENT_COLOR
  sta zp_TEXT_COLOR
  lda sound::vera::channel_instrument,x 
  jsr graphics::printing::print_hex

; Print Vol
@print_volume:
  lda sound::vera::channel_vol,x
  and #%00111111
  jsr skip_and_print_value

; Print Volume Attenuation
@print_att:
  lda sound::vera::channel_attenuation,x
  jsr skip_and_print_value

  jsr print_vera_pan
  jsr print_vera_wave

; Print PWM
@print_pwm:
  lda sound::vera::channel_wave,x
  and #%00111111
  jsr skip_and_print_value

; Print Arp
  lda sound::vera::channel_arp_value,x
  jsr skip_and_print_value

; Print Glide
  lda sound::vera::channel_glide_speed,x
  jsr skip_and_print_value

; Print Pitch Sweep
  lda sound::vera::channel_pitch_sweep_value,x
  jsr skip_and_print_value

; Print Volume Sweep
  lda sound::vera::channel_volume_sweep_value,x
  jsr skip_and_print_value

; Print PWM Sweep
  lda sound::vera::channel_pwm_sweep_value,x
  jsr skip_and_print_value

; Print Vibrato
  lda sound::vera::channel_vibrato_depth,x
  jsr skip_and_print_value

; Print Tremolo
  lda sound::vera::channel_tremolo_depth,x
  jsr skip_and_print_value

; Print Pulsolo
  lda sound::vera::channel_pulsolo_depth,x
  jsr skip_and_print_value

; Print Flags
  ;phx
  ;jsr print_vera_flags
  ;plx

@vera_loop_bottom:
; Loop Bottom
  inx
  iny
  cpx #sound::vera::NUM_CHANNELS
  bne @vera_channels_loop

; FM Loop
  iny
  ldx #$00
@fm_channels_loop:
; Print Note / Start Row
  lda #NOTE_X_START
  jsr graphics::drawing::goto_xy
  lda sound::fm::  channel_flags,x
  sta FLAGS
  lda sound::fm::channel_note,x
  sta NOTE
  jsr print_note

; Print Inst
; Using part skip_and_print routine here
; because we don't to color the instrument
  jsr skip_two
  lda #INSTRUMENT_COLOR
  sta zp_TEXT_COLOR
  lda sound::fm::channel_instrument,x 
  jsr graphics::printing::print_hex

; Print Vol
@print_fm_volume:
  lda sound::fm::channel_volume,x
  jsr skip_and_print_value

; Print Volume Attenuation
@print_fm_att:
  lda sound::fm::channel_attenuation,x
  jsr skip_and_print_value

  jsr print_fm_pan

@print_fm_algo:
  lda sound::fm::channel_pan_feedback_algo,x
  and #%00000111
  jsr skip_and_print_single_value

@fm_loop_bottom:
; Loop Bottom
  inx
  iny
  cpx #sound::fm::NUM_CHANNELS
  bne @fm_channels_loop


; PCM
@print_pcm:
  ldy #PCM_Y_START
  lda #NOTE_X_START
  jsr graphics::drawing::goto_xy
  lda zp_PCM_FLAGS
  sta FLAGS
  lda zp_PCM_SAMPLE_RATE
  sta NOTE
  jsr print_note


  jsr skip_two
  lda #INSTRUMENT_COLOR
  sta zp_TEXT_COLOR
  lda zp_PCM_SAMPLE_NUMBER
  jsr graphics::printing::print_hex

  lda zp_PCM_VOLUME
  and #%00001111
  jsr skip_and_print_value

  ;jsr print_pcm_flags

;@midi_debug:
  ; MIDI Debug
  ;lda #$20
  ;ldy #$01
  ;jsr graphics::drawing::goto_xy
	;ldy #midi::INTERRUPT_IDENT_OFFSET
	;lda (zp_MIDI_BASE),y
  ;jsr graphics::printing::print_hex

  ;lda #$20
  ;ldy #$02
  ;jsr graphics::drawing::goto_xy
	;ldy #midi::INTERRUPT_ENABLE_OFFSET
	;lda (zp_MIDI_BASE),y
  ;jsr graphics::printing::print_hex

  ;lda #$20
  ;ldy #$03
  ;jsr graphics::drawing::goto_xy
	;lda zp_MIDI_DEBUG
  ;jsr graphics::printing::print_hex

@end:
  rts
.endproc


.proc skip_and_print_value
; Helpers
@skip_and_print_value:
  pha
  beq @zero
@non_zero:
  lda #ACTIVE_FLAG_COLOR
  bra @text_color
@zero:
  lda #INACTIVE_FLAG_COLOR
@text_color:
  sta zp_TEXT_COLOR

; Skip 2
  jsr skip_two
  pla
  jsr graphics::printing::print_hex
  rts
.endproc

.proc skip_and_print_single_value
; Helpers
@skip_and_print_value:
  pha
  beq @zero
@non_zero:
  lda #ACTIVE_FLAG_COLOR
  bra @text_color
@zero:
  lda #INACTIVE_FLAG_COLOR
@text_color:
  sta zp_TEXT_COLOR

; Skip 2
  jsr skip_two
  pla
  jsr graphics::printing::print_single_hex
  rts
.endproc


.proc skip_two
; Skip two chars over
@skip_two:
  lda VERA_data0
  lda VERA_data0
  lda VERA_data0
  lda VERA_data0
  rts
.endproc

