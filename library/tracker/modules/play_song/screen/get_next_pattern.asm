.proc get_next_pattern
@get_next_pattern:
	; Load next pattern
	rambank zp_BANK_MISC
	inc zp_SCROLL_ORDER_NUMBER
@load_pattern:
  ldx zp_SCROLL_ORDER_NUMBER
	lda order_list,x
  beq @start_over
	sta zp_SCROLL_PATTERN_NUMBER
	stz zp_SCROLL_ROW_NUMBER
  rts
@start_over:
  stz zp_SCROLL_ORDER_NUMBER
	bra @load_pattern
.endproc