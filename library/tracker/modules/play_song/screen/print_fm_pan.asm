.proc print_fm_pan
@print_fm_pan:
; Skip 2
  lda VERA_data0
  lda VERA_data0
  lda VERA_data0
  lda VERA_data0

  lda sound::fm::channel_pan_feedback_algo,x
  sta VALUE
  bbs 6, VALUE, @pan_left_active
  bra @pan_left_inactive
@pan_left_active:
  lda #SCREENCODE_L
  bra @pan_left_print
@pan_left_inactive:
  lda #SCREENCODE_DASH
@pan_left_print:
  sta VERA_data0
  lda VERA_data0 ; Skip color
@pan_right_check:
  bbs 7, VALUE, @pan_right_active
  bra @pan_right_inactive
@pan_right_active:
  lda #SCREENCODE_R
  bra @pan_right_print
@pan_right_inactive:
  lda #SCREENCODE_DASH
@pan_right_print:
  sta VERA_data0
  lda VERA_data0 ; Skip color
  rts
.endproc