; Before we start playback, we need to pre-fill 
; the 16 rows of the infinite scroll with the 
; notes coming up (noting there could be pattern breaks and things)
;
; Uses:
; zp_TMP0

.proc prime_scroll
	lda #$01          ; Set primary address bank to 1, stride to 0
  sta VERA_addr_high 

	lda #$00
	ldy #$00
	jsr graphics::drawing::goto_xy

	lda zp_ORDER_NUMBER
	sta zp_SCROLL_ORDER_NUMBER
	lda zp_PATTERN_NUMBER
	sta zp_SCROLL_PATTERN_NUMBER
	lda zp_ROW_NUMBER
	sta zp_SCROLL_ROW_NUMBER
	stz zp_SCROLL_ROW_EFFECT_PARAM


	ldy #$00
	; This weird shit to avoid off by one errors when we jump to a 
	; new pattern via an effect
	sta zp_SCROLL_ROW_NUMBER
	dec zp_SCROLL_ROW_NUMBER
@loop:
	inc zp_SCROLL_ROW_NUMBER
	jsr tracker::effects::evaluate_scroll_effect
	ldx zp_SCROLL_ROW_NUMBER
@print_row_number:
	lda #PATTERN_ROW_COLOR
	sta zp_TEXT_COLOR
	; From up above with gotoxy
	txa
	jsr graphics::printing::print_hex
@print_row:
	;tya
	txa
	phy
	;phx
	jsr tracker::get_row
	lda zp_ARG0
	sta zp_SCROLL_ROW_POINTER
	lda zp_ARG0 + 1
	sta zp_SCROLL_ROW_POINTER + 1

	lda #PATTERN_NOTE_COLOR
	sta zp_TEXT_COLOR
	lda #SCROLL_NOTE_COLOR
	sta zp_ARG0
	lda #SCROLL_NOTEREL_COLOR
	sta zp_ARG1
	lda #SCROLL_NOTEOFF_COLOR
	sta zp_ARG2
	lda #SCROLL_NOTENULL_COLOR
	sta zp_ARG3

	jsr print_row

	ply
	; Go down to new row in screen
	; y is our screen relative position
	; x is our row actual position
	; The stx is because evalute_effect may change it
	;stx zp_SCROLL_ROW_NUMBER
	;inx
	; if x has overflowed to the next pattern, reset and load next pattern
	cpx #ROW_MAX
	bne @continue
	jsr get_next_pattern

@continue:
	iny
	lda #$00
	jsr graphics::drawing::goto_xy
	;inx
	;stx zp_SCROLL_ROW_NUMBER
	
	cpy #PATTERN_WINDOW_SIZE
	bne @loop

@end:
	;stx zp_SCROLL_ROW_NUMBER
	inc zp_SCROLL_ROW_NUMBER
	jsr tracker::effects::evaluate_scroll_effect
	lda #PATTERN_WINDOW_SIZE
	sta zp_SCROLL_SCREEN_NUMBER
	rts
.endproc