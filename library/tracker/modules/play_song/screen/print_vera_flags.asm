.proc print_vera_flags

; For each of the flags, change the color as appropriate
; This works by rotating the value left each time and
; checking the carry flag
@print_vera_flags:
  jsr skip_two
  lda sound::vera::channel_flags,x
  sta VALUE
  ldx #$00
@print_flags_loop:
  clc
  rol VALUE
  bcs @print_flags_active
@print_flags_inactive:
  lda #INACTIVE_FLAG_COLOR
  bra @print_flags_color
@print_flags_active:
  lda #ACTIVE_FLAG_COLOR
@print_flags_color:
  pha
  lda VERA_data0  ; skip character
  pla
  sta VERA_data0
@print_flags_loop_bottom:
  inx
  cpx #NUM_VERA_FLAGS
  bne @print_flags_loop
  rts

.endproc