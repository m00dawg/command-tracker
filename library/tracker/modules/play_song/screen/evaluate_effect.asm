; If the row scan found a jump effect, evaluate it
.proc evaluate_effect
	lda zp_SCROLL_ROW_EFFECT_PARAM
	cmp #tracker::effects::PATTERN_BREAK
	beq @pattern_break
	cmp #tracker::effects::JUMP_TO_ORDER
	beq @jump_to_order
	rts

@pattern_break:	
	jsr @get_next_pattern
	stz zp_SCROLL_ROW_EFFECT_PARAM
	stz zp_SCROLL_ROW_EFFECT_VALUE
	;stz zp_SCROLL_ROW_NUMBER
	rts

@jump_to_order:
	rts

	@get_next_pattern:
	; Load next pattern
	rambank zp_BANK_MISC
	inc zp_SCROLL_ORDER_NUMBER
	@load_pattern:
	ldx zp_SCROLL_ORDER_NUMBER
	lda order_list,x
	beq @start_over
	sta zp_SCROLL_PATTERN_NUMBER
	stz zp_SCROLL_ROW_NUMBER
	rts
	@start_over:
	stz zp_SCROLL_ORDER_NUMBER
	bra @load_pattern

.endproc