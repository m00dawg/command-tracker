.proc update_play_screen2

@start:
  ; Only update screen if we're in the play song module
  lda zp_CURRENT_MODULE
  cmp #PLAY_SONG_MODULE
  beq @play_song
  rts

@play_song:
  ; Set stride to 1, high bit to 0
  lda #$10
  sta VERA_addr_high

  ldx #$00
  ldy #CHANNEL_Y_START
@vera_channels_loop:

.endproc

; Inst # in x
; Must run rambank first
.proc print_vera_instrument_name
    rambank zp_BANK_VINS
    lda vera_instrument_names,x
    sta VERA_data0
    inx
    cpx #sound::vera::
.endproc