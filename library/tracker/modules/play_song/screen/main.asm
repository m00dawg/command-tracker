.scope screen

CHANNEL_Y_START = $09
PCM_Y_START = CHANNEL_Y_START + $1A
CHANNEL_X_START = $0B
NOTE_X_START = $09

INSTRUMENT_COLOR = $BF
NOTE_COLOR = $BF
INACTIVE_FLAG_COLOR = $BC
ACTIVE_FLAG_COLOR = $BF

LEFT_COLOR = $CD
RIGHT_COLOR = $CA
DASH_COLOR = $B1

; Note colors
SCROLL_NOTE_COLOR = $01
SCROLL_NOTEREL_COLOR = $08
SCROLL_NOTEOFF_COLOR = $02
SCROLL_NOTENULL_COLOR = $0C
; Vol / Effect colors
SCROLL_VOLUME_COLOR = $0E
SCROLL_EFX_COLOR = $03

NUM_VERA_FLAGS = $05
NUM_PCM_FLAGS = $02
PCM_FLAGS_X = $46
PCM_FLAGS_Y = $26

; Pattern Scroll
PATTERN_ROW_COLOR = $0F
PATTERN_WINDOW_SIZE = $14

VALUE = zp_TMP1
FLAGS = zp_TMP2
NOTE = zp_TMP3

.include "library/tracker/modules/play_song/screen/print_note.asm"
.include "library/tracker/modules/play_song/screen/print_pcm_flags.asm"
.include "library/tracker/modules/play_song/screen/print_vera_flags.asm"
.include "library/tracker/modules/play_song/screen/print_vera_pan.asm"
.include "library/tracker/modules/play_song/screen/print_vera_wave.asm"
.include "library/tracker/modules/play_song/screen/print_fm_pan.asm"
.include "library/tracker/modules/play_song/screen/print_row.asm"
.include "library/tracker/modules/play_song/screen/draw_ui.asm"
.include "library/tracker/modules/play_song/screen/get_next_pattern.asm"
.include "library/tracker/modules/play_song/screen/update_scroll.asm"
;.include "library/tracker/modules/play_song/screen/evaluate_effect.asm"
.include "library/tracker/modules/play_song/screen/prime_scroll.asm"
.include "library/tracker/modules/play_song/screen/update_play_screen.asm"
;.include "library/tracker/modules/play_song/screen/update_play_screen2.asm"







.endscope