.segment "PLAY_SONG_MODULE"

.scope play_song

.word init
.word keyboard_loop

.include "library/tracker/modules/play_song/screen/main.asm"



;PLAY_FILENAME:
;	.byte "scr/play.scr"
;PLAY_FILENAME_LENGTH = $0C

.proc init
@start:
  vera_layer0_16_color
  turn_on_layer_0

  ; Infinite Tracker Scroll
	lda #$FF
	sta VERA_L0_hscroll_h
	lda #$F8
	sta VERA_L0_hscroll_l

  lda #$00
	sta VERA_L0_vscroll_h
	;lda #$C8
  lda #$D0
	sta VERA_L0_vscroll_l
	
  ; Turn off layer 0
  ; Commented out for now until the UI is better suited for 256
  ;lda VERA_dc_video
	;and #%11101111
	;sta VERA_dc_video

  jsr screen::draw_ui

  ;jsr tracker::stop_song
  ; Check current state, if 0, don't remove ISR
  lda zp_STATE
  beq @start_song
  stz zp_KEY_PRESSED

@start_song:
  lda #PLAY_SONG_STATE
  sta zp_STATE

  ldy zp_ORDER_NUMBER
  rambank zp_BANK_MISC
  lda starting_speed
  sta zp_SPEED
  lda order_list,y
  sta RAM_BANK
  sta zp_PATTERN_NUMBER
  jsr screen::prime_scroll

	;lda #$01          ; Set primary address bank to 1, stride to 0
  ;lda #$10
  ;sta VERA_addr_high 
  ;jsr ui::print_header


  ; Prepare for playback
  ;jsr sound::vera::init_channel_registers
  jsr sound::vera::init
  jsr sound::fm::init  
  jsr sound::pcm::init
  ; Init ZSM (if applicable, it will check)
  jsr sound::zsm::init
  jsr tracker::interrupts::play_irq

  ; Turn off activity LED
  ldx #$42 ; System Management Controller
  ldy #$05 ; Activity LED
  lda #$00 ; Turn off
  jsr i2c_write_byte ; Turn off LED
  ; Fall into the next loop
.endproc

.proc keyboard_loop
; Loop on user interaction
@keyboard_loop:
  cli
  jsr ui::print_row_number
  jsr screen::update_play_screen
  ; If the row scan found a jump effect, evaluate it
  jsr screen::update_scroll

  ; Moved to ISR
  ;jsr screen::evaluate_effect

@scan_keys:
  ; call ps2data_fetch
  sei
  lda #$08
  jsr extapi
  jsr SCNKEY
  cli
  ;wai
; Check for input from the keyboard, and do something if
; a key was pressed (return value is something other than 0)
@check_keyboard:
  sei
  jsr GETIN  ;keyboard
  cli
  beq @keyboard_loop
  sta zp_KEY_PRESSED
; Check for stop
@f8_key:
  cmp #F8
  bne @keyboard_loop
  jsr tracker::stop_song
  jmp main_application_loop
@loop:
  ; We don't want to use global keys here as that can mess things up while playing
  ; jmp main_application_loop
  bra keyboard_loop
.endproc

.endscope
