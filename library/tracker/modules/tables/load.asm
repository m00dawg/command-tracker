; Procs to load/draw tables

; Load the selected table from memory
.proc load_table
  stz table_address
  lda #TABLE_BASE_ADDRESS_HIGH
  sta table_address + 1
  add table_address + 1, table_number
  sta table_address + 1
  jsr print_table
  rts
.endproc

; 4 Effects Per Row
; 32 Rows
; x = table_address byte offset
.proc print_table
  ; Switch to lower layer
  lda #%00010001	; Inc set to 1, hi ram
  sta VERA_addr_high

  ldx #$00
  stz effect_count
  stz row_count

  ; Start from 0,0 and scroll to where we want the table
  lda #$00
  ldy #$00
  jsr graphics::drawing::goto_xy

@row_loop:
@effect_loop:
  ; print effect param and value
  lda table_address,x

@effect_loop_bottom:
  inx
  inc effect_count
  lda effect_count
  cmp #EFFECTS_PER_ROW
  bne @effect_loop

@row_loop_bottom:
  ; Move cursor down
  inx
  inc row_count
  lda row_count
  cmp #NUM_ROWS
  bne @row_loop
@end:
  rts
.endproc