.scope cursor

.proc cursor_up
@start:
	jsr ui::cursor_up
	rts
.endproc

.proc cursor_down
@start:
	jsr ui::cursor_down
	rts
.endproc

.proc cursor_left
@start:
	; If we're at min cursor, stop
	lda cursor_x
	cmp #CURSOR_X_MIN
	bne @continue
	rts
@continue:
  jsr ui::cursor_left
	dec current_cursor_effect_position
	jsr check_effect_left_skip
  rts
.endproc

.proc cursor_right
@start:
	; If we're at max cursor, stop
	lda cursor_x
	cmp #CURSOR_X_MAX
	bne @continue
	rts
@continue:
  jsr ui::cursor_right
	inc current_cursor_effect_position
	jsr check_effect_right_skip
  rts
.endproc

; If we are at the end of a current effect, we need to skip over
.proc check_effect_right_skip
	lda current_cursor_effect_position
	cmp #NUM_EFFECT_DIGITS
	beq @continue
	rts
@continue:
  jsr ui::cursor_right
	stz current_cursor_effect_position
	inc current_macro_effect
	rts
.endproc 

; If we are at the beginning of a current effect, we need to skip over
.proc check_effect_left_skip
	lda current_cursor_effect_position
	bmi @continue		; Rollover ($FF)
	rts
@continue:
  jsr ui::cursor_left
	lda #NUM_EFFECT_DIGITS - 1
	sta current_cursor_effect_position
	dec current_macro_effect
	rts
.endproc 

.endscope