.proc save_macros

  ; Preserve VERA state to stack
	lda VERA_addr_high
	pha
	; Skip over colors, lower layer
  lda #$21
  sta VERA_addr_high


	rambank zp_BANK_MISC
	lda #<MACROS_START_ADDRESS
	sta zp_ADDR0
	lda #>MACROS_START_ADDRESS
	sta zp_ADDR0 + 1

	ldy #$00
@row_loop:
	lda #CURSOR_X_MIN
	; y is row
	jsr graphics::drawing::goto_xy
	
	; Prime effect loop
	phy
	ldx #EFFECTS_PER_MACRO
	ldy #$00
@effect_loop:
	; Effect
	lda VERA_data0
	sta r0
	lda VERA_data0
	sta r1
	jsr graphics::printing::chars_to_number
	sta (zp_ADDR0),y
	iny
	
	; Parameter
	lda VERA_data0
	sta r0
	lda VERA_data0
	sta r1
	jsr graphics::printing::chars_to_number
	sta (zp_ADDR0),y
	iny

	; Skip blank
	lda VERA_data0

	dex
	bne @effect_loop
@effect_loop_bottom:
	; Add 16 to address (as that is how many times we looped up above)
	clc
	lda zp_ADDR0
	adc #$10
	sta zp_ADDR0
	lda zp_ADDR0+1
	adc #$00
	sta zp_ADDR0+1
	ply
@row_loop_bottom:
	iny
	cpy #NUM_MACROS
	bne @row_loop

@end:
	; Return VERA state
	pla
	sta VERA_addr_high
	rts


.endproc