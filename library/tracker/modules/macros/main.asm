

.scope macros

.segment "GOLDEN"

; Golden Variables
; These need to be preserved across module changes
current_macro_number: .res $01		; Current selected macro number
current_macro_effect:	.res $01		; Current effect column of selected macro (0-7)

.segment "MACROS_MODULE"

.word init
.word keyboard_loop

; Local Variables
current_cursor_effect_position: .res $01		; Current cursor of currect effect

; Constants
NUM_EFFECTS = $08
NUM_EFFECT_DIGITS = $04
NUM_DISPLAY_ROWS = $1F
CURSOR_X_MIN = $03
CURSOR_X_MAX = $29

.include "library/tracker/modules/macros/draw_ui.asm"
.include "library/tracker/modules/macros/load_macros.asm"
.include "library/tracker/modules/macros/save_macros.asm"
.include "library/tracker/modules/macros/cursor.asm"


.proc init
	stz current_cursor_effect_position
	jsr draw_ui
	jsr load_macros

	; Init Cursor
	; Set VERA Stride and bank
  lda #$11          ; Set primary address bank to 1 (lower layer), stride to 1
  sta VERA_addr_high 
  lda #$01
  sta cursor_layer
	jsr graphics::drawing::cursor_unplot
	lda #CURSOR_X_MIN
	sta cursor_x
	stz cursor_y
	jsr graphics::drawing::cursor_plot
	; Fall to keyboard loop
.endproc

.proc keyboard_loop
	lda zp_KEY_PRESSED
	bne @keyboard
	jmp main_application_loop

@keyboard:
	cmp #KEY_UP
	beq @cursor_up_jump
	cmp #KEY_DOWN
	beq @cursor_down_jump
	cmp #KEY_LEFT
	beq @cursor_left_jump
	cmp #KEY_RIGHT
	beq @cursor_right_jump
	cmp #KEY_HOME
	beq @home_jump

	; Rest of the stuff
	cmp #$2E    ; Alphanumeric, period, and a few things we don't want
	bpl @print_alphanumeric_jump
	jmp main_application_loop

@cursor_up_jump:
	jsr cursor::cursor_up
	jmp main_application_loop
@cursor_down_jump:
	jsr cursor::cursor_down
	jmp main_application_loop
@cursor_left_jump:
	jsr cursor::cursor_left
	jmp main_application_loop
@cursor_right_jump:
	jsr cursor::cursor_right
	jmp main_application_loop
@print_alphanumeric_jump:
	jsr print_alphanumeric
	jmp main_application_loop
@home_jump:
	jsr graphics::drawing::cursor_unplot
	lda #CURSOR_X_MIN
	sta cursor_x
	jsr graphics::drawing::cursor_plot
	jmp main_application_loop
.endproc

.proc print_alphanumeric
	pha
	lda cursor_x
  ldy cursor_y
  jsr graphics::drawing::goto_xy
  pla
  cmp #PETSCII_AT
  bmi @print_numeric
  cmp #PETSCII_G  ; Do not print invalid chars
  bpl @print_end
@print_alpha:
  sbc #$3F  ; add 40 to get to the letter screencodes, then fall through
@print_numeric:
  sta VERA_data0
  inc VERA_addr_low
  lda #EDITABLE_TEXT_COLORS
  sta VERA_data0
	jsr cursor::cursor_right
	jsr save_macros
@print_end:
	rts
.endproc

.endscope

