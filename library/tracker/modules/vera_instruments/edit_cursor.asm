; Cursors for the instrument edit section

.proc instrument_edit_cursor_up
    stz numerical_cursor_position
; If we're on the volume channel and we
; press up, go to left (-2)
    lda instrument_edit_pos
    cmp #INSTRUMENT_EDIT_VOLUME
    bne @decrement
    dec instrument_edit_pos
; Otherwise just increment until min
@decrement:
    cmp #INSTRUMENT_EDIT_LEFT
    beq @end
    dec instrument_edit_pos
@end:
    jsr jump_to_instrument_edit_position
    rts
.endproc

.proc instrument_edit_cursor_down
    stz numerical_cursor_position
; If we on the left channel and we press down,
; we need to go to volume (+2)
    lda instrument_edit_pos
    cmp #INSTRUMENT_EDIT_LEFT
    bne @increment
    inc instrument_edit_pos
; Otherwise just increment until max
@increment:
    cmp #INSTRUMENT_EDIT_PITCH_ENV_END
    beq @end
    inc instrument_edit_pos
@end:
    jsr jump_to_instrument_edit_position
    rts
.endproc

.proc instrument_edit_cursor_left
    stz numerical_cursor_position
; If we are on right, -1 (which move to left)
    lda instrument_edit_pos
    cmp #INSTRUMENT_EDIT_RIGHT
    bne @env_left
    dec instrument_edit_pos
    jmp @end
@env_left:
; If we are on an envelope, move -7
; unless we're already on volume env
; or any of the main instrument edit options
    cmp #INSTRUMENT_EDIT_VOL_ENV_END
    ;bcc @end
    blt @end
    sub instrument_edit_pos, #INSTRUMENT_ENV_OFFSET
    sta instrument_edit_pos
@end:
    jsr jump_to_instrument_edit_position
    rts    
.endproc

.proc instrument_edit_cursor_right
    stz numerical_cursor_position
; If we are on left, +1 (which moves to right)
    lda instrument_edit_pos
    cmp #INSTRUMENT_EDIT_LEFT
    bne @env_right
    inc instrument_edit_pos
    jmp @end
@env_right:
; If we are on an envelope, move +7
; unless we're already on pitch
    cmp #INSTRUMENT_EDIT_PITCH_ENV_ENABLE
    bcs @end
    cmp #INSTRUMENT_EDIT_VOL_ENV_ENABLE
    bcc @end
    add instrument_edit_pos,#INSTRUMENT_ENV_OFFSET
    sta instrument_edit_pos
@end:
    jsr jump_to_instrument_edit_position
    rts
.endproc

.proc jump_to_instrument_edit_position
    jsr graphics::drawing::cursor_unplot
    ldx instrument_edit_pos
    lda instrument_edit_x_pos_lut,x
    ldy instrument_edit_y_pos_lut,x
    sta cursor_x
    sty cursor_y
    jsr graphics::drawing::goto_xy
    jsr graphics::drawing::cursor_plot
    rts
.endproc