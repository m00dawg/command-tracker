.segment "VINST_MODULE"

.scope vera_instruments

.word init
.word keyboard_loop


.include "library/tracker/modules/vera_instruments/load_instrument.asm"
.include "library/tracker/modules/vera_instruments/cursor.asm"
.include "library/tracker/modules/vera_instruments/edit_instrument.asm"
.include "library/tracker/modules/vera_instruments/draw_ui.asm"

; Scope Constants
; Positions
INSTRUMENT_NAMES_START_X = $03
INSTRUMENT_NAMES_START_Y = $0D
LEFT_X = $29
LEFT_Y = $0D
RIGHT_X = $2A
RIGHT_Y = $0D
VOLUME_X  = $29
VOLUME_Y  = $0E
WAVEFORM_X  = $29
WAVEFORM_Y  = $0F
PWM_X  = $29
PWM_Y  = $10
FINE_PITCH_X  = $29
FINE_PITCH_Y  = $11
PITCH_SPEED_X  = $29
PITCH_SPEED_Y  = $12

; Env X Positions (shared across all envs)
VOLUME_ENV_X = $23
WAVE_ENV_X = $30
PITCH_ENV_X = $3D

; Env Y Positions (shared across all envs)
ENV_ENABLE_Y = $23
ENV_NUMBER_Y = $24
ENV_SPEED_Y = $25
ENV_LENGTH_Y = $26
ENV_LOOP_ENABLE_Y = $28
ENV_LOOP_MODE_Y = $29
ENV_LOOP_START_Y = $2A
ENV_LOOP_END_Y = $2B

; Colors
INSTRUMENT_TYPE_ACTIVE_COLOR = $B1
INSTRUMENT_TYPE_INACTIVE_COLOR = $BC
INSTRUMENT_TEXT_COLOR = $01
INSTRUMENT_ACTIVE_COLOR = $01
INSTRUMENT_INACTIVE_COLOR = $0E
VOLUME_COLOR = $9E
FINE_PITCH_COLOR = $9E
PITCH_SPEED_COLOR = $9E
WAVEFORM_COLOR = $9E
PWM_COLOR = $9E

ENV_NUMBER_COLOR = $9E
ENV_LENGTH_COLOR = $9E
ENV_ENABLE_COLOR = $9D
ENV_DISABLE_COLOR = $9B
ENV_LOOP_MODE_COLOR = $9E
ENV_LOOP_START_COLOR = $9E
ENV_LOOP_END_COLOR = $9E

; Instrument Edit Positions
INSTRUMENT_EDIT_LEFT = $00
INSTRUMENT_EDIT_RIGHT = $01
INSTRUMENT_EDIT_VOLUME = $02
INSTRUMENT_EDIT_WAVE = $03
INSTRUMENT_EDIT_PWM = $04
INSTRUMENT_EDIT_FINE_PITCH = $05
INSTRUMENT_EDIT_PITCH_SPEED = $06
INSTRUMENT_EDIT_VOL_ENV_ENABLE = $07
INSTRUMENT_EDIT_VOL_ENV_NUMBER = $08 
INSTRUMENT_EDIT_VOL_ENV_SPEED = $09
INSTRUMENT_EDIT_VOL_ENV_LENGTH = $0A
INSTRUMENT_EDIT_VOL_ENV_LOOP_ENABLE = $0B
INSTRUMENT_EDIT_VOL_ENV_LOOP_MODE = $0C
INSTRUMENT_EDIT_VOL_ENV_START = $0D
INSTRUMENT_EDIT_VOL_ENV_END = $0E
INSTRUMENT_EDIT_WAVE_ENV_ENABLE = $0F
INSTRUMENT_EDIT_WAVE_ENV_NUMBER = $10 
INSTRUMENT_EDIT_WAVE_ENV_SPEED = $11
INSTRUMENT_EDIT_WAVE_ENV_LENGTH = $12
INSTRUMENT_EDIT_WAVE_ENV_LOOP_ENABLE = $13
INSTRUMENT_EDIT_WAVE_ENV_LOOP_MODE = $14
INSTRUMENT_EDIT_WAVE_ENV_START = $15
INSTRUMENT_EDIT_WAVE_ENV_END = $16
INSTRUMENT_EDIT_PITCH_ENV_ENABLE = $17
INSTRUMENT_EDIT_PITCH_ENV_NUMBER = $18 
INSTRUMENT_EDIT_PITCH_ENV_SPEED = $19
INSTRUMENT_EDIT_PITCH_ENV_LENGTH = $1A
INSTRUMENT_EDIT_PITCH_ENV_LOOP_ENABLE = $1B
INSTRUMENT_EDIT_PITCH_ENV_LOOP_MODE = $1C
INSTRUMENT_EDIT_PITCH_ENV_START = $1D
INSTRUMENT_EDIT_PITCH_ENV_END = $1E

; How many env options there are so we know how 
; many to skip over when moving left or right
; within the envelope section
INSTRUMENT_ENV_OFFSET = $08
;INSTRUMENT_ENV_OFFSET = $07

; Scope Variables
edit_mode = zp_TMP0
;pan_temp = zp_TMP1
instrument_edit_pos = zp_TMP3
numerical_mask = zp_TMP4
numerical_cursor_position = zp_TMP5

; Calling Args for ui::instruments::list_names
instrument_base_address = zp_ARG0
text_color              = zp_ARG1
number_color            = zp_ARG2
num_instruments         = zp_ARG3
edit_start_x            = zp_ARG4
edit_start_y            = zp_ARG5

; Calling Args for ui::instruments::set_pan
left_x                  = zp_ARG6
left_y                  = zp_ARG7
pan                     = zp_ARG8

; Calling args for ui::instruments::print_flag
enable_color            = zp_ARG6
disable_color           = zp_ARG7
flag                    = zp_ARG8


; Locations where instrument positions are
; (for cursor hopping)
instrument_edit_x_pos_lut:
  ; Top Part (L, R, Vol, Wave, Width, Fine, Pitch
  .byte $29,$2A,$29,$29,$29,$29,$29
  ; Volume Env
  .byte $23,$23,$23,$23,$23,$23,$23,$23
  ; Wave Env
  .byte $30,$30,$30,$30,$30,$30,$30,$30
  ; Pitch Env
  .byte $3D,$3D,$3D,$3D,$3D,$3D,$3D,$3D
instrument_edit_y_pos_lut:
  ; Top Part (L, R, Vol, Wave, Width, Fine, Pitch
  .byte $0D,$0D,$0E,$0F,$10,$11,$12
  ; Volume Env
  .byte $23,$24,$25,$26,$28,$29,$2A,$2B
  ; Wave Env
  .byte $23,$24,$25,$26,$28,$29,$2A,$2B
  ; Pitch Env
  .byte $23,$24,$25,$26,$28,$29,$2A,$2B

.proc init
@start:
	;lda #VERA_INSTRUMENTS_FILENAME_LENGTH
	;ldx #<VERA_INSTRUMENTS_FILENAME
	;ldy #>VERA_INSTRUMENTS_FILENAME
	;jsr files::load_to_vram
  
  jsr draw_ui

  rambank zp_BANK_VINS

  lda #%00010001	; Inc set to 1, hi ram
  sta VERA_addr_high
  turn_on_layer_0
  vera_layer0_16_color

  ; Load a buncha stuff for the ui::instruments routines
  lda #<sound::vera::BASE_NAMES_ADDRESS
  sta instrument_base_address
  lda #>sound::vera::BASE_NAMES_ADDRESS
  sta instrument_base_address + 1
  lda #INSTRUMENT_TEXT_COLOR
  sta text_color
  lda #INSTRUMENT_INACTIVE_COLOR
  sta number_color
  lda #sound::vera::NUM_INSTRUMENTS
  sta num_instruments
  lda #LEFT_X
  sta edit_start_x
  lda #LEFT_Y
  sta edit_start_y
  jsr ui::instruments::list_names

  stz zp_INSTRUMENT
  stz edit_mode
  stz instrument_edit_pos
  jsr load_instrument

  jsr ui::instruments::cursor::reset_on_names

  stz zp_KEY_PRESSED
.endproc

.proc keyboard_loop
keyboard_loop:
  lda zp_KEY_PRESSED
  cmp #KEY_UP
  beq @cursor_up_jump
  cmp #KEY_DOWN
  beq @cursor_down_jump
  cmp #KEY_LEFT
  beq @cursor_left_jump
  cmp #KEY_RIGHT
  beq @cursor_right_jump
  cmp #KEY_TAB
  beq @change_edit_mode
  cmp #SPACE
  beq @spacebar_jump
  ; Start at period
  cmp #PETSCII_PERIOD
  bpl @print_characters_jump
  jmp main_application_loop

; Cusor Up
@cursor_up_jump:
  jsr cursor::cursor_up
  jmp main_application_loop

; Cursor Down
@cursor_down_jump:
  jsr cursor::cursor_down
  jmp main_application_loop

; Cursor Left
@cursor_left_jump:
  jsr cursor::cursor_left
  jmp main_application_loop

; Cursor Right
@cursor_right_jump:
  jsr cursor::cursor_right
  jmp main_application_loop

@spacebar_jump:
  jmp spacebar

@print_characters_jump:
  tax
  lda edit_mode
  bne @print_characters_jump_instrument_edit
  txa
  jsr print_character
  jmp main_application_loop
@print_characters_jump_instrument_edit:  
  jsr edit_instrument_value
  jmp main_application_loop


@change_edit_mode:
  lda edit_mode
  bne @instrument_names
@instrument_settings:
  inc edit_mode
  jsr ui::instruments::cursor::reset_on_edit
  jmp @change_edit_mode_end
@instrument_names:
  stz edit_mode
  stz instrument_edit_pos
  jsr ui::instruments::cursor::reset_on_names
@change_edit_mode_end:
  jmp main_application_loop
.endproc

; If we pressed space in instrument name mode, add a space
; otherwise the behavior is dependent on where we are 
; in the instrument edit screen
.proc spacebar
  tax
  lda edit_mode
  bne @instrument_edit
  jmp @spacebar_print_character
@instrument_edit:
  jsr toggle_instrument_flag
  jmp @spacebar_end
@spacebar_print_character:
  txa  
  jsr print_character
@spacebar_end:
  jmp main_application_loop
.endproc

.proc print_character
  jsr ui::instruments::print_character
  lda #<sound::vera::BASE_NAMES_ADDRESS
  sta instrument_base_address
  lda #>sound::vera::BASE_NAMES_ADDRESS
  sta instrument_base_address + 1
  jsr ui::instruments::save_names
@print_end:
  rts
.endproc




.endscope

