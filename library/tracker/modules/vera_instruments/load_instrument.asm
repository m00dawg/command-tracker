; Load instrument into right hand view

	; Variables
	; See main.asm

.proc load_instrument
@start:
	lda #%00010000	; Inc set to 1, lo ram
	sta VERA_addr_high
	ldx zp_INSTRUMENT
	lda #LEFT_X
	sta left_x
	lda #LEFT_Y
	sta left_y
	lda vera_instruments_vol,x
	sta pan
	jsr ui::instruments::set_pan

@set_vol:
	lda #VOLUME_X
	ldy #VOLUME_Y
	jsr graphics::drawing::goto_xy
	lda #VOLUME_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_vol,x
	and #%00111111
	jsr graphics::printing::print_hex

@set_fine_pitch:
	lda #FINE_PITCH_X
	ldy #FINE_PITCH_Y
	jsr graphics::drawing::goto_xy
	lda #FINE_PITCH_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_fine_pitch,x
	jsr graphics::printing::print_hex

@set_pitch_speed:
	lda #PITCH_SPEED_X
	ldy #PITCH_SPEED_Y
	jsr graphics::drawing::goto_xy
	lda #PITCH_SPEED_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_pitch_speed,x
	jsr graphics::printing::print_single_hex

@set_waveform:
	lda #WAVEFORM_X
	ldy #WAVEFORM_Y
  jsr graphics::drawing::goto_xy
	lda #WAVEFORM_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_wave,x
	and #%11000000
	cmp #PULSE_TYPE
	beq @set_waveform_pulse
	cmp #SAW_TYPE
	beq @set_waveform_saw
	cmp #TRI_TYPE
	beq @set_waveform_tri
@set_waveform_noise:
	lda #SCREENCODE_N
	jmp @set_waveform_print
@set_waveform_pulse:
	lda #SCREENCODE_P
	jmp @set_waveform_print
@set_waveform_saw:
	lda #SCREENCODE_S
	jmp @set_waveform_print
@set_waveform_tri:
	lda #SCREENCODE_T
@set_waveform_print:
	jsr graphics::printing::print_alpha_char

@set_pwm:
	lda #PWM_X
	ldy #PWM_Y
  	jsr graphics::drawing::goto_xy
	lda #PWM_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_wave,x
	and #%00111111
	jsr graphics::printing::print_hex

@volume_env:
	; Flags and Modes
	; Env Enable
	lda vera_instruments_vol_envelope_flags,x
	and #%10000000
	sta flag
	lda #VOLUME_ENV_X
	ldy #ENV_ENABLE_Y
	jsr print_flag
	; Loop Enable
	lda vera_instruments_vol_envelope_flags,x
	and #%01000000
	sta flag
	lda #VOLUME_ENV_X
	ldy #ENV_LOOP_ENABLE_Y
	jsr print_flag
	; Env Loop Mode
	lda vera_instruments_vol_envelope_flags,x
	and #%00100000
	sta flag
	lda #VOLUME_ENV_X
	ldy #ENV_LOOP_MODE_Y
	jsr print_loop_mode
	; Env Number
	lda #VOLUME_ENV_X
	ldy #ENV_NUMBER_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_NUMBER_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_vol_envelope_num,x
	jsr graphics::printing::print_hex
	; Env Speed
	lda #VOLUME_ENV_X
	ldy #ENV_SPEED_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_NUMBER_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_vol_envelope_speed,x
	jsr graphics::printing::print_hex
	; Env Length
	lda #VOLUME_ENV_X
	ldy #ENV_LENGTH_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_LENGTH_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_vol_envelope_length,x
	jsr graphics::printing::print_hex
	; Env Loop Start
	lda #VOLUME_ENV_X
	ldy #ENV_LOOP_START_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_LOOP_START_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_vol_envelope_loop_start,x
	jsr graphics::printing::print_hex
	; Env Loop End
	lda #VOLUME_ENV_X
	ldy #ENV_LOOP_END_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_LOOP_END_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_vol_envelope_loop_end,x
	jsr graphics::printing::print_hex

; Wave Env
@wave_env:
	; Flags and Modes
	; Env Enable
	lda vera_instruments_wave_envelope_flags,x
	and #%10000000
	sta flag
	lda #WAVE_ENV_X
	ldy #ENV_ENABLE_Y
	jsr print_flag
	; Loop Enable
	lda vera_instruments_wave_envelope_flags,x
	and #%01000000
	sta flag
	lda #WAVE_ENV_X
	ldy #ENV_LOOP_ENABLE_Y
	jsr print_flag
	; Env Loop Mode
	lda vera_instruments_wave_envelope_flags,x
	and #%00100000
	sta flag
	lda #WAVE_ENV_X
	ldy #ENV_LOOP_MODE_Y
	jsr print_loop_mode
	; Env Number
	lda #WAVE_ENV_X
	ldy #ENV_NUMBER_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_NUMBER_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_wave_envelope_num,x
	jsr graphics::printing::print_hex
	; Env Speed
	lda #WAVE_ENV_X
	ldy #ENV_SPEED_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_NUMBER_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_wave_envelope_speed,x
	jsr graphics::printing::print_hex
	; Env Length
	lda #WAVE_ENV_X
	ldy #ENV_LENGTH_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_LENGTH_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_wave_envelope_length,x
	jsr graphics::printing::print_hex
	; Env Loop Start
	lda #WAVE_ENV_X
	ldy #ENV_LOOP_START_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_LOOP_START_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_wave_envelope_loop_start,x
	jsr graphics::printing::print_hex
	; Env Loop End
	lda #WAVE_ENV_X
	ldy #ENV_LOOP_END_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_LOOP_END_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_wave_envelope_loop_end,x
	jsr graphics::printing::print_hex

; Pitch Env
@pitch_env:
	; Flags and Modes
	; Env Enable
	lda vera_instruments_pitch_envelope_flags,x
	and #%10000000
	sta flag
	lda #PITCH_ENV_X
	ldy #ENV_ENABLE_Y
	jsr print_flag
	; Loop Enable
	lda vera_instruments_pitch_envelope_flags,x
	and #%01000000
	sta flag
	lda #PITCH_ENV_X
	ldy #ENV_LOOP_ENABLE_Y
	jsr print_flag
	; Env Loop Mode
	lda vera_instruments_pitch_envelope_flags,x
	and #%00100000
	sta flag
	lda #PITCH_ENV_X
	ldy #ENV_LOOP_MODE_Y
	jsr print_loop_mode
	; Env Number
	lda #PITCH_ENV_X
	ldy #ENV_NUMBER_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_NUMBER_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_pitch_envelope_num,x
	jsr graphics::printing::print_hex
	; Env Speed
	lda #PITCH_ENV_X
	ldy #ENV_SPEED_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_NUMBER_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_pitch_envelope_speed,x
	jsr graphics::printing::print_hex
	; Env Length
	lda #PITCH_ENV_X
	ldy #ENV_LENGTH_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_LENGTH_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_pitch_envelope_length,x
	jsr graphics::printing::print_hex
	; Env Loop Start
	lda #PITCH_ENV_X
	ldy #ENV_LOOP_START_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_LOOP_START_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_pitch_envelope_loop_start,x
	jsr graphics::printing::print_hex
	; Env Loop End
	lda #PITCH_ENV_X
	ldy #ENV_LOOP_END_Y
  jsr graphics::drawing::goto_xy
	lda #ENV_LOOP_END_COLOR
	sta zp_TEXT_COLOR
	lda vera_instruments_pitch_envelope_loop_end,x
	jsr graphics::printing::print_hex

@end:
	lda #%00010001	; Inc set to 1, hi ram
  	sta VERA_addr_high
	rts
.endproc

; a = x-position
; y = y-position
; flag = flag (zero vs non-zero)
.proc print_flag
@start:
	pha
	lda #ENV_ENABLE_COLOR
	sta enable_color
	lda #ENV_DISABLE_COLOR
	sta disable_color
	pla
	jsr ui::instruments::print_flag
	rts
.endproc


.proc print_loop_mode
@start:
	jsr graphics::drawing::goto_xy
	lda #ENV_LOOP_MODE_COLOR
	sta zp_TEXT_COLOR
	lda flag
	beq @loop_mode_forward
@loop_mode_ping_pong:
	lda #SCREENCODE_P
	jmp @loop_mode_print
@loop_mode_forward:
	lda #SCREENCODE_F
@loop_mode_print:
	jsr graphics::printing::print_alpha_char
@flag_end:
	rts
.endproc
