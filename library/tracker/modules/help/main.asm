.segment "HELP_MODULE"

.scope help

.word init
.word keyboard_loop

.proc init
	vera_layer0_16_color

  ; Set VERA Stride and bank
  lda #$10          ; Set primary address bank to 0, stride to 1
  sta VERA_addr_high 

	; Draw Frame
	lda #$18
	sta zp_ARG0	;X1
	lda #$0F
	sta zp_ARG1	;Y1
	;lda #$4F		;X2
	lda #$34
	sta zp_ARG2
	;lda #$3B		;Y2
	lda #$26
	sta zp_ARG3
	lda #FRAME_COLOR
	sta zp_ARG4
	jsr graphics::drawing::draw_rounded_box

	; Clear inner frame
	lda #$19
	sta zp_ARG0
	lda #$10
	sta zp_ARG1
	lda #$33
	sta zp_ARG2
	lda #$25
	sta zp_ARG3
	lda #$B1
	sta zp_ARG4
	jsr graphics::drawing::draw_solid_box

	stz zp_KEY_PRESSED
	ldx help_submodule
	jmp (help_submodule_jump_table,x)
.endproc

.proc keyboard_loop
	jmp main_application_loop
.endproc

.proc main_help
	print_string_macro modules
	jmp main_application_loop
.endproc


help_submodule_jump_table:
  .word main_help

modules: 
	.byte SCREENCODE_XY,$19,$10
  .byte SCREENCODE_COLOR,$B1,"modules",SCREENCODE_RETURN
	.byte SCREENCODE_COLOR,$B1,"   f1",SCREENCODE_COLOR,$B6,": ",SCREENCODE_COLOR,$B7 ,"help",SCREENCODE_RETURN
	.byte SCREENCODE_COLOR,$B1,"   f2",SCREENCODE_COLOR,$B6,": ",SCREENCODE_COLOR,$B7 ,"edit pattern",SCREENCODE_RETURN
	.byte SCREENCODE_COLOR,$B1,"   f3",SCREENCODE_COLOR,$B6,": ",SCREENCODE_COLOR,$B7 ,"samples",SCREENCODE_RETURN
	.byte SCREENCODE_COLOR,$B1,"   f4",SCREENCODE_COLOR,$B6,": ",SCREENCODE_COLOR,$B7 ,"instruments",SCREENCODE_RETURN
	.byte SCREENCODE_COLOR,$B1,"   f5",SCREENCODE_COLOR,$B6,": ",SCREENCODE_COLOR,$B7 ,"play song",SCREENCODE_RETURN
	.byte SCREENCODE_COLOR,$B1,"   f6",SCREENCODE_COLOR,$B6,": ",SCREENCODE_COLOR,$B7 ,"play pattern",SCREENCODE_RETURN
	.byte SCREENCODE_COLOR,$B1,"   f7",SCREENCODE_COLOR,$B6,": ",SCREENCODE_COLOR,$B7 ,"play song at row",SCREENCODE_RETURN
	.byte SCREENCODE_COLOR,$B1,"   f8",SCREENCODE_COLOR,$B6,": ",SCREENCODE_COLOR,$B7 ,"stop song",SCREENCODE_RETURN
	.byte SCREENCODE_COLOR,$B1,"   f9",SCREENCODE_COLOR,$B6,": ",SCREENCODE_COLOR,$B7 ,"envelopes/macros",SCREENCODE_RETURN
	.byte SCREENCODE_COLOR,$B1,"  f10",SCREENCODE_COLOR,$B6,": ",SCREENCODE_COLOR,$B7 ,"file",SCREENCODE_RETURN
	.byte SCREENCODE_COLOR,$B1,"  f11",SCREENCODE_COLOR,$B6,": ",SCREENCODE_COLOR,$B7 ,"orders",SCREENCODE_RETURN
	.byte SCREENCODE_COLOR,$B1,"  f12",SCREENCODE_COLOR,$B6,": ",SCREENCODE_COLOR,$B7 ,"song options",SCREENCODE_RETURN
	.byte SCREENCODE_COLOR,$B1,"alt-q",SCREENCODE_COLOR,$B6,": ",SCREENCODE_COLOR,$B7 ,"exit",SCREENCODE_RETURN
	.byte 0



.endscope

