; Print the top header which most screens in DT use
.proc print_header
	print_string_macro_position_color song_label, #SONG_TITLE_LABEL_X, #SONG_TITLE_LABEL_Y, #HEADER_COLOR
	print_string_macro_position_color info_label, #ORD_OCT_LABEL_X, #ORD_OCT_LABEL_Y, #HEADER_COLOR
	rts
	song_label: 
		.byte "song:",SCREENCODE_RETURN
		.byte "by:",0
	info_label:
		.byte "ord:   oct:",SCREENCODE_RETURN
		.byte "pat:   spd:",SCREENCODE_RETURN
		.byte "row:   bpm:",0

.endproc