.proc print_row_number
print_row_number:
  ; Set stride to 1, high bit to 0
  lda #$10
  sta VERA_addr_high

  lda #ROW_DISPLAY_X
  ldy #ROW_DISPLAY_Y
  jsr graphics::drawing::goto_xy

  ; Color
  set_text_color #TEXT_COLORS
  lda zp_ROW_NUMBER       ; Get the current row conunt
  jsr graphics::printing::print_hex
  rts
.endproc
