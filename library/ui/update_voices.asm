; Update the voices display 
; Currently only VERA

; Voice Activity Display
VOICE_CHARACTER = SCREENCODE_DIAMOND
PSG_VOICE_DISPLAY_X = $3F
PSG_VOICE_DISPLAY_Y = $01
PCM_VOICE_DISPLAY_X = $4C
PCM_VOICE_DISPLAY_Y = $02
VOICE_ACTIVE_COLOR = $BD
VOICE_INACTIVE_COLOR = $B5

.proc update_voices
  POSITION = zp_TMP0
  FLAGS = zp_TMP1
@start:
  lda #$10
  sta VERA_addr_high

  lda #PSG_VOICE_DISPLAY_X
  sta POSITION

  ldx #$00
@psg_loop:
  lda POSITION
  ldy #PSG_VOICE_DISPLAY_Y
  jsr graphics::drawing::goto_xy
  lda sound::vera::channel_flags,x
  sta FLAGS
  bbs 7, FLAGS,@active
@inactive:
  ldy #VOICE_INACTIVE_COLOR
  sty zp_TEXT_COLOR
  jmp @output
@active:
  ldy #VOICE_ACTIVE_COLOR
  sty zp_TEXT_COLOR
@output:
  lda #VOICE_CHARACTER
  sta VERA_data0
  lda zp_TEXT_COLOR
  sta VERA_data0
  inc POSITION
  inx
  cpx #sound::vera::NUM_CHANNELS
  bne @psg_loop

@pcm:
  lda #PCM_VOICE_DISPLAY_X
  ldy #PCM_VOICE_DISPLAY_Y
  jsr graphics::drawing::goto_xy
  bbs 7, zp_PCM_FLAGS, @pcm_active
@pcm_inactive:
  ldy #VOICE_INACTIVE_COLOR
  sty zp_TEXT_COLOR
  bra @pcm_output
@pcm_active:
  ldy #VOICE_ACTIVE_COLOR
  sty zp_TEXT_COLOR
@pcm_output:
  lda #VOICE_CHARACTER
  sta VERA_data0
  lda zp_TEXT_COLOR
  sta VERA_data0
@end:
  rts
.endproc
