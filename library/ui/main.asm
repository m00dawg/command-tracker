; Graphic routines

.scope ui

  ; Label Positions
  ; Left Side
  SONG_TITLE_LABEL_X = $01
  SONG_TITLE_LABEL_Y = $01
  SONG_TITLE_X = $06
  SONG_TITLE_Y = $01

  COMPOSER_LABEL_X = $01
  COMPOSER_LABEL_Y = $02
  COMPOSER_X = $06
  COMPOSER_Y = $02
  ; Right Side
  ORD_OCT_LABEL_X = $2C
  ORD_OCT_LABEL_Y = $01
  PAT_SPD_LABEL_X = $2C
  PAT_SPD_LABEL_Y = $02
  ROW_BPM_LABEL_X = $2C
  ROW_BPM_LABEL_Y = $03

  ORDER_DISPLAY_X = $30
  ORDER_DISPLAY_Y = $01
  PATTERN_DISPLAY_X = $30
  PATTERN_DISPLAY_Y = $02
  ROW_DISPLAY_X = $30
  ROW_DISPLAY_Y = $03
  OCTAVE_DISPLAY_X = $37
  OCTAVE_DISPLAY_Y = $01
  SPEED_DISPLAY_X = $37
  SPEED_DISPLAY_Y = $02
  BPM_DISPLAY_X = $37
  BPM_DISPLAY_Y = $03

  ; Colors
  HEADER_COLOR = $B1


  ; Version
  ;dream_tracker_string: .byte "dream tracker v0.200",0

  ; Top Header Labels
  ; more_channels_label: .byte ">>>",0

  ; Order List Page Labels:
  ;order_list_label: .byte "orders:",0
  ;goto_pattern_message: .byte "press g to edit pattern",0
 

  ; includes
  .include "library/ui/scroll.asm"
  .include "library/ui/print_header.asm"
  ; .include "library/ui/draw_save_frame.asm"
  .include "library/ui/print_song_info.asm"
  .include "library/ui/print_current_pattern_number.asm"
  ;.include "library/ui/print_current_instrument.asm"
  .include "library/ui/print_current_octave.asm"
  ;.include "library/ui/print_number_of_orders.asm"
  .include "library/ui/print_current_order.asm"
  .include "library/ui/print_speed.asm"
  ;.include "library/ui/print_bpm.asm"
  .include "library/ui/print_row_number.asm"
  ;.include "library/ui/print_pattern.asm"
  ;.include "library/ui/print_playing_pattern.asm"
  ;.include "library/ui/print_edit_pattern.asm"
  .include "library/ui/print_note.asm"
  ;.include "library/ui/scroll_pattern.asm"
  ;.include "library/ui/update_play_screen/main.asm"

  .include "library/ui/update_upper_info.asm"
  ;.include "library/ui/update_voices.asm"

  .include "library/ui/move_cursor.asm"

  .include "library/ui/instruments/main.asm"


.endscope

