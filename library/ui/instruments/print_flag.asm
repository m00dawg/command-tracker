; a = x-position
; y = y-position
; See main.asm for calling arg ZPs
.proc print_flag
@start:
	jsr graphics::drawing::goto_xy
	lda flag
	beq @flag_inactive
@flag_active:
	lda enable_color
	sta zp_TEXT_COLOR
	lda #SCREENCODE_CIRCLE_FULL
	jmp @flag_print
@flag_inactive:
	lda disable_color
	sta zp_TEXT_COLOR
	lda #SCREENCODE_CIRCLE_EMPTY
@flag_print:
  sta VERA_data0
  lda zp_TEXT_COLOR
  sta VERA_data0
@flag_end:
	rts
.endproc