.proc calc_next_instrument_address
  add16to8 instrument_base_address, #sound::INSTRUMENT_NAME_LENGTH, address_calc_result
  lda address_calc_result
  sta instrument_base_address
  lda address_calc_result + 1
  sta instrument_base_address + 1
  rts
.endproc