; Copy names from VRAM to instrument bank

.proc save_names
  ; Set to 2 to skip colors
  lda #%00100001	; Inc set to 2, high ram
  sta VERA_addr_high
  
  ldx #$00
@names_loop:
  lda #INSTRUMENT_NAMES_CURSOR_START_X
  phx
  ply
  jsr graphics::drawing::goto_xy
  ; Read 16 bytes into BRAM
  ldy #$00
@write_name_loop:
  lda VERA_data0
  sta (instrument_base_address),y
@write_name_bottom:  
  iny
  cpy #sound::INSTRUMENT_NAME_LENGTH
  bne @write_name_loop
@names_loop_bottom:
  jsr calc_next_instrument_address
  inx
  cpx num_instruments
  bne @names_loop
@end:
  rts
.endproc