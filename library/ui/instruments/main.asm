.scope instruments

  ; Variables
  address_calc_result     = zp_ADDR0
  instrument_base_address = zp_ARG0
  text_color              = zp_ARG1
  number_color            = zp_ARG2
  num_instruments         = zp_ARG3
  edit_start_x            = zp_ARG4
  edit_start_y            = zp_ARG5

  ; For set_pan
  left_x                  = zp_ARG6
  left_y                  = zp_ARG7
  pan                     = zp_ARG8

  ; For flag
  enable_color            = zp_ARG6
  disable_color           = zp_ARG7
  flag                    = zp_ARG8

  ; Constants
  SCROLL_X_HIGH = $FF
  SCROLL_X_LOW = $E8
  SCROLL_Y_HIGH = $FF
  SCROLL_Y_LOW = $98

  ; Colors
  ACTIVE_COLOR = $01
  LEFT_PAN_ACTIVE_COLOR   = $CD
  RIGHT_PAN_ACTIVE_COLOR = $CA
  PAN_INACTIVE_COLOR = $CB



    ; Constrain movement of the cursor to only the instrunment names list
  INSTRUMENT_NAMES_CURSOR_START_X = $03
  INSTRUMENT_NAMES_CURSOR_STOP_X = $12
  INSTRUMENT_NAMES_CURSOR_START_Y = $00
  INSTRUMENT_NAMES_CURSOR_STOP_Y = $1F

  


  .include "library/ui/instruments/calc_next_instrument_address.asm"
  .include "library/ui/instruments/list_names.asm"
  .include "library/ui/instruments/save_names.asm"
  .include "library/ui/instruments/print_character.asm"
  .include "library/ui/instruments/set_pan.asm"
  .include "library/ui/instruments/print_flag.asm"
  .include "library/ui/instruments/toggle_flags.asm"

  .scope cursor
    .include "library/ui/instruments/cursor/cursor_movement.asm"
    .include "library/ui/instruments/cursor/reset_on_names.asm"
    .include "library/ui/instruments/cursor/reset_on_edit.asm"
  .endscope

.endscope