; Shared routine amongst all instrument types to 
; list the instruments/samples along the left list
; (PSG, PCM, FM)

.proc list_names

@start:
  ; L0 = lower scroll space
  ; Lower Layer VRAM is high (confusing, I know)
  lda #%00010001	; Inc set to 1, high ram
  sta VERA_addr_high
  stz VERA_addr_med
  stz VERA_addr_low

  lda text_color
  sta zp_TEXT_COLOR

  ldx #$00
@names_loop:
  ; Return to left part of the screen, offset by 
  ; y row
  phx
  ply
  lda #$00
  jsr graphics::drawing::goto_xy

  ; First print the inst number
  lda number_color
  sta zp_TEXT_COLOR
  txa
  jsr graphics::printing::print_hex
  ; Skip for colon (on top layer)
  stz VERA_data0
  stz VERA_data0
  
  lda text_color
  sta zp_TEXT_COLOR

  ; Print insturment name
  ldy #$00
@print_name_loop:
  lda (instrument_base_address),y
  jsr graphics::printing::print_alpha_char
  iny
  cpy #sound::INSTRUMENT_NAME_LENGTH
  bne @print_name_loop
  ; End

@names_loop_bottom:
  jsr calc_next_instrument_address
  inx
  cpx num_instruments
  bne @names_loop

; Move the fm names list to the right spot on screen
; Note the values are 2's compliment (negative)
@scroll:
  lda #SCROLL_X_HIGH
  sta VERA_L0_hscroll_h
  lda #SCROLL_X_LOW
  sta VERA_L0_hscroll_l
  lda #SCROLL_Y_HIGH
  sta VERA_L0_vscroll_h
  lda #SCROLL_Y_LOW
  sta VERA_L0_vscroll_l
@end:
  rts

.endproc
