; Helpers
.proc toggle_first_flag
  pha
  and #%10000000
  ; If flag is 0, we enable
  beq @enable
@disable:
  pla
  and #%01111111
  jmp @end
@enable:
  pla
  ora #%10000000
@end:
  rts
.endproc

.proc toggle_second_flag
  pha
  and #%01000000
  ; If flag is 0, we enable
  beq @enable
@disable:
  pla
  and #%10111111
  jmp @end
@enable:
  pla
  ora #%01000000
@end:
  rts
.endproc

.proc toggle_third_flag
  pha
  and #%00100000
  ; If flag is 0, we enable
  beq @enable
@disable:
  pla
  and #%11011111
  jmp @end
@enable:
  pla
  ora #%00100000
@end:
  rts
.endproc

.proc toggle_fourth_flag
  pha
  and #%00010000
  ; If flag is 0, we enable
  beq @enable
@disable:
  pla
  and #%11101111
  jmp @end
@enable:
  pla
  ora #%00010000
@end:
  rts
.endproc