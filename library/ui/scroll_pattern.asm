; Deprecated!!!
.proc scroll_pattern

; r15 = place to hold 16-bit shift value
scroll_pattern:
  ; If we're scrolling, check for keyboard since we disable the system
  ; kernal routine and update the display
  jsr SCNKEY
  ; Print row number as we no longer do this in the ISR
  jsr ui::print_row_number

  ; if we're on row 0, we need to set the scroll, otherwise we jump past it
  lda zp_ROW_NUMBER
  bne @scrolling

@initial_scroll:
  ; move pattern over by 1 character (so it fits in the frame)
  ; move pattern down by pattern_play_pos
  ; THIS SHOULD NOT BE HERE
  ; we shuld do this when playback of the song starts
  ; so we only call this once.
  lda #$FF
  sta VERA_L0_hscroll_h
  lda #$F8
  sta VERA_L0_hscroll_l

  ; This rolls the scroll backwards
  ; (FF00 = -256)
  ;lda #$EE
  lda #PATTERN_SCROLL_START_H
  sta VERA_L0_vscroll_h
  lda #PATTERN_SCROLL_START_L
  sta VERA_L0_vscroll_l
  rts

@scrolling:
  jsr ui::scroll_up

@end:
  rts

.endproc

