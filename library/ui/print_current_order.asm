.proc print_current_order
print_current_order:
  ; Set stride to 1, high bit to 0
  lda #$10
  sta VERA_addr_high

  lda #ORDER_DISPLAY_X
  ldy #ORDER_DISPLAY_Y
  jsr graphics::drawing::goto_xy

  ; Color
  set_text_color #TEXT_COLORS
  lda zp_ORDER_NUMBER       ; Get the current row conunt
  jsr graphics::printing::print_hex
  rts
.endproc
