
.proc cursor_up
cursor_up:
  jsr graphics::drawing::cursor_unplot
  dec cursor_y
  jsr graphics::drawing::cursor_plot
  ;jsr inc_vera
  rts
.endproc

.proc cursor_down
cursor_down:
  jsr graphics::drawing::cursor_unplot
  inc cursor_y
  jsr graphics::drawing::cursor_plot
  ;jsr inc_vera
  rts
.endproc

.proc cursor_left
cursor_left:
  jsr graphics::drawing::cursor_unplot
  dec cursor_x
  jsr graphics::drawing::cursor_plot
  ;jsr dec_vera
  rts
.endproc

.proc cursor_right
cursor_right:
  jsr graphics::drawing::cursor_unplot
  inc cursor_x
  jsr graphics::drawing::cursor_plot
  ;jsr inc_vera
  rts
.endproc

inc_vera:
  ldx VERA_addr_low
  inx
  stx VERA_addr_low
  rts

dec_vera:
  ldx VERA_addr_low
  dex
  stx VERA_addr_low
  rts
