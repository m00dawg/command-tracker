.proc print_speed
print_speed:
  push_state_disable_interrupts

  ; Set stride to 1, high bit to 0
  lda #$10
  sta VERA_addr_high

  lda #SPEED_DISPLAY_X
  ldy #SPEED_DISPLAY_Y
  jsr graphics::drawing::goto_xy

  ; Color
  set_text_color #TEXT_COLORS
  lda zp_SPEED       ; Get the current row conunt
  jsr graphics::printing::print_hex  

  ; Now print BPM
    ; Set stride to 1, high bit to 0
  lda #$10
  sta VERA_addr_high

  lda #BPM_DISPLAY_X
  ldy #BPM_DISPLAY_Y
  jsr graphics::drawing::goto_xy

  ; Color
  ;set_text_color #TEXT_COLORS
	rambank zp_BANK_MISC
	lda sync_method
	beq @vsync
	cmp #$01
	beq @via
@midi:
	print_string_macro_position_color midi_text, #BPM_DISPLAY_X, #BPM_DISPLAY_Y, #TEXT_COLORS
  bra @end
@via:
	lda zp_BPM       ; Get the current row conunt
  jsr graphics::printing::print_hex
	bra @end
@vsync:
	print_string_macro_position_color vsync_text, #BPM_DISPLAY_X, #BPM_DISPLAY_Y, #TEXT_COLORS

@end:
  plp
  rts

	vsync_text: 
		.byte "vs",0
	midi_text: 
		.byte "md",0
.endproc
