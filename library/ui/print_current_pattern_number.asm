.proc print_current_pattern_number
@start:
  ; Placeholder to display pattern (currently hardcoded to 01)
  ; Set stride to 1, high bit to 0
  lda #$10
  sta VERA_addr_high
  lda #PATTERN_DISPLAY_X
  ldy #PATTERN_DISPLAY_Y
  jsr graphics::drawing::goto_xy
  ; Color
  set_text_color #TEXT_COLORS
  lda zp_PATTERN_NUMBER       ; Get the current pattern
  jsr graphics::printing::print_hex        ; print it
  rts
.endproc
