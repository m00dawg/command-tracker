; Push X random values onto the stack
; This is broken. I am sad.
; x = number of random values to generate and push onto the stack
.proc push_entropy
	; Preserve return address
	pla
	sta zp_ADDR0
	pla
	sta zp_ADDR0 + 1

	phx
	jsr entropy_get
	sta entropy_seed
	stz entropy_seed + 1
	plx

@loop:
	jsr galois16
	pha
	dex
	bne @loop

@end:
	; Restore return address
	lda zp_ADDR0 + 1
	pha
	lda zp_ADDR0
	pha

	rts
.endproc


; Taken from https://github.com/bbbradsmith/prng_6502/blob/master/galois16.s
.proc galois16
	lda entropy_seed+0
	.repeat 8
		asl
		rol entropy_seed+1
		bcc :+
			eor #$39
		:
	.endrepeat
	sta entropy_seed+0
	cmp #0
	rts
.endproc