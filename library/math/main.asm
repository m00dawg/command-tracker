; General math routines
.segment "CODE"

.scope math
  ;.include "library/math/add16.asm"
  ;.include "library/math/add.asm"
  .include "library/math/mod8.asm"
  .include "library/math/multiply_by_12.asm"
  .include "library/math/divide_by_12.asm"
  .include "library/math/multiply16.asm"
  .include "library/math/set_top_nibble.asm"
  .include "library/math/sort8.asm"  
  .include "library/math/push_entropy.asm"
.endscope
