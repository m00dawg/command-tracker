; Multiply a by 12, return result as a
.proc multiply_by_12
multiply_by_12:
  phx
  sta zp_MATH0
  ldx #$0B    ; 12
  clc
@loop:
  adc zp_MATH0
  dex
  bne @loop
@end:
  plx
  rts
.endproc
