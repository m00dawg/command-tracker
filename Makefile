all:

#	cl65 -t cx16 -o BOX.PRG -l box.list box.asm
#	cl65 -t cx16 -o DRAWCHARS.PRG -l drawchars.list drawchars.asm
#	cl65 -t cx16 -o PLAYSONG.PRG -l playsong.list playsong.asm
#	cl65 -t cx16 -o DREAMTRK.PRG -l dreamtracker.list dreamtracker.asm
	cl65 -t cx16 -C cx16.cfg -o DT.PRG -l dreamtracker.list dreamtracker.asm -Ln dreamtracker.sym -m dreamtracker.map
#	cl65 -t cx16 -o KEYBOARD.PRG  keyboard-test.asm
#	cl65 -t cx16 -o SCRATCH.PRG scratch.asm
#	cl65 -t cx16  -o TEST.PRG test.asm 

zip:
	zip dreamtracker.zip DT.PRG SCR/*.SCR DAT/*.DAT SONGS/DEMO.DTR SONGS/RUNNING.DTR SONGS/WILY.DTR SONGS/TEARS.DTR SONGS/LOOP.DTR SONGS/ROTN.DTR SONGS/CAVE.DTR SONGS/NEW.DTR SAMPLES/*

clean:
	rm -f *.prg disk.d64 *.PRG *.o *.list *.zip dump.bin *.dis *.wav *.sym dump*.bin *.map DAT/*

docs:
	./buildocs.sh

cart:
	./makecart -ram 32 95 -o test.crt
