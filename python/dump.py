#!/usr/bin/env python
# A program which outputs a Dream Tracker save file
# for version 2 of the saves

import sys
import cbmcodecs2

null_value = 255

note_alpha = [
    'C-', 'C#', 'D-', 'D#', 'E-', 'F-', 
    'F#', 'G-', 'G#', 'A-', 'A#', 'B-'
]

#note_names:  .byte "ccddeffggaab-^-."
#note_sharps: .byte "-#-#--#-#-#--^-."


def note_to_text (note):
    if note == 0xFF:
        return "..."
    if note == 0xFE:
        return "==="
    if note == 0xFD:
        return "^^^"

    octave = int(note) // 12
    note_number = note - (octave * 12)
    return(note_alpha[note_number] + str(octave))
    
def print_effect (effect):
    if effect == 0xFF:
        return ".."
    return f'{effect:0{2}x}'

def print_hex(value):
    return f'{value:0{2}x}'

def uint_from_bytes(value):
    return int.from_bytes(value, byteorder='little', signed=False)


filename=sys.argv[1]

file = open(filename, "rb")

version = uint_from_bytes(file.read(1))

# Misc Bank
misc = file.read(0x2000)
# Envelope Bank
envelopes = file.read(0x2000)
# VERA Instruments Bank
vera_instruments = file.read(0x2000)
# FM Instruments Bank
fm_instruments = file.read(0x2000)

# Patterns
# 8193 because the first byte is the pattern number
patterns = []
while True:
    pattern = []
    pattern_number_byte = file.read(0x1)
    pattern_number = uint_from_bytes(pattern_number_byte)
    # Break on seeing a 0 (end of patterns)
    if pattern_number == 0:
        break
    pattern.append(pattern_number_byte)
    pattern.append(file.read(0x2000))
    patterns.append(pattern)

# Samples
# 8193 because the first byte is the sample number
samples = []
while True:
    sample = []
    sample_number_byte = file.read(0x1)
    sample_number = int.from_bytes(sample_number_byte, byteorder='little', signed=False)
    # Break on seeing a 0 (end of patterns)
    if sample_number == 255:
        break
    sample.append(sample_number_byte)
    sample.append(file.read(0x2000))

file.close()

# Dig through misc bank
order_list					= misc[0x0000:0x0FF]
order_list_sorted 			= misc[0x0100:0x01FF]
title 						= misc[0x0200:0x021F]
composer 					= misc[0x0220:0x023F]
sync_method 				= misc[0x0240]
starting_speed 				= misc[0x0241]
starting_bpm 				= misc[0x0242]
global_transpose  			= misc[0x0243]
fm_global_timer_a_high 		= misc[0x0244]
fm_global_timer_a_low  		= misc[0x0245]
fm_global_timer_b  			= misc[0x0246]
fm_global_irq_control  		= misc[0x0247]
fm_global_ch7_noise_control = misc[0x0248]
fm_global_lfo_frequency 	= misc[0x0249]
fm_global_lfo_am_depth  	= misc[0x024A]
fm_global_lfo_pm_depth  	= misc[0x024B]
fm_global_lfo_waveform  	= misc[0x024C]
padding						= misc[0x024D:0x02FB]
description					= misc[0x02FC:0x06FC]
space_to_grow				= misc[0x06FD:0x07FF]

sample_start = 0x0800
sample_names = []
for i in range(16):
    sample_name = misc[sample_start:sample_start+0x0010]
    sample_names.append(sample_name)
    sample_start = sample_start + 0x0010



print("File Version: " + print_hex(version))
print("Song Title: " + title.decode('screencode_c64_uc'))
print("Composer: " + composer.decode('screencode_c64_uc'))
print("Description:")
try:
	print(description.decode('screencode_c64_uc'))
except Exception:
    pass
print("Starting Speed: " + print_hex(starting_speed))

print("Orders:")
for order in order_list:
    print(print_hex(order) + " ", end="")
print()

print("Sample names")
for i,name in enumerate(sample_names):
    print(str(i) + ": " + name.decode('screencode_c64_uc'))

#print("Envelopes:")
#count = 0
#for i in range(32):
#    print("Envelope " + print_hex(i))
#    for j in range(256):
#        print(print_hex(envelopes[count]) + " ", end=" ")
#        count += 1
#    print()

print("Patterns")
pattern_number = 0
for pattern in patterns:
    # This would be the final 0 byte after all the patterns have been read
    if len(pattern[1]) == 1:
        print("End of Patterns")
    if len(pattern[1]) == 8192:
        counter = 0
        print("Pattern: " + str(uint_from_bytes(pattern[0])))
        for row in range(0x40):
            print(print_hex(row) + ": ", end="")
            for channel in range(25):
                # Note
                print(note_to_text(pattern[1][counter]), end=" ")
                counter += 1
                # Inst
                print(print_effect(pattern[1][counter]), end=" ")
                counter += 1          
                # Vol
                print(print_effect(pattern[1][counter]), end=" ")
                counter += 1
                # Effect Parameter
                print(print_effect(pattern[1][counter]), end="")
                counter += 1
                # Effect Value
                print(print_effect(pattern[1][counter]), end=" ")
                counter += 1
            print()
        pattern_number += 1



