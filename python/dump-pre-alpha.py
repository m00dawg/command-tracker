#!/usr/bin/python3

# A program which outputs a Dream Tracker save file

null_value = 255

note_alpha = [
    'C-', 'C#', 'D-', 'D#', 'E-', 'F-', 
    'F#', 'G-', 'G#', 'A-', 'A#', 'B-'
]


def note_to_text (note):
    if note == 0xFF:
        return "..."
    if note == 0xFE:
        return "==="
    if note == 0xFD:
        return "^^^"

    octave = int(note) // 12
    note_number = note - (octave * 12)
    return(note_alpha[note_number] + str(octave))
    
def print_effect (effect):
    if effect == 0xFF:
        return ".."
    return f'{effect:0{2}x}'



filename="SONGS/OLD.DTR"

file = open(filename, "rb")

# OS header
header = file.read(2)

version = file.read(1)
song_speed = file.read(1)
# That extra byte is a bug in the load/save ? 
# Should probably just change to storing the entire page...
title = file.read(16) 
composer = file.read(16)
orders = file.read(256)
envelopes = file.read(8192)
vera_instruments = file.read(8192)

# No idea why it woudl be off?
not_sure = file.read(1)

patterns = []
while(pattern := file.read(8193)):
    patterns.append(pattern)
file.close()

print(orders)

pattern_number = 0
for pattern in patterns:
    print("Pattern: " + str(pattern_number))
    print("Pattern Length: " + str(len(pattern)))    
    counter = 0
    for row in range(0x40):
        print(f'{row:0{2}x}' + ": ", end="")
        for channel in range(17):
            # Note
            print(note_to_text(pattern[counter]), end=" ")
            counter += 1
            # Inst
            print(print_effect(pattern[counter]), end=" ")
            counter += 1          
            # Vol
            print(print_effect(pattern[counter]), end=" ")
            counter += 1
            # Effect Parameter
            print(print_effect(pattern[counter]), end="")
            counter += 1
            # Effect Value
            print(print_effect(pattern[counter]), end=" ")
            counter += 1
        print()
    pattern_number += 1



