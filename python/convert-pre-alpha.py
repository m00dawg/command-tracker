#!/usr/bin/python3

# A program which attemps to convert between the alpha
# file format which only supported PSG (but had a random PCM column)
# and the newer format which supports all 25 channels.
#
# The newer format should be easier to migrate to/from since 
# there is no longer the weird OS header in the file anymore.

null_value = 255
zero_value = 0

note_alpha = [
    'C-', 'C#', 'D-', 'D#', 'E-', 'F-', 
    'F#', 'G-', 'G#', 'A-', 'A#', 'B-'
]


def note_to_text (note):
    if note == 0xFF:
        return "..."
    if note == 0xFE:
        return "==="
    if note == 0xFD:
        return "^^^"

    octave = int(note) // 12
    note_number = note - (octave * 12)
    return(note_alpha[note_number] + str(octave))
    
def print_effect (effect):
    if effect == 0xFF:
        return ".."
    return f'{effect:0{2}x}'


filename="SONGS/OLD.DTR"
new_filename="SONGS/NEW.DTR"

file = open(filename, "rb")
new_file = open(new_filename, 'wb')

# OS header
header = file.read(2)

version = file.read(1)
song_speed = file.read(1)
# That extra byte is a bug in the load/save ? 
# Should probably just change to storing the entire page...
title = file.read(16) 
file.read(1)
composer = file.read(16)
file.read(1)
orders = file.read(256)
envelopes = file.read(8192)
vera_instruments = file.read(8192)


# 8193 becuase the first byte is the pattern #
patterns = []
while(pattern := file.read(8193)):
    patterns.append(pattern)
file.close()

# New file formats no longer require the header
#new_file.write(header)
# Version
new_version = 1
new_file.write(new_version.to_bytes(1, 'little'))
new_file.write(song_speed)
new_file.write(title)
new_file.write(composer)
new_file.write(orders)
new_file.write(envelopes)
new_file.write(vera_instruments)

# Version 00 of the save files was VERA + PCM
# New version add's FM which adds 40 bytes per row
# Since PCM didn't work on version 00, we can ignore things there
# to make it easy.

# So this means we need to read just the VERA/PSG from an old row (80)
# Then pad the rest (45 bytes) with $FF to re-align the patterns

#for pattern in patterns:


print(orders)

pattern_number = 0
for pattern in patterns:
    if(len(pattern) == 8193):
        counter = 0
        print("Pattern: " + (print_effect(pattern[counter])))
        byte = pattern[counter].to_bytes(1, 'little')
        new_file.write(byte)
        print("Pattern Length: " + str(len(pattern)))
        counter += 1
        for row in range(0x40):
            print(f'{row:0{2}x}' + ": ", end="")
            for channel in range(17):
                # Note
                print(note_to_text(pattern[counter]), end=" ")
                byte = pattern[counter].to_bytes(1, 'little')
                new_file.write(byte)
                counter += 1
                # Inst
                print(print_effect(pattern[counter]), end=" ")
                byte = pattern[counter].to_bytes(1, 'little')
                new_file.write(byte)
                counter += 1          
                # Vol
                print(print_effect(pattern[counter]), end=" ")
                byte = pattern[counter].to_bytes(1, 'little')
                new_file.write(byte)
                counter += 1
                # Effect Parameter
                print(print_effect(pattern[counter]), end="")
                byte = pattern[counter].to_bytes(1, 'little')
                new_file.write(byte)
                counter += 1
                # Effect Value
                print(print_effect(pattern[counter]), end=" ")
                byte = pattern[counter].to_bytes(1, 'little')
                new_file.write(byte)
                counter += 1
            for padding in range(8*5):
                new_file.write(null_value.to_bytes(1, 'little'))
            print()
        
        pattern_number += 1
        for pading in range(192):
            new_file.write(null_value.to_bytes(1, 'little'))

# Write final 0 to indicate we're done loading patterns
new_file.write(zero_value.to_bytes(1, 'little'))
new_file.close()