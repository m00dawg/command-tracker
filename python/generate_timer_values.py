#!/usr/bin/python3

# Generate timer value LUTs from the MIDI BPM range
# Used for VIA sync

starting_bpm = 30
max_bpm = 250
updates_per_row = 100
rows_per_beat = 4
clockspeed = 8000000
hi_bytes = []
lo_bytes = []
filename = "TIMING.DAT"

bpm = starting_bpm
while bpm < max_bpm:
  bps = (bpm / 60)
  updates_per_beat = rows_per_beat * updates_per_row
  ticks_per_second = updates_per_beat * bps
  timer_value = round(clockspeed / ticks_per_second)
  timer_in_hex = str(hex(timer_value))


  #print("bpm: " + str(bpm))
  #print("bps: " + str(bps))
  #print("ticks per second: " + str(ticks_per_second))
  #print("timer value: " + str(timer_value))
  #print("")
  #string[start:end:step]
  print(timer_in_hex)
  print("Big: " + timer_in_hex[2:4])
  print("Little: " + timer_in_hex[4:6])
  hi_bytes.append(int(timer_in_hex[2:4], 16))
  lo_bytes.append(int(timer_in_hex[4:6], 16))
  bpm += 1
    
print(hi_bytes)
print()
print(lo_bytes)

timer_file = open(filename, 'wb')

# Write lo byte LUT
counter = 0
while counter < (max_bpm - starting_bpm):
  timer_file.write(lo_bytes[counter].to_bytes(1, 'little'))
  counter += 1

# Write hi byte LUT
counter = 0
while counter < (max_bpm - starting_bpm):
  timer_file.write(hi_bytes[counter].to_bytes(1, 'little'))
  counter += 1

timer_file.close()
