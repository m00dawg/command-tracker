# Handling Samples

Samples are currently stored in BRAM (with plans to move them to 
CRAM) and are limited to the page size, 8k.

Since the max length of a sample is known, this can be used to
determine how fast we need to fill the PCM buffer and allows
a semi-naive approach to doing so.

The PCM data is divided into 256 byte "chonks". During the 
sound engine tick interrupt, one or more chonks is written
to the PCM buffer depending on the desired sample playback
(which is just the raw channel note value). The value is
the lowest value which fills the buffer fast enough to keep
up, but not so fast it overrruns the buffer before the 
sample length is reached.





