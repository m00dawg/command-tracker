# Instruments

Thoguhts on how to structure instruments if the tracker ends up having it's
own sound engine (and not using Concerto, which has it's own patching solution
that can be used).

This is a living document while I work on deciding how I might structure
patches. For instance should I go with 8 PSG channels instead of 16 
to make them bitimbral?

## VERA Instruments

Patterns support $FF (255) instruments
Current banking supports $80 (128) instruments

## Envelope Flags

### Volume and Wave/PWM

    bit 7 = enabled
    bit 6 = range (full / lower)
    bit 5 = start (immediate / noterel)
    bit 4 = loop
    bit 3 = loop direction
    bit 2-0 = speed

### Pitch

    bit 7 = enabled
    bit 6 = fine or coarse
    bit 5 = start (immediate / noterel)
    bit 4 = loop
    bit 3 = loop direction
    bit 2-0 = speed

## Structure of Arrays

See `bank.inc`

```
.segment "BANKVINS"
    ; Using literals here because not sure it works with variables
    ; using structs doesn't seem to work either
    ; Number of instruments: $80 (128)

    ; Space used: 
    ; names = 2048 (16 * 128)
    ; instrument data = 2304 (18 * 128)
    ; Remaining: 3840

    vera_instrument_names:          .res ($80 * $10)    ; 128 * 16
    vera_instruments_vol:                       .res $80
    vera_instruments_fine_pitch:                .res $80
    vera_instruments_wave:                      .res $80
    vera_instruments_vol_envelope_num:          .res $80
    vera_instruments_vol_envelope_length:       .res $80
    vera_instruments_vol_envelope_flags:        .res $80
    vera_instruments_vol_envelope_loop_start:   .res $80
    vera_instruments_vol_envelope_loop_end:     .res $80
    vera_instruments_wave_envelope_num:         .res $80
    vera_instruments_wave_envelope_length:      .res $80
    vera_instruments_wave_envelope_flags:       .res $80
    vera_instruments_wave_envelope_loop_start:  .res $80
    vera_instruments_wave_envelope_loop_end:    .res $80
    vera_instruments_pitch_envelope_num:        .res $80
    vera_instruments_pitch_envelope_length:     .res $80
    vera_instruments_pitch_envelope_flags:      .res $80
    vera_instruments_pitch_envelope_loop_start: .res $80
    vera_instruments_pitch_envelope_loop_end:   .res $80
```

## YM2151 Insturments

Instruments only need a subset of the FM registers which would be:

24 bytes:

fm_instruments_pan_algo: 
fm_instruments_key_code:
fm_instruments_key_fraction:
fm_instruments_modulation_sensitivity:
fm_instruments_detune_fine_frequency_m1:
fm_instruments_detune_fine_frequency_m2:
fm_instruments_detune_fine_frequency_c1:
fm_instruments_detune_fine_frequency_c2:
fm_instruments_attenuation_m1:
fm_instruments_attenuation_m2:
fm_instruments_attenuation_c1:
fm_instruments_attenuation_c2:
fm_instruments_ks_attack_m1:
fm_instruments_ks_attack_m2:
fm_instruments_ks_attack_c1:
fm_instruments_ks_attack_c2:
fm_instruments_am_decay1_m1:
fm_instruments_am_decay1_m2:
fm_instruments_am_decay1_c1:
fm_instruments_am_decay1_c2:
fm_instruments_detune_coarse_decay2_m1:
fm_instruments_detune_coarse_decay2_m2:
fm_instruments_detune_coarse_decay2_c1:
fm_instruments_detune_coarse_decay2_c2:



https://github.com/X16Community/x16-docs/blob/master/X16%20Reference%20-%2009%20-%20Sound%20Programming.md

```
.segment "BANKFINS"
    ; Using literals here because not sure it works with variables
    ; using structs doesn't seem to work either
    ; Number of instruments: $80 (128)

    ; Space used: 
    ; names = 2048 (16 * 128)
    ; instrument data = 24 * 128
    ; Remaining: 3072

    fm_instrument_names:          .res ($80 * $10)    ; 128 * 16

    fm_instruments_pan_algo: 
    fm_instruments_key_code:
    fm_instruments_key_fraction:
    fm_instruments_modulation_sensitivity:
    fm_instruments_detune_fine_frequency_m1:
    fm_instruments_detune_fine_frequency_m2:
    fm_instruments_detune_fine_frequency_c1:
    fm_instruments_detune_fine_frequency_c2:
    fm_instruments_attenuation_m1:
    fm_instruments_attenuation_m2:
    fm_instruments_attenuation_c1:
    fm_instruments_attenuation_c2:
    fm_instruments_ks_attack_m1:
    fm_instruments_ks_attack_m2:
    fm_instruments_ks_attack_c1:
    fm_instruments_ks_attack_c2:
    fm_instruments_am_decay1_m1:
    fm_instruments_am_decay1_m2:
    fm_instruments_am_decay1_c1:
    fm_instruments_am_decay1_c2:
    fm_instruments_detune_coarse_decay2_m1:
    fm_instruments_detune_coarse_decay2_m2:
    fm_instruments_detune_coarse_decay2_c1:
    fm_instruments_detune_coarse_decay2_c2:

.struct YMInstrument 
    name            .byte 16
    
.endstruct
```
