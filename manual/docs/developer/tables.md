# Tables

Tables work kind of like envelopes in that they run at the speed 
of the sound engine. They are much like tables in LSDJ only 
allow up to 4 effects over 32 rows to be used. This results in
32 total tables per bank. This is mostly to make address math
easier, though 4 effects over 16 rows giving 64 tables might
be more advantageous.

Since tables run at engine speed, this is one good way to do 
things like fast pitch slides needed for PWM kickdrums.