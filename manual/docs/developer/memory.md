# Banked Memory Allocation

The following assumes fully populated expansion RAM (2MB)

```
00 = Kernel
01-FB = Patterns
FC = Channel Registers
FD = VERA Instruments
FE = Envelopes
FF = Misc Bank (Song Title, Composer, Order List)
```