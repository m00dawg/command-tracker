File Format
-----------

This is an overview of the file storage format. There's quite a few moving
pieces here so be sure to look at the other sections in the table of contents.

In general, section terminators are defined by `0xFF`. This means the total
count of some things is 127 not 128. Pattern numbers start at 1 and not 0.
This is becuase patterns are stored in BRAM and page 0 is for the kernal.
This means we can use '0' to denote when we are done reading patterns.

This is current a very basic, and arguably wasteful, format.


```
VERSION: 1 byte
MISC: 8k (page)
  Contains song title, composer, starting speed, order list, 
  sorted order list, sample names, and sample metadata
ENVELOPES: 8k (page)
VERA_INSTRUMENTS: 8k (page, though in reality instruments are currently less)
FM_INSTRUMENTS: 8k (page, though in reality instruments are currently less)

PATTERNS:
  Pattern #: (1 byte)
  Pattern Data: 8k (page)
  ...
Patterns End Marker: $00 (1 byte)

SAMPLES:
  Sample #: (1 byte)
  Sample Data: 8k (page)
  ...
Samples End Marker: $FF (1 byte)
```