# DreamTracker

Manual for DreamTracker, a native music tracker for the 
[Commander X16](https://commanderx16.com/) principally 
written by Tim Soderstrom.

![Pattern Example](manual/images/pattern2.png)

## Download

Current Version: **V0.71**: Bugfixes and Disco

To download the latest version simply click 
[here](https://www.dreamtracker.org/downloads/dreamtracker-0.71.zip)

Changes from 0.70 include:

  * Background Color Effect
  * Linear Finetune effect for PSG
  * Save Fix
  * Numerous small bugfixes
  
  
**Important** The version now includes a file converter. At present,
and part of the reason this is a pre-release, it updates the effect parameters
to the new values but does *not* currently adjust the vibrato/tremolo/pulsolo
and these have changed significantly.

Also please use a recent version of x16emu (or VERA if running on real hardware)
to make sure you benefit from the XOR feature of the triangle and saw.

## Video Demos

You can find a list of teaser and demo videos on the 
Victim Cache YouTube page [here](https://www.youtube.com/watch?v=irjA-zv4lDo&list=PL1NEcis11RsTsCb68rSr0VXk83Cp6xDJP&index=8).

## Discussion

If you want to talk to Tim or others about DreamTracker, here's
a few places to go:

  * If you are on Matrix, join [#general](https://matrix.to/#/#general:matrix.dreamtracker.org) on the DreamTracker test server
  * Join the [Commander X16 Discord](https://discord.gg/nS2PqEC) (Tim is @m00dawg there)
  * Follow the [devlog](https://cx16forum.com/forum/viewtopic.php?t=6669) 
    on the Commander X16 forums.
  * Follow Tim on Mastodon (@tim@social.victimcache.com)

## Support

DreamTracker is currently free and open source. If you would like to
support development, you can peruse the source code 
[here](https://gitlab.com/m00dawg/dream-tracker).

You can also support the project by supporting the band Tim is a part of,
[Victim Cache](https://www.victimcache.com). We use Dream Tracker, along
with other trackers and chiptune instruments and there's a good chance
you might like at least some of the music we make. Proceeds from our
album and merch sales help fund development of Dream Tracker.

Or simply spread the word about Dream Tracker and the Commander X16.

## User Documentation

Information on how to use Dream Tracker (you know, to make music
and stuff).

  * [Overview of Trackers](manual/overview.md)
  * [Installing & Running Dream Tracker](manual/install.md)
  * [Using Dream Tracker](manual/usage.md)
  * [Patterns](manual/patterns.md)
  * [Macros](manual/macros.md)
  * [VERA Instruments](manual/vera_instruments.md)
  * [FM Instruments](manual/fm_instruments.md)
  * [PCM Samples](manual/samples.md)
  * [MIDI and Wavetable](manual/midi.md)
  * [Effects List](manual/effects.md)
  * [Pre 0.70 Effects List](manual/pre-0.70-effects.md)
  * [Envelopes](manual/envelopes.md)
  * [The Order List](manual/orders.md)
  * [ZSM Export](manual/zsm.md)

## Developer Documentation

Developer docs are a mess right now. I'm working on it! But wanted
the user docs to be at least somewhat reasonable first.