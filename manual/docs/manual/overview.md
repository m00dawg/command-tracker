# Overview of Trackers

A tracker is a specific type of music sequencing software. Music
is written in patterns, which are a bit like a piano roll. A 
pattern is often divided up into channels and within each channel
notes can be placed, along with modifiers (effects) at specified rows.

Arranging these patterns into an list comprises a song.

This method of music creation has some benefits and drawbacks which
is why trackers have persisted since the very first tracker,
Soundtracker. In the case of the X16, it is a good way to provide
a dense and precise way of modulating the sound chips.

There have been many with trackers still being made today - including
this one! But also fully modern and professional trackers such as 
[Renoise](https://www.renoise.com/) which supports samples, software
instruments, Rewire, etc.

Dream Tracker is a native tracker which runs on the Commander X16.
It's interface is derived from Impulse Tracker and shares some of 
the same design decisions. Dream Tracker is not the only tracker
to support the X16. At present the modern PC tracker, 
[Furance](https://github.com/tildearrow/furnace) also supports the X16.
At present is supports all the sound features of the X16, in fact 
whereas Dream Tracker currently only supports the PSG though support
for the YM2151 and PCM definitely planned.

## Further Reading

  * https://en.wikipedia.org/wiki/Music_tracker