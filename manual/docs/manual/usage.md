# Using Dream Tracker

Once Dream Tracker is running, you will be presented with the help menu.
As Dream Tracker was inspired by Impulse Tracker, the F-keys are mostly 
the same.

Dream Trackers stats with a blank song and initial instruments set. 
From here you can start making music!

## Exiting

If you wish to exit the tracker, you can press `ALT-Q` in most modules.
At present this performs a soft reboot using the SMC.

## Loading and Saving

If you wish to load or save a song, press F10 to pull up the load-save
menu and arrow-key down to the FILENAME. Here you can put in the 
filename of the song to load or save. It is recommended, though not required,
to use the .DTR extension. Files are loaded from the same directory
DREAMTRK.PRG is in. In the future, saving/loading from a SONGS folder
will be supported (and hopefully a full file navigation system).

From here on out, check out the other parts of the manual for how
to use various parts of the tracker. If you're not familiar with
trackers, consider starting with the [Overview](overview.md).