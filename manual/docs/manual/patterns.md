# Patterns

## Overview

Patterns are the beating heart of trackers. A pattern is a collection
of one or more (in the case of Dream Tracker more) channels with 
a number of rows. 64 rows is common (which is the current max
row size for Dream Tracker). Typically patterns are displayed
with channels going across the top and rows going downward - 
sort of like a player piano.

Each channel in a row can have notes places within it along with
an instrument number and an optional volume and effect value. The
instrument tells the sound engine which instrument to play and is
always required when playing a note. If volume is ommitted, the
default volume of the instrument is used unless there are active
effect influencing the volume (such as a fast volume sweep).

VERA instruments can be played on channels $00-$0F (00-15) and 
you can switch instruments as often as you feel like. This can 
be extremely advantageous for sculpting the sound.

Effects are optional and allow modulating the sound of the channel.
There are global, shared and unique effects.

Some effects are temporal and only fire when they are called and some
persist until they are turned off. For more information see
the [Effects](effects.md) section of the manual.

When a song is being played, the sound engine scans each row in a
pattern from top to bottom. When it reaches the end, it will consult
the [Orders](orders.md) list to fetch the next pattern. In this way an
entire song can be constructed by arranging patterns.

## Navigation

Be sure you are in Pattern Edit mode (`F2`). 

| Key         | Function                                |
| ----------- | --------------------------------------- |
| Arrow-Keys  | Move around                             |
| `TAB`       | Move to next channel                    |
| `SHIFT-TAB` | Move to previous channel                | 
| `ALT-1`			| Jump to First PSG Channel								|
| `ALT-2`			| Jump to First FM Channel								|
| `ALT-3`			| Jump to PCM Channel				      				|
| `Page Up`   | Move up 16 rows                         |
| `Page Down` | Move down 16 rows                       |
| `Home`		  | Go to first row	($00)										|
| `End`		    | Go to last row	($3F)										|
| `[`         | Decrease octave                         |
| `]`         | Increase octave                         |
| `-`         | Jump to previous pattern in order list  |
| `+`         | Jump to next pattern in order list      |
| `ALT-B`     | Mark begin of block                     |
| `ALT-E`     | Mark end of block                       |
| `ALT-V`     | Paste block                             |
| `ALT-I`     | Assign instrument to block              |
| `ALT-]`     | Shift notes in block up by 1 semitone   |
| `ALT-[`     | Shift notes in block down by 1 semitone |

### Placing Notes and Effects

![Pattern Example](images/pattern1.png)

The first 3 dots are the note. To place a note, use the keyboard
like a sort of piano. Pressing `Z` will place a C-# where # is the 
current selected octave. `Q` will place a C at an octave above the 
currently selected octave. From here you can experiment to figure out
the rest, but it functions very similar to the keys on a piano.

To select an instrument, arrow over to the instrument colum, which is
the 2nd set of dots right next to the note. If there is already an
instrument there you can replace it. In doing so you will update
the selected instrument so the next time you place a note, it will
use that instrument.

Next is the volume column. As noted this is optional but if you
want to specify a volume or volume effect, you would put it here.

Then the effects. Effects are two parts. The first is the effect
parameter and the second is the value for that parameter. You
can read more about them in the [Effects](effects.md) section 
of the manual.

### NOTEOFF and NOTEREL

There are two special notes you can place in patterns. Pressing
the `1` key while on the note part of a pattern/row will place a
`---`. This is a special note called NOTEOFF which immediately 
stops the note being played.

Pressing \` will place a NOTEREL (Note-Release) which is 3
PETSCII up-arrows. While NOTEOFF immediately stops the note,
NOTEREL allows automation to continue depending on how the instrument
has been setup.

## Organizing Patterns

While many modern trackers tend to be based around Fast Tracker ][
and more specifically FamiTracker, Dream Tracker is based around Impulse
Tracker. It uses some of the same keyboard shortcuts and, unlike many
modern trackers, uses a single pattern and a single order list.

By contrast, [Furnace](https://github.com/tildearrow/furnace) adopts
a one pattern per channel approach where these patterns can be ordered
in dependently. This has pros and cons. Neither is necessarily better
than the other and either can be used to make great music for the X16.

For more information on arranging patterns, check out the 
[Orders](orders.md) part of the manual.