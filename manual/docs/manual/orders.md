# Orders

The order list is access by pressing `F11`.

The orders list is where you can arrange your song. Dream Tracker
was inspired by Impulse Tracker and, hence, there is a single 
order list. Each number in the list corresponds to a full pattern.
The orders is list is 256 entries long though your song doesn't have
to be that long.

The sound engine will play through from order 00 until it reaches
the last order number which has a number. In other words, it will 
stop at the first `--` and loop back around. This means you can use parts 
of the orders list as a sort of idea board without having to 
put those orders in the song. You can also use 
[pattern effects](effects.md) to jump around the orders list as well.

Navigate the list using the up and down arrows. To pull up a pattern,
press `G` (for Goto Pattern) which will pull up the Edit Pattern screen
(`F2`) with the pattern loaded.

To create a new pattern, simply type in a number from 01 to F0. Keen eyed
oberservers might note that this feels a lot like selecting a RAM bank and,
in fact, that's exactly how it works. Each pattern is stored in a page in
BRAM. This is why pattern `00` doesn't exist and is instead used to delete
a pattern from the order list. Likewise, this is why it only goes up to
`F0`. The higher pages are reserved for Dream Tracker use. At present only
3 pages are used so a few more patterns may be added in the future
depending on how much RAM is needed for new features (FM insturments,
 notably).