# Samples

DreamTracker currently supports importing raw mono 8-bit signed samples at 
playback rates of up to 48.8kHz. The max size is 8k at present and
only 16 samples are supported, though this will very likely change
once the new cartridge / memory expansion boards are available
for the real hardware. That change will allow for at least 128 
samples at up to 16k each.

## Loading Samples

To load samples, press `F3` to get to the sample bank screen.
Here you can name and load samples from disk. The left side
allows you to choose and name a sample. Note for samples
to be saved to the song, you must give them a name. 

Press `TAB` to switch to the right pane and type in the 
filename of the sample you wish to load to that sample number. 

DreamTracker expects samples to be in the SAMPLES directory
so be sure to place your raw files there. Then simply type
the name of the sample and press `ENTER`.

### Converting samples for DreamTracker

As noted DreamTracker at present can only import raw mono 8-bit signed
samples no larger than 8k. One way to produce these is to use 
Audacity (or one of the forks, such as Tenacity), and load up your sample.

Then set the project rate on the bottom left to no greater than 48828.
Note that since samples are space limited to 8k, such a high sample rate
would be a fairly short sample. Additionally you may want to modulate 
the sample rate in the song such that it is recommend to use a sample
rate closer to 24414 (half-rate) or 12207 (quarter-rate). If you prefer
to use the more standard sample rates of 48, 44.1, 22.05, 11.025 you 
can, just be aware you may have to adjust things in the tracker.

If the sample is in stereo, click on the down area next to the sample
name in the waveform view and click Splite Stereo Track. Then delete
one of the tracks.

Finally go to File->Export->Export Audio. For Header select
RAW (header-less) and for Encoding, select Signed 8-bit PCM. Name 
your file whatever you like (best to keep it short) and place it in
the SAMPLES directory. Make sure it is under 8k.

## Playing Samples

Each sample corresponds to an instrument in the pattern, much 
like normal VERA instruments. These can only be used on the PCM
channel. To get to the PCM channel quickly while in the pattern
editor (`F2`) simply press `CMD-3`.

Couple things to note (get it!?), PCM only supports 4-bit volume
so the max for the volume column is `0F`. Instruments work as they
do for the other tracks, selecting an instrument selects the 
corresponding sample.

The note-values correspond to the playback speed (sample rate)
from 1 (basically noise at roughly 350Hz) up to 128 (48828Hz). 
The sample rates do not equate to pitch. This makes using
melodic samples more difficult, but should be sufficient 
for things like toms.

A future enhancement may be a way to map samples to notes
more directly to build a sort of sample-pack so one can play
melodic samples (like a piano) easier though this may be more
of a 2.0 sort of feature.

## Future Plans

Some effects such as volume and pitch slides, retrigger, sample-offsets.

A DreamTracker specific file format could be useful. Right now only volume
is part of a sample, but in the future panning _might_ be where one could
hard pan left, right, or center. Actual stereo samples are a maybe and if so
having a header and file format becomes very important.

Also a distant future is multi-page samples though this may be more of a 2.0 
feature.