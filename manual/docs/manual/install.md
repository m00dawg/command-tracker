# Installing and Running DreamTracker

Dream Tracker was written for the X16 in mind so if able, it is
recommended to run it on real hardware. However it also runs on the 
emulators as well.

Note that Dream Tracker has to clear the BRAM area and so it may 
take a few seconds to start. This is normal.

## Grabbing the Latest Version

The latest download is available on the [main](../index.md) page.

## Running On Real Hardware

DreamTracker works best on systems with 2MB of RAM, however it will
run with 512k systems. Be aware songs cannot be as long and, at present,
there is no protection from loading a song that requires >512k.

To run Dream Tracker on real hardware, place the unzipped files 
into a directory on your FAT32-formatted SD card. Be sure you have
DREAMTRK.PRG as well as the SCR and DAT sub-directories.

Launch it just as you would any other app using BASIC of your 
favorite shell such as [cx16shell](https://github.com/irmen/cx16shell).

## Running with x16emu

To run with x16emu, unpack the ZIP in a directory and then run x16emu
as you would any other app. To make things easier, I place symlinks to
x16emu and rom.bin in the same directory. On Linux, I can then simply do:

```
cd dream-tracker
./x16emu -rom rom.bin -scale 2 -prg DREAMTRK.PRG -ram 2048 -run
```

## Runnong with box16

box16 seems to have issues with host filesystem access. This means you'll 
need to make an sdcard.img file and place dreamtracker there. The source
distribution of Dream Tracker includes some helper shell scripts to 
make this easier.