# ZSM Export

ZSM Export is located in the File Menu (`F10`) but since it's likely a very
important feature for many, it seemed relevant to have it's own section.

ZSM support was added in early November, 2023 and is currently in alpha. 
It supports all 3 sound sources (PSG, FM, PCM) and produced ZSM files 
that sounded nearly identical as the DreamTracker files when played back
via Melodius. Being alpha there may be some exceptions.

Similarly, the implementation currently relies on playing through the song
in realtime rather than having a separate export. This will change in future
releases.

## Future Improvements

  * Status messages on realtime export (until that gets replaced)
  * Specify your own filename
  * Non-realtime export
  * Efficient writes using `MCIOUT`
  * Setting loop position (by specifying an order number)
  * Channel masks (for using ZSM along with effects)
  * More compact file (ZSM export currently writes every tick)

## Recording Your Song to ZSM

The ZSM file is written in realtime as the song plays, which means if your song
loops, DreamTracker will keep writing to the file until you stop the song. To make
sure the song stops in the right spot, it's recommended to add the Stop Song 
(`0Cxx`) command at the end of your song.

Folks who never got to record music to cassette can experience a bit what it's like
by trying the export feature.

Once you have your stop song command in the right spot, to write the ZSM simply
go to the file menu (`F10`) and press ALT-Z. The song will start playing normally.
Note if using a real X16 or with the an SD card image, you will likely notice lots
of pauses. This is "normal" since writing data byte by byte to the SD card takes time.

Using x16emu with the host file system generally avoids pauses. This could stand
to be improved, I know. The initial feature was to simply enable ZSM export so folks
could start using the tracker to write music for games and apps. The ideal is to be
able to efficienty export the song non-realtime using the real hardware end to end.

**When your song stops, be sure to wait some time for the file to finish writing.**