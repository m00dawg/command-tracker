# Using Envelopes

At present there are 32 envelopes at up to 256 slots each. 
These can be accessed by pressing `F9`. 

![Envelope Example](images/envelope1.png)

## Navigation

| Key         | Movement                                |
| ----------- | --------------------------------------- |
| Arrow-Keys  | Move around                             |
| `TAB`       | Switch to/from env editor               |
| `Page Up`   | Increase value by $10                   |
| `Page Down` | Increase value by $10                   |
| `HOME`      | Set to max                              |
| `END`       | Set to min                              |
| `ALT-M`     | Switch between short/full envelope view |

Select the envelope you want using the arrow keys (up and down). 
To modify a particular envelope, press `TAB` to switch to the 
envelope editor. Here you can use the arrow keys to draw out your 
envelope. To go back and select another envelope, press `TAB` again.

You know you are in envelope edit mode when you see a PETSCII up arrow.
This indicates which position of the envelope you are on.

The `HOME` and `END` keys jump to the top or bottom of the envelope list
or set the current position of the envelope to the maximum or minimum value
repsectively.

`PAGE-UP` and `PAGE-DOWN` increment or decement the envelope value 
by $10 (16)

`ALT-M` toggles between the short view, intended for volume and wave
on the PSG, which caps out at `$3F`; and the full view, intended for 
pitch.

## Using Envelopes

Envelopes can be assigned to multiple sources. For instance, an envelope 
could be used for PWM in one instrument while be used for volume in another.
Currently VERA instruments are where envelopes can be assigned. At the 
moment, volume and PWM envelopes are implemented. To use them switch to the 
VERA Instrument menu (`F3`). Here you can assign envelopes to each 
instrument. Assigning and configuring instruments to use envelopes 
will be covered in the VERA Instruments portion of the manual.