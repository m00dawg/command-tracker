# Pre 0.70 Pattern Effects

Pattern effects are used to modulate sounds in various ways 
and can be used in both patterns as well as (if/once supported) tables.

Effects between the sound sources (PSG, FM, and PCM) are all distinct. 
That is PSG effect `1234` could be different for FM and PCM.

## VERA

### Implemented Effects

| Hex   | Effect                     | Persistent |
| ----- | -------------------------- | ---------- |
| 00xx  | Pitch Slide Down           | No         |
| 01xx  | Pitch Slide Up             | No         |
| 02xx  | Pitch Sweep Down           | Yes        |
| 03xx  | Pitch Sweep Up             | Yes        |
| 04xx  | Volume Slide/Sweep Down/Up | No/Yes     |
| 05xx  | PWM Slide/Sweep Down/Up    | No/Yes     |
| 06xx  | PWM Set Value              | No         |
| 07xy  | Arpeggio                   | Yes        |
| 08xx  | Glissando (Glide)          | Yes        |
| 09xy  | Vibrato                    | Yes        |
| 0Axy  | Tremolo                    | Yes        |
| 0Bxy  | Pulsolo                    | Yes        |
| 0C..  | Stop Song                  | Global     |
| 0D..  | Pattern Break              | Global     |
| 0Exx  | Set Song Speed             | Global     |
| 0Fxx  | Jump to Order              | Global     |
| 10..  | Reset Channel              | No         |
| 11..  | Global Transpose           | Global     |
| 12xx  | Pitch Speed Multiplier     | No         |


#### Glissando (Glide)

Glissando, also known as Glide and in the Eurorack world as slew,
will slide to the target note at a speed of `xx`. This effect
is persistent meaning once enabled, it will continue to slide to
the last note played in the pattern. This includes if swapping
instruments in the same channel.

To disable, set the speed to `00`

#### Vibrato, Tremolo, Pulsolo

These effects work a tad differently than some trackers. Instead of 
speed and depth, DreamTracker uses loop-length and depth. Subtle
but important difference where loop-length (the first number)
sets how big the triangle loop is.

The triangle loop is made by adding a the depth constant to the
base channel pitch/vol/PWM for half the loop and then subtracting it for
the rest of the loop. When playing a new note, the loop position
is reset to 1/4th of the loop (halfway through the up stage) which
is close to the zero cross.

This means `x` and `y` are perhaps more coupled than they are in
other trackers.

If all that didn't make any sense, don't worry, it works mostly 
the same as other vibratos. `x` is, more or less, speed and `y`
is more or less the depth.

To disable set depth (`y`, or the 2nd parameter) to `0`. 

#### Slides vs Sweeps

Slides are single shot. That is to say, they run once when you put
specify the effect for a channel. If you want the effect to fire 
multiple times, you have to specify it. This is by design because
it allows for intentional coarse stair-step style sounds as well
as slicer/stacatto, among other things.

Sweeps, however, are continual and run until they are turne off
by specifying 00 (for example `0500`). They also run at the same
speed as the sound engine which means they are _much_ smoother and
faster. They can be thought of like a simple linear envelope in this
way. If you want to make a kick-drum from a square-wave, you'll
want to use a sweep.

Some effects combine slide/sweet and direction in one command. This
is because the VERA only supports volume and PWM from 00-3F. We can
use the top bits to add more function. If that sounds complicated,
don't worry, just use the table below:

| Range | Function   |
| ------| ---------- |
| 00-3F | Slide Down |
| 40-7F | Slide Up   |
| 80-BF | Sweep Down |
| C0-FF | Sweep Up   |

#### Global Transpose

Changes the global transpose value to the one provided. It is signed 
meaning putting in `11FF` would transpose down by 1 semitone,
`11FE` by 2, etc. Putting in `1101` would transpose up by 1,
`1102` up by 2, etc.

#### Pitch Speed Multiplier

This sets the channel pitch shift value, which controls the speed
that pitch sweeps and glides run at. This value corresponds to
how many bit shifts to shift the glide or sweep value before
adding it to the note.semitone values. The default value can
be set for each instrument (see [Vera Instruments](vera_instruments.md)).

### Volume Column Effects

Since the volume range of the VERA is `00-3F`, the upper values
can be used for other things, such as panning, changing the wave
or setting the PWM.

| Range | Function            |
| ------| ------------------- |
| 00-3F | Volume              |
| 40-7F | Channel Attenuation |
| 80-BF | Set PWM             |
| C0    | Pan Center          |
| C1    | Pan Left            |
| C2    | Pan Right           |
| C3    | Set Wave to Pulse   |
| C4    | Set Wave to Saw     |
| C5    | Set Wave to Tri     |
| C6    | Set Wave to Noise   |
| TBD   | Turn off vol env    |
| TBD   | Turn on vol env     |
| TBD   | Turn off pwm env    |
| TBD   | Turn on pwm env     |
| TBD   | Turn off pitch env  |
| TBD   | Turn on pitch env   |
| E0    | Legato Off (TBD)    |
| E1    | Legato On (TBD)     |

#### Channel Attenuation

The range `40-7F` is used to set attenuation. This value is subtracted
from the channel volume before it is written out to the VERA. This can
be extremely useful if wanting to, for example, use multiple channels
to simulate delay. The attenuation is *always* applied to the volume 
and is global to the channel.

## FM

Here are currently implemented and planned effects. There will likely
be more than is listed currently, but these are either what works today
or what will be available soon.

### Effects

| Hex   | Effect                       | Persistent |
| ----- | ---------------------------- | ---------- |
| 00xx  | Pitch Slide Down             | No         |
| 01xx  | Pitch Slide Up               | No         |
| 02xx  | Pitch Sweep Down             | Yes        |
| 03xx  | Pitch Sweep Up               | Yes        |
| 04xx  | Volume Slide  Down/Up        | TBD        |
| 05xx  | Volume Sweep  Down/Up        | TBD        |
| 06-x  | Legato                       | TBD        |
| 07xy  | Arpeggio                     | TBD        |
| 08xx  | Glissando (Glide)            | Yes        |
| 09xx  | LFO Waveform                 | Global     |
| 0Axx  | LFO Frequency                | Global     |
| 0Bxx  | LFO AM/PM Depth              | Global     |
| 0Cxx  | Mod Sense. (Vibrato/Tremolo) | TBD        |
| 0Dxx  | Channel Attenuation          | TBD        |
| 0Exx  | Ch. 7 ($17) Noise Control    | Yes        |
| 0Fxx  | (TO ADD: Global to Op Vol)   | Yes        |
| 10..  | Reset Channel                | No         |

#### FM Channel Register Effects

These directly control the channel registers. 

| Hex   | Effect                            | Persistent  |
| ----- | --------------------------------- | ----------- |
| 40xx  | M1 Fine Detune and Frequency Mult | Yes         |
| 41xx  | M2 Fine Detune and Frequency Mult | Yes         |
| 42xx  | C1 Fine Detune and Frequency Mult | Yes         |
| 43xx  | C2 Fine Detune and Frequency Mult | Yes         |
| 44xx  | M1 Volume                         | Yes         |
| 45xx  | M2 Volume                         | Yes         |
| 46xx  | C1 Volume                         | Yes         |
| 47xx  | C2 Volume                         | Yes         |
| 48xx  | M1 Key Scale and Attack           | Yes         |
| 49xx  | M2 Key Scale and Attack           | Yes         |
| 4Axx  | C1 Key Scale and Attack           | Yes         |
| 4Bxx  | C2 Key Scale and Attack           | Yes         |
| 4Cxx  | M1 AM Enable and Decay 1 Rate     | Yes         |
| 4Dxx  | M2 AM Enable and Decay 1 Rate     | Yes         |
| 4Exx  | C1 AM Enable and Decay 1 Rate     | Yes         |
| 4Fxx  | C2 AM Enable and Decay 1 Rate     | Yes         |
| 50xx  | M1 Coarse Detune and Decay 2 Rate | Yes         |
| 51xx  | M2 Coarse Detune and Decay 2 Rate | Yes         |
| 52xx  | C1 Coarse Detune and Decay 2 Rate | Yes         |
| 53xx  | C2 Coarse Detune and Decay 2 Rate | Yes         |
| 54xx  | M1 Sustain and Release            | Yes         |
| 55xx  | M2 Sustain and Release            | Yes         |
| 56xx  | C1 Sustain and Release            | Yes         |
| 57xx  | C2 Sustain and Release            | Yes         |
| 58xx  | Pan, Feedback and Algorithm       | Yes         |
| 59xx  | PM and AM Modulation Sensitivity  | Yes         |

#### Noise Control

Enables/Disables the noise waveform on modulator C2 of channel 7 and
controls the noise frequency. The lower 5 bits control the frequency.
Values `$80-$9F` enable the effect and set the frequency.

### Volume Column Effects

Since the volume range of the YM2151 is `00-7F`, the upper values
can be used for other things, such as panning, etc.

| Range | Function            | Persistent |
| ----- | ------------------- | ---------- |
| 00-7F | Volume (TBD)        | Yes        |
| C0    | Pan Center          | Yes        |
| C1    | Pan Left            | Yes        |
| C2    | Pan Right           | Yes        |
| E0    | Legato              | No         |

#### Legato

Normally when playing a note, a note release is sent to the YM2151 
immediately followed by a note-on. This resets the enveloeps and things.
Sometimes it may be desirable to let the envelopes run while changing
the note played, and this is what legato does. Guitar players can think
of it as "pull-offs" in a way.

It is evaluated only for the current note being played. I may add a
toggleable version (e.g. perhaps `E1`) which could be used when needed.

## PCM

PCM has no effect support yet beyond volume, but a few effects are planned:

  * Retrigger
  * Sample Offset
  * Reverse?
  * (Coarse) Volume/Pitch Slide/Sweep Support

Since we have a lot of room in the volume column, there may be 
combinations, such as using the Volume to fine two sample offset.
