# VERA PSG Instruments

VERA supports 16 Programmable Sound Generators (PSGs) each of 
which supports 4 different waveforms (pulse, saw, triangle, noise); 
left/right panning; 6-bit volume; 6-bit PWM. By modulating these
various elements, interesting sounds can be made.

![VERA Instruments Example](images/vera_instruments1.png)

The instrument screen, accessed by pressing `F4`, allows defining of up
to 128 instruments. Instruments are a bit like defaults. When you
specify a new instrument within the channel column of a pattern, 
on playback the sound engine will retrieve the configuration of the instrument.

Further modifications to the sound can be done via pattern effects.
Be aware, however, that running envelopes can change how those effects
might work.

Upon loading the screen, the active panel will be the left side where you
can select and name the instruments. The names are just to make things
easier to remember. The current selected instrument number will be 
highlighted with the contents of that instrument along the right.

To edit an instrument, simply press TAB to switch to the right side.

## Instrument Parameters

The default settings of the instrument are along the top section and
represent the basic sound to get from it. You can navigate via the
arrow keys. Up and down moves between sections; left to right moves
between characters. It can also be a shortcut to get to the other
envelopes.

### Pan

Here you can enable or disable L and R by pressing space and left/right
arrow. Note you can disable both channels and this might be useful depending
on the types of modulation you might want to do.

### Volume

Here you can set the volume from $00 to $3F with $3F being full volume.

### Wave

VERA supports 4 wave types, Pulse, Saw, Triangle and Noise. Pick which one
you want by pressing the corresponding key on the keyboard.

### Width

Set the width. At present this only works for Pulse instruments and
sets the duty cycle (with `$3F` being 50/50). There has been some
experiments on adding width to Saw and Triangle, which turns the 
effect into a sort of wave-folder. That feature isn't yet in mainline
VERA (From Tim: though it would be an amazing feature and I really
hope we see it).

Modulating PWM is one of the ways to get those iconic SID sounds.

### Fine Tune (Pitch)

Here you can modify the base pitch of the instrument. You might want
to do this to make a chorus sound by using two instruments that have
a different fine pitch and playing them on 2 different channels for example.

The fine pitch range is from -128 to 127 and is represented in 2's 
compliment hexadecimal. That means values from `$00-$80` pitch up
and values from `$FF-$81` pitch down. 

Thus if you wanted to pitch the base note down by -1, you would put in
`$FF` for the value. For -2, `$FE`, etc.

### Pitch Speed

This controls the speed of the pitch sweeps and glide. It is basically
how many bit shifts to move the given value over prior to adding or
subtracting the value from the pitch. Useful for instance if you need
pitch slides to go faster for things like kick drums.

## Envelopes

One of the ways to modulate sounds is by way of [Envelopes](envelopes.md).
Envelopes allow you to change the shape of the sound automatically. 
At present, Volume and PWM have basic functionality. You can enable/disable
each envelope, specify which envelope number to use and specify the length.

The length is how far the sound engine marches through the envelope before it
stops. It will stop at that position and stay there. This is useful for 
the future (for things like note-off).

Loops are defined in the interface but are not yet supported. Likewise
pitch does not yet work.