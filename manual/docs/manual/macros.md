# Macros

Macros allow for executing multiple effects per channel/row.

There are currently 64 ($40) macros available, each of which 
can execute up to 8 effects. Macros do not know or care about
which sound chip is being used, meaning they can be used across
PSG, FM, and PCM channels. This does mean macro functionality
may differ if using per-chip effects.

![Macro Example](images/macro1.png)

Macros are accessed by pressing `F9` multiple times. Each effect
column works the same way as the pattern effects. The first
byte (the first two numbers) is the effect parameter. The 
second byte (the last two numbers) is the value.

To run a macro, use the `3F` effect followed by the macro number
you wish to call. 

Note, you cannot call a macro from a macro. 